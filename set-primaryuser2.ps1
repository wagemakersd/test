
##########
$IntuneDeviceId = $autoobj.ManagedDeviceId
$uri = "beta/deviceManagement/managedDevices('$IntuneDeviceId')/users/`$ref"
$UserId = $user.id

$JSON = @{ 
    "@odata.id"="https://graph.microsoft.com/beta/users/$UserId" 
} | ConvertTo-Json -Compress

Try {
    invoke-mggraphrequest -Uri $Uri -Body $JSON -ContentType "application/Json" -Method POST
}
catch{
    
}