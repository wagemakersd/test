﻿##################################################################################
########################## Create Office 365 account #############################
##################################################################################

 #Set recource properties capacity, location, phone
$msolcred = get-credential
connect-msolservice -credential $msolcred

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session

$DomainController="deffm-dc1"

$CPath="\\deffm-adfs2.vwdgroup.net\Office365$"
$USRPath="$CPath\USR\"
$SMBPath="$CPath\SMB"
$RoomPath="$CPath\Room"
$OoOPath="$CPath\OoO"
$Date=get-date -Format yyyyMMdd

############################# Create useraccount #################################
##################################################################################
$USRList=read-host "userfile to process"
If(!($USRList))
{
$SMBList=read-host "SMBfile to process"
}
If(!($SMBList))
{
$RoomList=read-host "Roomfile to process"
}
If(!($RoomList))
{
$OoOList=read-host "OoOfile to process"
}

If($USRList)
{
    $Entry=$USRList
    $CSV=import-csv -Path "$USRPath\$Entry" -Delimiter "|" 
    $CSV

    $UserPrincipalName=$CSV.UserPrincipalName
    $Office365=$CSV.Office365


    $msolUser = Get-MsolUser -UserPrincipalName $UserPrincipalName -ErrorAction SilentlyContinue
    If($msolUser)
    {
    $msolUse
        If ($Office365 -eq "Full")
        {
        $Office365
            $License = (Get-MsolUser -UserPrincipalName $UserPrincipalName).Licenses.AccountSkuId
            foreach($lic in $license)
            {
                $lic
                Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -RemoveLicenses $Lic
            }
            #Full License
            Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -AddLicenses vwd365:ENTERPRISEPACK -ErrorAction SilentlyContinue
            $serviceplan = New-MsolLicenseOptions -AccountSkuId vwd365:ENTERPRISEPACK -DisabledPlans FORMS_PLAN_E3,STREAM_O365_E3,Deskless,FLOW_O365_P2,POWERAPPS_O365_P2,PROJECTWORKMANAGEMENT,SWAY,INTUNE_O365,YAMMER_ENTERPRISE,RMS_S_ENTERPRISE,SHAREPOINTWAC,SHAREPOINTENTERPRISE
            Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -LicenseOptions $serviceplan

            $timeout2 = new-timespan -Minutes 5
            $sw2 = [diagnostics.stopwatch]::StartNew()
            $enabledmailbox=$null
            While($sw2.elapsed -lt $timeout2)
            {
                start-sleep -seconds 30
                $enabledmailbox = get-recipient -identity $vwdEmail -ErrorAction SilentlyContinue
                If($enabledmailbox)
                {
                    $enabledmailbox
                    Set-Mailbox $vwdEmail -AuditEnabled $true -AuditAdmin Copy,Create,FolderBind,HardDelete,MessageBind,Move,MoveToDeletedItems,SendAs,SendOnBehalf,SoftDelete,Update -AuditDelegate Create,FolderBind,HardDelete,Move,MoveToDeletedItems,SendAs,SendOnBehalf,SoftDelete,Update -AuditOwner Create,HardDelete,MailboxLogin,Move,MoveToDeletedItems,SoftDelete,Update
                    Break
                }
            }
        }
        Elseif  ($Office365 -eq "Exchange + Skype")
        {
            $Office365
            $License = (Get-MsolUser -UserPrincipalName $UserPrincipalName).Licenses.AccountSkuId
            foreach($lic in $license)
            {
                $lic
                Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -RemoveLicenses $Lic
            }
            #EXCHANGE AND SKYPE License
            Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -AddLicenses vwd365:EXCHANGEENTERPRISE  -ErrorAction SilentlyContinue
            Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -AddLicenses vwd365:MCOSTANDARD  -ErrorAction SilentlyContinue
        }
        elseif ($Office365 -eq "Forward External") 
        {
            $Office365 
            $License = (Get-MsolUser -UserPrincipalName $UserPrincipalName).Licenses.AccountSkuId
            foreach($lic in $license)
            {
                $lic 
                Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -RemoveLicenses $Lic
            }
        }
    $NewName=$Entry -replace "csv", "$Date"
    Rename-Item -Path "$USRPath\$Entry" -Newname $NewName
    $lic=$null
    $License=$null
    }
}

################################ Shared Mailbox ##################################
##################################################################################


Elseif($SMBList)
{
    $Entry=$SMBList
    $CSV=Import-csv -Path "$SMBPath\$Entry" -Delimiter "|"

    $givenName = ''
    $surName = ''
    $displayname = $CSV.Displayname
    $vwdEmail = $CSV.UserPrincipalName
    $emailprefix = $vwdEmail -replace "@vwd.com"
    $samAccountName = $CSV.SamAccountName
    $sloc=$CSV.Sloc
    $SCountry=$CSV.Country
    $sgmail="$samAccountName"+"-sg@vwd.com"

    $msolUser = Get-MsolUser -UserPrincipalName $vwdEmail -ErrorAction SilentlyContinue
    If($msolUser)
    {
        $msolUser
        Set-MsolUserLicense -UserPrincipalName $vwdEmail -AddLicenses vwd365:EXCHANGEENTERPRISE  -ErrorAction SilentlyContinue

        $timeout2 = new-timespan -Minutes 5
        $sw2 = [diagnostics.stopwatch]::StartNew()
        $enabledmailbox=$null
        While($sw2.elapsed -lt $timeout2)
        {
            start-sleep -seconds 30
            $enabledmailbox = get-recipient -identity $vwdEmail -ErrorAction SilentlyContinue
            If($enabledmailbox)
            {
                $enabledmailbox
                Set-Mailbox $vwdEmail -Type Shared
                Break
            }
        }

        $permission=add-mailboxpermission -identity $vwdEmail -user $sgmail -accessright Fullaccess
        $License = (Get-MsolUser -UserPrincipalName $vwdEmail).Licenses.AccountSkuId
        foreach($lic in $license)
        {
            $lic
            Set-MsolUserLicense -UserPrincipalName $vwdEmail -RemoveLicenses $Lic
        }
    $NewName=$Entry -replace "csv", "$Date"
    Rename-Item -Path "$SMBPath\$Entry" -Newname $NewName
    }
}

################################ Room Mailbox ##################################
##################################################################################


Elseif($RoomList)
{
    $Entry=$RoomList
    Import-csv -Path "$RoomPath\$Entry" -Delimiter "|"

    $vwdEmail = $CSV.UserPrincipalName
    $samAccountName = $CSV.samAccountName
    $Capacity = $CSV.Capacity

    $msolUser = Get-MsolUser -UserPrincipalName $vwdEmail -ErrorAction SilentlyContinue
    If($msolUser)
    {
        $msolUser
        Set-MsolUserLicense -UserPrincipalName $vwdEmail -AddLicenses vwd365:EXCHANGEENTERPRISE  -ErrorAction SilentlyContinue
        
        $timeout2 = new-timespan -Minutes 5
        $sw2 = [diagnostics.stopwatch]::StartNew()
        $enabledmailbox=$null
        While($sw2.elapsed -lt $timeout2)
        {
            start-sleep -seconds 30
            $enabledmailbox = get-recipient -identity $vwdEmail -ErrorAction SilentlyContinue
            If($enabledmailbox)
            {
                $enabledmailbox
                Set-Mailbox $vwdEmail -Type Room
                If($Capacity)
                {
                    Set-Mailbox -Identity $samAccountName -ResourceCapacity $Capacity
                }
                Break
            }  
        }
        $License = (Get-MsolUser -UserPrincipalName $vwdEmail).Licenses.AccountSkuId
        foreach($lic in $license)
        {
            $lic
            Set-MsolUserLicense -UserPrincipalName $vwdEmail -RemoveLicenses $Lic
        }
    $NewName=$Entry -replace "csv", "$Date"
    Rename-Item -Path "$RoomPath\$Entry" -Newname $NewName
    }
}


################################ Out of Office ###################################
##################################################################################


Elseif($OoOList)
{
    $Entry=$OoOList
    $csv=import-csv -Path "$OoOPath\$Entry" -Delimiter "|"

    $OMail = $CSV.Email
    $OFWD = $CSV.Forwarding
    $OMSG = $CSV.OutofOffice

    If($OFWD)
    {
        $OFWD
        Set-Mailbox $OMail -ForwardingAddress $OFWD
    }
    Else
    {
        Set-Mailbox $OMail -ForwardingAddress $null
    }

    If($Global:OMSG)
    {
        $Global:OMSG
        Set-MailboxAutoReplyConfiguration -Identity $OMail -AutoReplyState Enabled -InternalMessage $OMSG -ExternalMessage $OMSG
    }    
    Else
    {
        Set-MailboxAutoReplyConfiguration -Identity $OMail -AutoReplyState Disabled
    }
    $NewName=$Entry -replace "csv", "$Date"
    Rename-Item -Path "$OoOPath\$Entry" -Newname $NewName
}

If($O365Session)
{
    Remove-PSSession -Session $O365Session
    $O365Session=$null
}
Remove-Variable * -ErrorAction SilentlyContinue