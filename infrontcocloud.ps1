$recipients=get-recipient -ResultSize unlimited

$recipients.RecipientType|select -unique
$notsynced=$recipients|?{$_.IsDirSynced -eq $false}

$recipients.count
$notsynced.Count
$nogroup=$notsynced|?{$_.RecipientType -eq "MailUniversalDistributionGroup"}
$nogroup=$nogroup|?{$_.RecipientType -ne "MailUniversalSecurityGroup"}
$nogroup.count

$noext=$nogroup|?{$_.WindowsLiveID -notmatch "#EXT#"}
$noext.count
$noadm=$noext|?{$_.displayname -notmatch "(Admin)"}
$noadm.count

$noadm|?{$_.RecipientTypeDetails -eq "SharedMailbox"}|select name, PrimarySmtpAddress
$addinfrontco=$noadm|?{$_.RecipientTypeDetails -eq "SharedMailbox"}
$addinfrontco.count

$change=@()
$dontchange=@()
foreach($entry in $addinfrontco){
    $suffix= $entry.PrimarySmtpAddress.split("@")[-1]
    $proxy= $entry.PrimarySmtpAddress -replace "$suffix","infront.co"
    if(!(get-recipient $proxy -ErrorAction SilentlyContinue)){
        write-host -ForegroundColor Green "Adding $proxy to $($entry.DisplayName)"
        $change+= New-Object -TypeName PSObject -property @{
            New = $proxy
            Primary = $entry.PrimarySmtpAddress
        }
        Set-Mailbox -Identity $entry.name -EmailAddresses @{Add="$proxy"}
    }
    else{
        Write-Warning "$proxy already found"
        $dontchange+= New-Object -TypeName PSObject -property @{
            New = $proxy
            Primary = $entry.PrimarySmtpAddress
        }
    }
}
$change1=$change
$dontchange1=$dontchange




$groups=$notsynced|?{$_.RecipientTypeDetails -eq "MailUniversalDistributionGroup"}
($notsynced|?{$_.RecipientType -eq "MailUniversalDistributionGroup"}).count
$groups|select RecipientTypeDetails -Unique
$groups.count
$groups[100]|fl *

$groups.displayName