$csv=import-csv "c:\temp\user-migrations.csv"


$list=$csv|?{$_.Type -eq "Shared"}
$array=@()
### IF CORRECT THE BELOW SHOULD NOT GIVE ANY RESULT
### CHECKING IF SHARED MAILBOX IS SHARD MAILBOX
Foreach($entry in $list)
{
    $entry.TargetUPN
    If((get-recipient $entry.TargetUPN -resultsize unlimited -erroraction silentlycontinue).recipienttypedetails -eq "UserMailbox")
    {
        $array+=$entry
    }
}

### CHECKING IF SHARD MAILBOX HAS A LICENSE
$array2=@()
Foreach($entry in $list)
{
    $entry.TargetUPN
    If(((get-msoluser -UserPrincipalName $entry.TargetUPN -erroraction silentlycontinue).islicensed -eq $true))
    {
        $array2+=$entry
    }
}
($array2.TargetUPN).count
$array2|%{
    get-recipient $_.targetupn  -resultsize unlimited |select name,ArchiveDatabase
}

$array3=@()
$array2|%{
    If((get-recipient $_.targetupn  -resultsize unlimited ).ArchiveDatabase -eq $null)
    {
        $array3+=$_
        $_.targetupn
        Get-MailboxStatistics $_.TargetUPN |select displayName,TotalItemSize    
    }
}
$array3
$array4=$array3|?{$_.TargetUPN -ne "BER-SMB-00060@infrontfinance.com"}




$array4|%{
    $_.TargetUPN
    $License = (Get-MsolUser -UserPrincipalName $_.TargetUPN).Licenses.AccountSkuId
    foreach($lic in $license)
    {
        $lic
        Set-MsolUserLicense -UserPrincipalName $_.TargetUPN -RemoveLicenses $Lic -ErrorAction Stop
    }
}