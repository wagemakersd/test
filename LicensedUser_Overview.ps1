$Thumbprint="45d2e0b4238489cdb93295996483305202ba50f6"
$Tenant="23371993-8e9a-4de5-97db-90d3e293e863"
$Org="infrontonline.onmicrosoft.com"
$App="cc8469f3-2bdf-4d2a-b430-636ea63a1691"
<#
Connect-MgGraph -CertificateThumbprint $Thumbprint -ClientId $App -TenantId $Tenant
#>
$cred=Get-Credential

$License=Get-MgSubscribedSku -All  
| Where SkuPartNumber -eq $LicSku

Foreach ($lic in $license){
    write-host "`n $($lic.SkuPartNumber) `n" 
    $mgusers=Get-MgUser -all -Property "displayName,mail,userprincipalname,OfficeLocation,BusinessPhones,JobTitle,OnPremisesDomainName,OnPremisesSamaccountName,OnPremisesSyncEnabled,AccountEnabled,id,assignedlicense,usertype,SignInActivity" -Filter "assignedLicenses/any(u:u/skuId eq $($Lic.skuid))"
    $enabled=$mgusers|?{$_.accountenabled -eq $true}
    $disabled=$mgusers|?{$_.accountenabled -ne $true}
    $equal=($lic.ConsumedUnits -eq $enabled.count)
    $enabled.count
    if($equal -eq $true){
        Write-host -ForegroundColor Green "License- and enabled usertotal are equal:" ($lic.ConsumedUnits -eq $enabled.count)
    }else {
        Write-Warning  "License- and enabled usertotal are equal: $equal" 
    }
    if($disabled){
        write-warning "Disabled users:" 
        $disabled.UserPrincipalName
    }

    $oslusers=@()
    $vwdgroupusers=@()
    $cloudusers=@()
    foreach ($mguser in $enabled){
        switch ($mguser.OnPremisesDomainName) {
            "osl.infront.as" {
                write-host "$($mguser.displayname) ; OSL"
                $oslusers+=$mguser
            }
            "vwdgroup.net" {
                write-host "$($mguser.displayname) ; Former vwd user"
                $vwdgroupusers+=$mguser
            }
            default{
                write-host "$($mguser.displayname) ; No matched OnPremisesDomainName"
                $cloudusers+=$mguser
            }
        }
    }
    #$vwdgroupusers|FT DisplayName,UserPrincipalName,OnPremisesSamaccountName,Mail,OfficeLocation,BusinessPhones,AccountEnabled,UserType
    #$oslusers|FT DisplayName,UserPrincipalName,OnPremisesSamaccountName,Mail,OfficeLocation,BusinessPhones,AccountEnabled,UserType
    #$cloudusers|FT DisplayName,UserPrincipalName,OnPremisesSamaccountName,Mail,OfficeLocation,BusinessPhones,AccountEnabled,UserType
    #$vwdgroupusers|select -expandproperty SignInActivity

    #$vwdusers=get-aduser -Filter * -Properties * -Server deffm-dc3.vwdgroup.net
    $vwdobjects=@()
    foreach ($synceduser in $vwdgroupusers){
        $synceduser.DisplayName
        $onpremuser=get-aduser -Identity $synceduser.OnPremisesSamaccountName -Properties * -Server deffm-dc3.vwdgroup.net -Credential $cred
        $vwdobjects+=New-Object -TypeName psobject -Property @{
            DisplayName                 = $synceduser.DisplayName
            UserPrincipalName           = $synceduser.UserPrincipalName
            OnPremisesSamaccountName    = $synceduser.OnPremisesSamaccountName
            OnPremisesDomainName        = $synceduser.OnPremisesDomainName
            Mail                        = $synceduser.Mail
            OfficeLocation              = $synceduser.OfficeLocation
            BusinessPhones              = ($synceduser.BusinessPhones) -join ","
            AccountEnabled              = $synceduser.AccountEnabled
            LastLogonDate               = IF($synceduser.AdditionalProperties.signInActivity.lastSignInDateTime){$synceduser.AdditionalProperties.signInActivity.lastSignInDateTime.Split("T")[0]}else{$Null}
            PasswordLastSet             = $onpremuser.PasswordLastSet
            Title                       = $onpremuser.Title
            CanonicalName               = $onpremuser.CanonicalName
            Company                     = if($onpremuser.Company){$onpremuser.Company}else{$null}
            manager                     = $onpremuser.manager
            AccountExpirationDate       = if($onpremuser.AccountExpirationDate){$onpremuser.AccountExpirationDate}else{"0"}
            Talentsoft_Username         = if($onpremuser.extensionAttribute10){$onpremuser.extensionAttribute10}else{""}
            User_type                   = if($synceduser.Mail -match ".ext@"){"External"}elseif($synceduser.DisplayName -match ","){"Internal"}else{"Other"}
        }
    }

    $oslobjects=@()
    foreach ($synceduser in $oslusers){
        $synceduser.DisplayName
        $onpremuser=get-aduser -Identity $synceduser.OnPremisesSamaccountName -Properties * -Server SE8DC.osl.infront.as -Credential $cred
        $oslobjects+=New-Object -TypeName psobject -Property @{
            DisplayName                 = $synceduser.DisplayName
            UserPrincipalName           = $synceduser.UserPrincipalName
            OnPremisesSamaccountName    = $synceduser.OnPremisesSamaccountName
            OnPremisesDomainName        = $synceduser.OnPremisesDomainName
            Mail                        = $synceduser.Mail
            OfficeLocation              = $synceduser.OfficeLocation
            BusinessPhones              = ($synceduser.BusinessPhones) -join ","
            AccountEnabled              = $synceduser.AccountEnabled
            LastLogonDate               = IF($synceduser.AdditionalProperties.signInActivity.lastSignInDateTime){$synceduser.AdditionalProperties.signInActivity.lastSignInDateTime.Split("T")[0]}else{$Null}
            PasswordLastSet             = $onpremuser.PasswordLastSet
            Title                       = $onpremuser.Title
            CanonicalName               = $onpremuser.CanonicalName
            Company                     = if($onpremuser.Company){$onpremuser.Company}else{$null}
            manager                     = $onpremuser.manager
            AccountExpirationDate       = if($onpremuser.AccountExpirationDate){$onpremuser.AccountExpirationDate}else{"0"}
            Talentsoft_Username         = if($onpremuser.extensionAttribute10){$onpremuser.extensionAttribute10}else{""}
            User_type                   = if($synceduser.Mail -match ".ext@"){"External"}elseif($synceduser.DisplayName -match ","){"Internal"}else{"Other"}
        }
    }


    $cloudobjects=@()
    foreach ($clouduser in $cloudusers){
        $clouduser.DisplayName
        $cloudobjects+=New-Object -TypeName psobject -Property @{
            DisplayName                 = $clouduser.DisplayName
            UserPrincipalName           = $clouduser.UserPrincipalName
            OnPremisesSamaccountName    = $clouduser.OnPremisesSamaccountName
            OnPremisesDomainName        = "Cloud only user"
            Mail                        = $clouduser.Mail
            OfficeLocation              = $clouduser.OfficeLocation
            BusinessPhones              = ($clouduser.BusinessPhones) -join ","
            AccountEnabled              = $clouduser.AccountEnabled
            LastLogonDate               = IF($clouduser.AdditionalProperties.signInActivity.lastSignInDateTime){$clouduser.AdditionalProperties.signInActivity.lastSignInDateTime.Split("T")[0]}else{$Null}
            PasswordLastSet             = $null
            Title                       = $null
            CanonicalName               = $null
            Company                     = $null
            manager                     = $null
            AccountExpirationDate       = $null
            Talentsoft_Username         = $null
            User_type                   = if($clouduser.Mail -match ".ext@"){"External"}else{"Other"}
        }
    }



    $combinedobjects=$vwdobjects+$oslobjects+$cloudobjects

    Export-Excel -InputObject $combinedobjects -Path "c:\temp\licenses\$($lic.SkuPartNumber).xlsx"


}






<#################################################################################
$Upnsuffixsonprem=@()
$Upnsuffixscloud=@()
foreach ($mguser in $mgusers){
    $Upnsuffix=($mguser.userprincipalname -split "@")[-1]
    if ($mguser.OnPremisesSyncEnabled){
    $Upnsuffixsonprem+=$Upnsuffix
    }
    else {
        $Upnsuffixscloud+=$Upnsuffix
    }
}
write-host "`nSynced:"
$Upnsuffixsonprem|select -Unique
write-host "`nCloud:"
$Upnsuffixscloud|select -Unique

#>
Foreach ($lic in $license){
    write-host "`n $($lic.SkuPartNumber) `n" 
    $mgusers=Get-MgUser -all -Property "displayName,mail,userprincipalname,OfficeLocation,BusinessPhones,JobTitle,OnPremisesDomainName,OnPremisesSamaccountName,OnPremisesSyncEnabled,AccountEnabled,id,assignedlicenses,usertype,SignInActivity,AdditionalProperties.signInActivity.lastSignInDateTime" -Filter "assignedLicenses/any(u:u/skuId eq $($Lic.skuid))"
    $enabled=$mgusers|?{$_.accountenabled -eq $true}
    $disabled=$mgusers|?{$_.accountenabled -ne $true}
    $equal=($lic.ConsumedUnits -eq $enabled.count)
    $enabled.count
    if($equal -eq $true){
        Write-host -ForegroundColor Green "License- and enabled usertotal are equal:" ($lic.ConsumedUnits -eq $enabled.count)
    }else {
        Write-Warning  "License- and enabled usertotal are equal: $equal" 
    }
    if($disabled){
        write-warning "Disabled users:" 
        $disabled.UserPrincipalName
    }

    $licdetails=@()
    foreach ($mguser in $mgusers){
    #$licdetail=get-mguser -UserId $mguser.id -Property 'userprincipalname','assignedlicenses', 'displayname','SignInActivity'
    #$mgusers|?{$_.id -eq $mguser.id}|select displayname, AssignedLicenses
    #$licdetails+=$licdetail|Add-Member -MemberType NoteProperty -Name LastLogonDate -Value $licdetail.AdditionalProperties.signInActivity.lastSignInDateTime
    $licdetails+=new-object -TypeName psobject -Property @{
            displayname =  $mguser.DisplayName
            AssignedLicenses =  $mguser.AssignedLicenses.skuid -join ","
            LastLogonDate =  $mguser.signInActivity.LastSignInDateTime
            Userprincipalname = $mguser.userprincipalname
            AccountEnabled = $mguser.AccountEnabled
        }
    }
    Export-Excel -InputObject $licdetails -Path ("c:\temp\licenses\graph\$($lic.SkuPartNumber)"+".xlsx")
}

($licdetails|?{$_.AssignedLicenses -notmatch "078d2b04-f1bd-4111-bbd4-b4b1b354cef4"}).count
#078d2b04-f1bd-4111-bbd4-b4b1b354cef4


$Licenses=Get-MgSubscribedSku -All 
$totals=@()
foreach($license in $licenses){
    $total = ($license| select -ExpandProperty PrepaidUnits).enabled
    $totals+=New-Object -typename PSObject -Property @{
        ConsumedUnits   = $license.ConsumedUnits
        TotalUnits      = $total
        SkuPartNumber   = $license.SkuPartNumber
    }
}
$totals |select SkuPartNumber, TotalUnits, ConsumedUnits |Export-Excel -Path c:\temp\licenses\graph\license_overview.xlsx

