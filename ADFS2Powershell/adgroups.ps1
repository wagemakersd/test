Remove-Variable * -ErrorAction SilentlyContinue

$oldname=(Get-ADGroup -Filter * |?{$_.name -match "gdep"})
$array=@()
$users=@()
$otherusers=@()
$membergroups=@()
foreach($group in $oldname)
{
    $newname=$group.name -replace "GDEP_", "nlams-"
    $members=Get-ADGroupMember -Identity $group
    
    foreach($member in $members)
    {
        If ($member.objectClass -eq "user")
        {
            $adusr=get-aduser -Identity $member -Properties *
            If($adusr.GivenName -eq $null)
            {
                $otherusers+=$adusr.SamAccountName
            }
            else 
            {    
                $surName=$adusr.surName -replace "\s", ""
                $givenName=$adusr.givenname
                $mail=$givenName.Substring(0,1)+"$surName"+"@vwd.com"
                $UPN=$mail.ToLower()
                $users+=$UPN
            }
        }
        elseif($member.objectClass -eq "Group")
        {
            $membergroup=$member.name -replace "GDEP_", "nlams-"
            $membergroups+=$membergroup
        }
    }
    
    $customobject = New-Object -TypeName psobject
        $customobject | Add-Member -MemberType NoteProperty -Name Group -Value $newname
        $customobject | Add-Member -MemberType NoteProperty -Name MemberUsers -Value (@($users) -join ',')
        $customobject | Add-Member -MemberType NoteProperty -Name OtherUsers -Value (@($otherusers) -join ',')
        $customobject | Add-Member -MemberType NoteProperty -Name MemberGroups -Value (@($membergroups) -join ',')

    $customobject | export-csv -Delimiter "|" -Path C:\Scripts\vwdgroup\Output\groups\ADGROUPS_TIJL_Produktie.csv -NoTypeInformation -Append
    $array += $customobject
}

#  IMPORT and SPLIT 

#$csv=import-csv -Delimiter "|" -Path C:\AMS-ADMINS\Powershell\migration\ADGROUPS_TIJL_Produktie.csv
$csv=import-csv -Delimiter "|" -Path C:\Scripts\vwdgroup\Output\groups\ADGROUPS_TIJL_Produktie.csv 

$DomainController=deffm-dc1.vwdgroup.net
$OU="OU=groups,ou=AM,ou=nl,dc=vwdgroup,dc=net"
$customobject=@()
$array=@()
Foreach($entry in $csv)
{
    $group=$entry.group
    $mem=$entry.MemberGroups 
    $memus=$entry.Memberusers
    $memot=$entry.Otherusers
    $mem=$mem.split(',')
    $memus=$memus.split(',')
    $memot=$memot.split(',')
    
    $customobject = New-Object -TypeName psobject
        $customobject | Add-Member -MemberType NoteProperty -Name Group -Value $group
        $customobject | Add-Member -MemberType NoteProperty -Name MemberUsers -Value ($memus)
        $customobject | Add-Member -MemberType NoteProperty -Name OtherUsers -Value ($memot)
        $customobject | Add-Member -MemberType NoteProperty -Name MemberGroups -Value ($mem)

       $notempty= $customobject|?{$_.MemberUsers -ne $null -and $_.membergroups -ne $null -and $_.Otherusers -ne $null}
       $array += $notempty
       If($notempty)
       {
            If(!(Get-adgroup -Identity $group  -Server $DomainController -ErrorAction SilentlyContinue))
            {
                New-ADGroup -name $group -Path $OU -GroupCategory Security -GroupScope Global -Server $DomainController
            }
            while(!(Get-adgroup -Identity $group  -Server $DomainController -ErrorAction SilentlyContinue))
            {
                "Wait: AD group is not yet created"
                start-sleep -seconds 15
            }
            foreach ($Member in $memus)
            {
                Try
                {
                    $Member = get-aduser -filter ('userprincipalname -eq $Member') -Server $DomainController
                    $Member = $Member.SamAccountName
                    Add-ADGroupMember -Identity $group -Members $Member -Server $DomainController
                    "Success: User $Member added"
                }
                Catch
                {
                    "Fail: Cannot find user $Member"
                }
            }
       } 
}