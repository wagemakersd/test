#get profile list
$users=get-childitem ((get-item $env:USERPROFILE).FullName| Split-Path)
#select old profile
$selected=$users | Out-GridView -PassThru -Title "Select profile to move folders from"

#filter on folders
$moving=get-childitem $selected.fullname |?{($_.name -match "Desktop") -or ($_.name -match "Documents") -or ($_.name -match "Downloads") -or ($_.name -match "Favorites") -or ($_.name -match "music") -or ($_.name -match "Pictures") -or ($_.name -match "Videos")}
foreach ($folder in $moving){
    #set destination to specific folder in profile
    $destination=$env:USERPROFILE+"\"+$folder.name
    #move from old to new profile
    ROBOCOPY $folder.fullname $destination /MOVE
}