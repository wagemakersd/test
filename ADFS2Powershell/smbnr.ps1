$counter = @()
$list =  Get-ADUser -Filter * -SearchBase "OU=Shared Mailboxes,OU=Migration,DC=vwdgroup,DC=net" | select-object samaccountname
foreach($entry in $list){ $counter += $entry.samaccountname.substring($entry.samAccountName.Length - 5) }
$last_nr = $counter | Sort-Object |Select-Object -last 1
write-host "`r`nLast used sequence number for SMB: $last_nr`r`n"  
PAUSE

