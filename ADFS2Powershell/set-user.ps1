# create User
$givenName=read-host "First Name"
$surName=read-host "Last Name"
$displayname = "$surName, $givenName"

$surName2 = $surName -replace "\s", ""
$emailprefix = $givenName.Substring(0,1)+"$surName2"
$emailprefix = $emailprefix.ToLower()
$UserPrincipalName = "$emailprefix"+"@vwd.com"

$DomainController = 'deffm-dc1'

$location = read-host "Location (f.e. DE/NL)"

$samAccountName = $surName2+$givenName.Substring(0,1)
$vwdEmail = "$emailprefix"+"@vwd.com"
$samAccountName = $samAccountName.ToLower()

"`n"
"Givenname:`t`t$givenName"
"Surname:`t`t$surName"
"Displayname:`t$displayname"
"sAMAccountName:`t$samAccountName"
"EmailPrefix:`t$emailprefix"
"Email:`t`t`t$vwdEmail"
"Location:`t`t$location"
"`n"
$correct=Read-host "Is the above correct (Y/N)"
If ($correct -eq "Y")
{
    Set-aduser -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Add @{ProxyAddresses = "sip:$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Replace @{MailNickName = "$emailprefix"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Replace @{Mail = "$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Replace @{userPrincipalName = "$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -add @{msExchUsageLocation = "$location"} -Server $DomainController -ErrorAction Stop
    Write-Host -ForegroundColor Green "SUCCESFULL: Account has been edited"
    PAUSE
}
Else
{
Write-Warning "Account has not been edited"
PAUSE
}