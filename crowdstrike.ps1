[CmdletBinding()]
param(
    [Parameter(Mandatory = $false)]
    [string]$SensorPath = "$PSScriptRoot\FalconSensor_Windows.exe",
    
    [Parameter(Mandatory = $false)]
    [string]$DomainController,
    
    [Parameter(Mandatory = $false)]
    [string]$CID = "C573CB8DA97049D8BC7E4BDB30921DA6-51",
    
    [Parameter(Mandatory = $false)]
    [int]$ThrottleLimit = 10,
    
    [Parameter(Mandatory = $false)]
    [switch]$DisableDefender = $false,
    
    [Parameter(Mandatory = $false)]
    [switch]$UninstallDefender = $false,
    
    [Parameter(Mandatory = $false)]
    [string]$TempPath = "C:\temp",

    [Parameter(Mandatory = $false)]
    [string]$GROUPING_TAGS,

    [Parameter(Mandatory = $False)]
    [switch]$OverwriteTag = $false,

    [Parameter(Mandatory = $false)]
    [string]$Servercsv
)

# Function to write formatted output
function Write-LogMessage {
    param(
        [string]$Message,
        [string]$ComputerName,
        [string]$Color = "White",
        [string]$LogFilePath = "$PSScriptRoot\Log\Falcon_Install.log"    # Default log path
    )
    
    # Create timestamp for the log entry
    $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    
    # Format the log message
    $logEntry = "$timestamp - $ComputerName - $Message"
    
    # Display message in console with color
    Write-Host "$ComputerName - $Message" -ForegroundColor $Color
    
    # Ensure log directory exists
    $logDirectory = Split-Path $LogFilePath -Parent
    if (!(Test-Path $logDirectory)) {
        New-Item -ItemType Directory -Path $logDirectory -Force | Out-Null
    }
    
    # Append message to log file
    Add-Content -Path $LogFilePath -Value $logEntry
}

# Validate sensor path
while (!(Test-Path -Path $SensorPath) -or !($SensorPath.EndsWith(".exe"))) {
    Write-LogMessage -Message "Can't reach path to *.exe to perform copy" -ComputerName "Localhost" -Color Yellow
    $SensorPath = Read-Host "Enter full path to executable"
}

If(!($Servercsv)){
    # Get Domain Controller if not specified
    if (-not $DomainController) {
        $DomainController = Read-Host "Enter DomainController to list servers"
    }
    #Check connection to DomainController
    if (!(Test-NetConnection -ComputerName $DomainController -Port 9389 -InformationLevel Quiet -ErrorAction SilentlyContinue)) {
        Write-LogMessage "Can't connect to port 9389 to list computers from the domaincontroller" -ComputerName "Localhost" -Color Yellow
        continue
    }

    # Get servers from AD
    $servers = Get-ADComputer -Server $DomainController -Filter {
        enabled -eq "true" -and (OperatingSystem -Like '*Windows Server*' -or OperatingSystem -Like 'Windows 10 Enterprise for Virtual Desktops')
    } -Properties * | Sort-Object DNSHostName

    # Show server selection grid
    $selectedServers = $servers | Select-Object DNSHostName, CanonicalName, Description, LastLogonDate, Modified | Out-GridView -PassThru
}
Else{
    $selectedServers=import-csv -Path $Servercsv |?{$_."Active Directory: Fully Qualified Domain Name"}| Select-Object @{N='DNSHostName'; E={$_."Aggregated: Preferred Host Name (FQDN)"}}
}

if($GROUPING_TAGS){
    $GROUPING_TAGS=$GROUPING_TAGS -replace " ",""
}

$selectedServers | ForEach-Object -ThrottleLimit $ThrottleLimit -Parallel {
    # Import the functions into the parallel session
    function Write-LogMessage {
        param(
            [string]$Message,
            [string]$ComputerName,
            [string]$Color = "White",
            [string]$LogFilePath = "$Using:PSScriptRoot\Log\Falcon_Install.log"    # Default log path
        )
        
        # Create timestamp for the log entry
        $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
        
        # Format the log message
        $logEntry = "$timestamp - $ComputerName - $Message"
        
        # Display message in console with color
        Write-Host "$ComputerName - $Message" -ForegroundColor $Color
        
        # Ensure log directory exists
        $logDirectory = Split-Path $LogFilePath -Parent
        if (!(Test-Path $logDirectory)) {
            New-Item -ItemType Directory -Path $logDirectory -Force | Out-Null
        }
        
        # Append message to log file
        Add-Content -Path $LogFilePath -Value $logEntry
    }

    $server = $_
    $TempPath = $using:TempPath 
    $TempPath = $TempPath.TrimEnd('\')
    
    Write-LogMessage "Start processing host" -ComputerName $server.DNSHostName -Color White
    
    if (!(Test-NetConnection -ComputerName $server.DNSHostName -Port 5985 -InformationLevel Quiet -ErrorAction SilentlyContinue)) {
        Write-LogMessage "Can't connect to port 5985 for remote installation" -ComputerName $server.DNSHostName -Color Yellow
        continue
    }
    <#
    if (!(Test-NetConnection -ComputerName $server.DNSHostName -Port 445 -InformationLevel Quiet -ErrorAction SilentlyContinue)) {
        Write-LogMessage "Can't connect to port 445" -ComputerName $server.DNSHostName -Color Yellow
        continue
    }
    #>
    Write-LogMessage "Host available" -ComputerName $server.DNSHostName -Color Gray
    
    # Check if CrowdStrike is already installed
    if (Get-CimInstance -ComputerName $server.DNSHostName -ClassName win32_service | Where-Object { $_.Name -eq "CSFalconService" }) {
        Write-LogMessage "CSFalconService already installed" -ComputerName $server.DNSHostName -Color Yellow
        
        If($using:OverwriteTag){
            #Change tag on Remote machine
            Invoke-command -ComputerName $server.DNSHostName -ScriptBlock {
                param($GROUPING_TAGS) 
                $SensorRegPath="HKLM:\\SYSTEM\CrowdStrike\{9b03c1d9-3138-44ed-9fae-d9f4c034b88d}\{16e0423f-7058-48c9-a204-725362b67639}\Default"
                $itemproperty=Get-ItemProperty -path $SensorRegPath
                if($itemproperty){
                    if(get-member -InputObject $itemproperty -Name "groupingtags"){
                        Write-host -ForegroundColor Green "$env:Computername - Registry path found!"
                        Set-ItemProperty -Path $SensorRegPath -Name "GroupingTags" -Value $GROUPING_TAGS
                    }
                    Else{
                        New-ItemProperty -Path $SensorRegPath -Name "GroupingTags" -PropertyType String  -Value $GROUPING_TAGS
                    }
                }
                else {
                    Write-host -ForegroundColor Yellow "$env:Computername - Registry path not found!"
                }
            } -ArgumentList $using:GROUPING_TAGS
        } 
        
        #If OS > 2012R2 disable or uninstall defender
        if([int]((Get-CimInstance -ClassName Win32_OperatingSystem -Namespace root/cimv2 -ComputerName $server.DNSHostName).BuildNumber) -gt 9600){
            if ($using:DisableDefender) {
                if((Get-CimInstance -ClassName MSFT_MpPreference -Namespace root/microsoft/windows/defender -ComputerName $server.DNSHostName).DisableRealtimeMonitoring -ne $true){
                    try{
                        Invoke-Command -ComputerName $server.DNSHostName -ErrorAction stop -ScriptBlock {
                            Set-MpPreference -DisableRealtimeMonitoring $true -ErrorAction Stop
                        }
                        Write-LogMessage "Defender realtime monitoring is disabled" -ComputerName $server.DNSHostName -Color Green
                    }
                    Catch{
                        Write-LogMessage "Could not disabled Defender realtime monitoring" -ComputerName $server.DNSHostName -Color Yellow
                    }
                }
                else{
                    Write-LogMessage "Defender realtime monitoring is already disabled" -ComputerName $server.DNSHostName -Color Green
                }
            }
            if ($using:UninstallDefender) {
                IF(Get-WindowsFeature Windows-Defender -ComputerName $server.DNSHostName){
                    Try{
                        Invoke-Command -ComputerName $server.DNSHostName -ErrorAction stop -ScriptBlock {
                            Uninstall-WindowsFeature -Name Windows-Defender -ErrorAction stop
                        }
                        Write-LogMessage "Uninstalled Windows Defender" -ComputerName $server.DNSHostName -Color Green
                    }
                    Catch{
                        Write-LogMessage "Failed uninstalling Windows Defender" -ComputerName $server.DNSHostName -Color Yellow
                    }
                }
            }
        }
        Else{
            Write-LogMessage "OS is Windows Server 2012R2 or lower, Defender not present" -ComputerName $server.DNSHostName -Color Gray
        }
        continue
    }
    
    <# Create temp directory if it doesn't exist
    $remoteTempPath = "\\$($server.DNSHostName)\$($TempPath.Replace(':', '$'))"
    if (!(Test-Path -Path $remoteTempPath -ErrorAction SilentlyContinue)) {
        New-Item -ItemType Directory -Path $remoteTempPath -Force
    }
    
    # Copy installer
    $destinationPath = Join-Path $remoteTempPath (Split-Path $using:SensorPath -Leaf)
    try{
        Copy-Item -Path $using:SensorPath -Destination $destinationPath -Force -ErrorAction stop
        if (!(Test-Path -Path $destinationPath)) {
            Write-LogMessage "Failed copying $(Split-Path $using:SensorPath -Leaf)" -ComputerName $server.DNSHostName -Color Yellow
            continue
        }
        Else{
            Write-LogMessage "Copied installer $(Split-Path $using:SensorPath -Leaf) to host" -ComputerName $server.DNSHostName -Color Gray
        }
    }
    Catch{
        Write-LogMessage "Failed copying $(Split-Path $using:SensorPath -Leaf)" -ComputerName $server.DNSHostName -Color Yellow
        continue
    }
    #>

    Write-LogMessage "Downloading and installing CrowdStrike sensor" -ComputerName $server.DNSHostName -Color Gray
  
    ###Remote commands on destination computer
    $result = Invoke-Command -ComputerName $server.DNSHostName -ScriptBlock {
        param($TempPath, $CID, $SensorPath, $GROUPING_TAGS)        
        function Get-FalconInstaller {
            param (
                [Parameter(Mandatory=$true)]
                [string]$ClientId,
                
                [Parameter(Mandatory=$true)]
                [string]$ClientSecret,
                
                [Parameter(Mandatory=$false)]
                [ValidateSet("windows", "mac", "linux")]
                [string]$Platform = "windows"
            )
        
            # EU-1 Base URLs
            $authUrl = "https://api.eu-1.crowdstrike.com/oauth2/token"
            $sensorUrl = "https://api.eu-1.crowdstrike.com/sensors/combined/installers/v1"
            $downloadUrl = "https://api.eu-1.crowdstrike.com/sensors/entities/download-installer/v1"
        
            try {
                Write-Host "Authenticating..." -ForegroundColor Yellow
                # Get OAuth2 token
                $authBody = @{
                    client_id = $ClientId
                    client_secret = $ClientSecret
                    grant_type = "client_credentials"
                }
        
                # Force TLS 1.2
                [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        
                $authResponse = Invoke-RestMethod -Method Post -Uri $authUrl -Body $authBody
                $accessToken = $authResponse.access_token
        
                # Set up headers for subsequent requests
                $headers = @{
                    'Authorization' = "Bearer $accessToken"
                    'Content-Type' = 'application/json'
                }
        
                Write-Host "Getting available installers..." -ForegroundColor Yellow
                # Get available installer versions
                $installerResponse = Invoke-RestMethod -Method Get -Uri "$sensorUrl`?platform=$Platform" -Headers $headers
        
                if (-not $installerResponse.resources) {
                    throw "No installers found for platform: $Platform"
                }
        
                # Get latest installer
                $latestInstaller = $installerResponse.resources | 
                    Sort-Object -Property version -Descending | 
                    Select-Object -First 1
        
                Write-Host "Found latest installer version: $($latestInstaller.version)" -ForegroundColor Yellow
                
                # Get the actual download URL
                $sha256 = $latestInstaller.sha256
                Write-Host "Requesting download URL for SHA256: $sha256" -ForegroundColor Yellow
                
                # Create filename with timestamp
                $timestamp = Get-Date -Format "yyyyMMdd"
                $fileName = "falcon_installer_${Platform}_${timestamp}.exe"
                $outputPath = Join-Path "C:\windows\Temp\" $fileName
        
                # Download installer directly using the download endpoint
                Write-Host "Downloading installer..." -ForegroundColor Yellow
                Invoke-WebRequest -Uri "$downloadUrl`?id=$sha256" -Headers $headers -OutFile $outputPath
        
                Write-Host "Download complete!" -ForegroundColor Yellow
                # Return the full path of downloaded file
                return $outputPath
            }
            catch {
                throw "Error downloading Falcon installer: $($_.Exception.Message)"
            }
        }
        
        try {
            # Replace with your API credentials
            $clientId = "b40367a0c5474beb9e6a9caa5587529e"
            $clientSecret = "ShF2Vi54oGvRegBWLaPZEDlyb3f71t6KXY8T90cs"
        
            $installPath = Get-FalconInstaller -ClientId $clientId -ClientSecret $clientSecret
            Write-Host "Successfully downloaded Falcon installer to: $installPath" -ForegroundColor Green
        }
        catch {
            Write-Host "Error downloading crowdstrike installer: $($_.Exception.Message)" -ForegroundColor Red
        }
        
        # Install Falcon Sensor
        $installerPath = Join-Path $TempPath (Split-Path $SensorPath -Leaf)
        IF($GROUPING_TAGS){
            cmd /c $installerPath /quiet /norestart CID=$CID GROUPING_TAGS=$GROUPING_TAGS /log "C:\windows\Temp\Falcon_$($env:Computername).log"
        }
        Else{
            cmd /c $installerPath /quiet /norestart CID=$CID /log "C:\windows\Temp\Falcon_$($env:Computername).log"
        }
    }  -ArgumentList $using:TempPath, $using:CID, $using:SensorPath, $using:GROUPING_TAGS
    
    #check if Falcon Agent is installed.
    If(Get-CimInstance -ClassName Win32_InstalledWin32Program -ComputerName $server.DNSHostName |?{$_.name -eq "CrowdStrike Windows Sensor"}){
        Write-LogMessage "CrowdStrike sensor installed" -ComputerName $server.DNSHostName -Color Green
        #If OS > 2012R2 disable or uninstall defender
        if([int]((Get-CimInstance -ClassName Win32_OperatingSystem -Namespace root/cimv2 -ComputerName $server.DNSHostName).BuildNumber) -gt 9600){
            if ($using:DisableDefender) {
                if((Get-CimInstance -ClassName MSFT_MpPreference -Namespace root/microsoft/windows/defender -ComputerName $server.DNSHostName).DisableRealtimeMonitoring -ne $true){
                    try{
                        Invoke-Command -ComputerName $server.DNSHostName -ErrorAction stop -ScriptBlock {
                            Set-MpPreference -DisableRealtimeMonitoring $true -ErrorAction Stop
                        }
                        Write-LogMessage "Defender realtime monitoring is disabled" -ComputerName $server.DNSHostName -Color Green
                    }
                    Catch{
                        Write-LogMessage "Could not disabled Defender realtime monitoring" -ComputerName $server.DNSHostName -Color Yellow
                    }
                }
                else{
                    Write-LogMessage "Defender realtime monitoring is already disabled" -ComputerName $server.DNSHostName -Color Green
                }
            }
            if ($using:UninstallDefender) {
                IF(Get-WindowsFeature Windows-Defender -ComputerName $server.DNSHostName){
                    Try{
                        Invoke-Command -ComputerName $server.DNSHostName -ErrorAction stop -ScriptBlock {
                            Uninstall-WindowsFeature -Name Windows-Defender -ErrorAction stop
                        }
                        Write-LogMessage "Uninstalled Windows Defender" -ComputerName $server.DNSHostName -Color Green
                    }
                    Catch{
                        Write-LogMessage "Failed uninstalling Windows Defender" -ComputerName $server.DNSHostName -Color Yellow
                    }
                }
            }
        }
        else {
            Write-LogMessage "OS is Windows Server 2012R2 or lower, Defender not present" -ComputerName $server.DNSHostName -Color Gray
        } 
    }
    Else{
        Write-LogMessage "Installation not found in software list, please check CrowdStrike sensor installation logs." -ComputerName $server.DNSHostName -Color Yellow
         # Copy Log from remote machine
        $log = (get-childItem -Path "\\$($server.DNSHostName)\c$\windows\temp" |?{$_.name -match "Falcon_"})
        $locallog="$Using:PSScriptRoot\Log"
        $log | ForEach-Object{
            try{
                Copy-Item -Path $_.FullName -Destination $(join-path $locallog $_.name) -Force -ErrorAction stop
                if (!(Test-Path -Path $(join-path $locallog $_.name))) {
                    Write-LogMessage "Failed copying $($_.name) to local log directory" -ComputerName $server.DNSHostName -Color Red
                }
                Else{
                    Write-LogMessage "Copied installer $($_.name) to local log directory" -ComputerName $server.DNSHostName -Color Gray
                }
            }
            Catch{
                Write-LogMessage "Failed copying $($_.name) to local log directory" -ComputerName $server.DNSHostName -Color Red
            }   
        }
    }

    # Cleanup
    try{
        #Remove-Item -Path $destinationPath -Force -ErrorAction stop
        Write-LogMessage "Removed installer $(Split-Path $using:SensorPath -Leaf) from host" -ComputerName $server.DNSHostName -Color Gray
    }
    Catch{
        Write-LogMessage "Failed removing installer $(Split-Path $using:SensorPath -Leaf) from host" -ComputerName $server.DNSHostName -Color Yellow
    }
}