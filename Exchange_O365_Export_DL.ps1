Connect-ExchangeOnline

$DL=Get-DistributionGroup
$dl[1]|fl *
$include = ($dl|gm |?{($_.MemberType -eq "Property") -and ($_.Definition -notmatch "Deserialized.Microsoft.Exchange.Data.")}).name
#$dl[1]|select $include

$dl |select $include |Write-SqlTableData -ServerInstance "CL-DS-MG1" -DatabaseName "dbBeheer" -SchemaName "dbo" -TableName "O365_DL" -force
$dl |gm

$Alias_array=@()
foreach($entry in $dl){

    $entry.EmailAddresses|%{
        $Object = new-object -TypeName PSObject -Property @{
            EmailAddress        = $_
            PrimarySmtpAddress  = $entry.PrimarySmtpAddress
            Id                  = $entry.ID
        }
        $Alias_array += $object
    }
}

$Alias_array |Write-SqlTableData -ServerInstance "CL-DS-MG1" -DatabaseName "dbBeheer" -SchemaName "dbo" -TableName "O365_DL_Alias" -force


$Members_array=@()
foreach($List in $DL){
    $list.PrimarySmtpAddress
    $members=Get-DistributionGroupMember -Identity $list.PrimarySmtpAddress |select Name, Recipienttype, displayname, PrimarySmtpAddress
    $members|%{
        $Object = new-object -TypeName PSObject -Property @{
            Name                    = $_.Name
            Recipienttype           = $_.Recipienttype
            displayname             = $_.displayname
            PrimarySmtpAddress      = $_.PrimarySmtpAddress
            DL_name                 = $list.name
            DL_PrimarySmtpAddress   = $list.PrimarySmtpAddress
        }
        $Members_array += $object
    }
}
$dl|?{$_.alias -eq "TDN"}
$Members_array |Write-SqlTableData -ServerInstance "CL-DS-MG1" -DatabaseName "dbBeheer" -SchemaName "dbo" -TableName "O365_DL_Members" -force