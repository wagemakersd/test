﻿$msolcred = get-credential
connect-msolservice -credential $msolcred

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session

Get-mailbox vwd-SMB-00028@vwd.com -archive
Get-mailbox quotes-eu@vwd.com -archive
Get-mailbox hnqiri@vwd.com | set-Mailbox -litigationHoldEnabled $false
Get-mailbox hnqiri@vwd.com | Select RecipientTypeDetails
Get-mailbox hnqiri@vwd.com | Set-Mailbox -Type Regular
Get-Mailbox pvanderkooij | Select RecipientTypeDetails

Get-Mailbox beheer-eu@vwd.com | FL Name,*Archive*

Start-ADSyncSyncCycle -policytype Delta

Start-ManagedFolderAssistant –Identity vwd-SMB-00028@vwd.com
Start-ManagedFolderAssistant –Identity vwd-SMB-00001@vwd.com
Start-ManagedFolderAssistant –Identity pvanderkooij@vwd.com

Export-MailboxDiagnosticLogs –Identity vwd-SMB-00028@vwd.com -ExtendedProperties -Archive

$FixAutoMapping = Get-MailboxPermission funds-eu@vwd.com |where {$_.AccessRights -eq "FullAccess" -and $_.IsInherited -eq $false}
$FixAutoMapping | Remove-MailboxPermission
$FixAutoMapping | ForEach {Add-MailboxPermission -Identity $_.Identity -User $_.User -AccessRights:FullAccess -AutoMapping $false}

Remove-MailboxPermission funds-eu@vwd.com -ClearAutoMapping

Remove-MailboxPermission funds-eu@vwd.com -User pvanderkooij@vwd.com -AccessRights fullaccess
Add-MailboxPermission funds-eu@vwd.com -AccessRights FullAccess -User pvanderkooij@vwd.com -Automapping:$true

Set-Mailbox -Identity nqirih -HiddenFromAddressListsEnabled $false

Get-MailboxServer | Format-Table Name,ManagedFolderWorkCycle* -Auto

Get-Mailbox -Identity vwd-SMB-00028@vwd.com | ft Alias,RetentionPolicy
Set-Mailbox -Identity vwd-SMB-00028@vwd.com -RetentionPolicy "Archive Policy"

Start-transcript "C:\Output.txt"

Get-Mailbox pvanderkooij@vwd.com | FL -RetentionHoldEnabled | Fl

Set-Mailbox vwd-SMB-00001@vwd.com -RetentionHoldEnabled $false

stop-transcript "C:\Output.txt"