﻿
$users = get-aduser -filter {Enabled -eq $False} -properties accountexpires
foreach ($UserToClean in $users)
{
    $samAccountName=$UserToClean.samaccountname
    $homedir=Test-Path "$homedirs\$samAccountName"
    If ($homedir -eq $true)
    {
        "$homedirs\$samAccountName"
        Move-Item -Path "$homedirs\$samAccountName" -Destination "$homedirs\_Archived\$samAccountName"
    }
}

$folders=get-childitem -Path "\\deffm-fs1\d$\office_user"
$notfound=@()
$users=@()
foreach($folder in $folders){
    $foldername=$folder.name
    $user = $null
    $user=get-aduser -filter {Samaccountname -eq $foldername} -Properties userprincipalname
    If(!($user)){
        $notfound += $folder
    }
    else{
        $users+=$user
    }
}
<#
$acl = Get-Acl -Path "$homedirs\_test"
$acl.Access
# get permissions
            $acl = Get-Acl -Path "$homedirs\_test"
            $acl.SetAccessRuleProtection($True, $Flase)
            $acl |Set-Acl



test-path "$homedirs\_test"


        # add a new permission (FullControl, Modify, Read)
            $permission = "svc_tasks", 'DeleteSubdirectoriesAndFiles, Delete, Synchronize', 'ContainerInherit, ObjectInherit', 'None', 'Allow'
            $rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permission
            $acl.SetAccessRule($rule)
            # set new permissions
            $acl | Set-Acl -Path "$homedirs\_test"
#>