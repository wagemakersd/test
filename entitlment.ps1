$json={
  "email": "sample string 1",
  "firstName": "sample string 2",
  "lastName": "sample string 3",
  "phone": "sample string 4",
  "isOnlineOrderPermitted": true,
  "additionalInformation": "sample string 5",
  "userType": "Subscriber",
  "expiryDate": "2023-05-24T13:24:59.2595802Z",
  "address1": "sample string 6",
  "address2": "sample string 7",
  "address3": "sample string 8",
  "postalCode": "sample string 9",
  "postalCity": "sample string 10",
  "countryCode": "sample string 11",
  "mainAccount": 1,
  "alternativeAccount": 1
}

$Cred = Get-Credential
$Url = "https://apitest.infrontservices.com/webapi/Api/PUT-api-1.0-Users-custUserId-Subscriptions-packageI"
$Body = @{
  grant_type = "password"
  username = "dennis.wagemakers"
  password = 'Px6:Er3!@#$$#@'
}
Invoke-RestMethod -Method 'GET' -Uri $url -Body $body
 -OutFile output.csv