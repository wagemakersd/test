﻿#Set recource properties capacity, location, phone
$msolcred = get-credential
connect-msolservice -credential $msolcred

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session

Import-Module LyncOnlineConnector
$cssess=New-CsOnlineSession -Credential $cred
Import-PSSession $cssess -AllowClobber

$DomainController = "deffm-dc1"
$Identity = "AMS-RES-00006"
$Capicity = "4"
$Location = "Amsterdam"
$Telephone = ""
Set-Mailbox -Identity $Identity -ResourceCapacity $Capicity
Set-ADUser -Identity $Identity -Add @{PhysicalDeliveryOfficeName = "$Location"} -Server $DomainController -ErrorAction Stop
if($Telephone){
Set-ADUser -Identity $Identity -Add @{telephoneNumber = "$Telephone"} -Server $DomainController -ErrorAction Stop
}


Set-Mailbox -Identity "ANT-RES-00006" -ResourceCapacity 7 -physicalDeliveryOfficeName Antwerpen -telephoneNumber

Get-Mailbox -Identity "AMS-RES-00003" | Select -physicalDeliveryOfficeName

Get-Mailbox -Identity "AMS-RES-00001" | fl * | Out-File -filepath C:\temp\ams-res-00001.txt

Get-Mailbox -Identity "TEST-ROOM-PIM" | fl *
Enable-RemoteMailbox "TEST-ROOM-PIM"  -RemoteRoutingAddress "TEST-ROOM-PIM@vwd.com"

Set-RemoteMailbox -Identity 'TEST-ROOM-PIM' -EnableRoomMailboxAccount $true -RoomMailboxPassword (ConvertTo-SecureString -String "" -AsPlainText -Force)



Set-Mailbox -Identity 'TEST-ROOM-PIM' -EnableRoomMailboxAccount $true -RoomMailboxPassword (ConvertTo-SecureString -String "" -AsPlainText -Force)

Get-ADuser -Identity "AMS-RES-00003" -Properties PhysicalDeliveryOfficeName | Select-Object -ExpandProperty physicaldeliveryofficename

Get-ADuser -Identity "AMS-RES-00003" -Properties telephoneNumber | Select-Object -ExpandProperty telephoneNumber

Get-ADuser -Identity "AMS-RES-00003" -Properties * | Out-File -filepath C:\temp\ams-res-00003.txt

Get-Mailbox -Identity "ANT-RES-00006" | Select ResourceCapacity

Get-Mailbox -ResultSize unlimited -Filter {(RecipientTypeDetails -eq 'RoomMailbox')} | Select name,alias,phonenumber | export-csv c:\temp\recources.csv | FORMAT-TABLE -autosize


Get-ADUser -Identity yangwe -Properties AccountExpirationDate |
Select-Object -Property SamAccountName, AccountExpirationDate

Set-ADAccountExpiration -Identity yangwe -DateTime '04/12/2017 12:00:00'

Start-ADSyncSyncCycle -policytype Delta

Get-CalendarProcessing -Identity "ams-res-00001" | fl *

Get-MSOLUser -UserPrincipalName pvanderkooij.ext@vwd.com | fl


Get-ADUser -Identity wagemakersd -Properties *



Get-CsUser "pvanderkooij.ext"

