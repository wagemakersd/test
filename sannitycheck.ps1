Connect-AzureAD
Connect-ExchangeOnline -UserPrincipalName $UPN -ShowProgress $true

$vwd_AD=(get-aduser -filter {(userprincipalname -like "*@vwd.com")} -properties *)
$vwd_OutOfScope = $vwd_AD | Where-Object {$_.extensionAttribute3 -notmatch "T2T_Project"}
$Azure = Get-AzureADUser -All $true

$result = @()
ForEach ($entry in $vwd_OutOfScope) {
    $Recipient = $null
    $Licenses = $null
    
    $Licenses = $(($Azure | where-object {$_.userprincipalname -eq $entry.userprincipalname}).assignedplans.Service) -join ";"
    $Recipient = Get-Recipient -Identity $entry.UserPrincipalName -ErrorAction SilentlyContinue


    $temp = New-Object -typename PSObject -Property @{
        UserPrincipalName       = $entry.UserPrincipalName
        Primarysmtpaddress      = $Recipient.Primarysmtpaddress
        Samaccountname          = $entry.Samaccountname
        Name                    = $entry.Name
        Description             = $entry.Description
        CanonicalName           = $entry.CanonicalName
        Enabled                 = $entry.Enabled
        RecipientTypeDetails    = $Recipient.RecipientTypeDetails
        ExchangeLicense         = If ($Licenses -match "exchange") {$true} else {$false}     
        All_Licenses            = $Licenses
        extensionAttribute1     = $entry.extensionAttribute1
        extensionAttribute2     = $entry.extensionAttribute2
        extensionAttribute3     = $entry.extensionAttribute3
    }
    $result += $temp  
}
$result| ft UserPrincipalName,Samaccountname,Primarysmtpaddress,Name,Enabled,ExchangeLicense,RecipientTypeDetails,All_Licenses
$process=$result|?{($_.RecipientTypeDetails -ne $null) -and ($_.UserPrincipalName -match "-res-")}
$result|?{($_.RecipientTypeDetails -eq $null) -and ($_.UserPrincipalName -match "-res-")}

$process=$result|?{($_.Samaccountname -match "_") -and ($_.CanonicalName -match "Disabled")}

foreach($item in $process)
{
        $newmail=$item.Userprincipalname -replace "@vwd.com","@infrontfinance.com"
        set-aduser -identity $item.Samaccountname -replace @{
            extensionAttribute1=$newmail
            extensionAttribute2="Disabled"
            extensionAttribute3="T2T_Project"
        }
        Add-ADGroupMember -Identity "vwd-LST-00317" -members $item.samaccountname
        $newmail
}
get-aduser FFM-RES-0051 -properties *|select extensionAttribute*



$adusers=get-aduser -filter {(userprincipalname -like "*@vwd.com") -and (enabled -eq $false)} -properties *|?{($_.userprincipalname -match "_") -and ($_.CanonicalName -match "Disabled")}
$total1=@()
$failed1=@()
foreach ($user in $adusers)
{
    try
    {
        $mail=get-user $user.userprincipalname -ErrorAction stop
        #$mail=get-mailbox -identity $user.userprincipalname -ErrorAction stop
        $total1+=$mail#|select  RecipientTypeDetails,LitigationHoldEnabled,PrimarySmtpAddress,Extension,CustomAttribute1,CustomAttribute2,CustomAttribute3
    }
    catch
    {
        $failed1+=$user
    }
}

$total1|?{$_.RecipientType -ne "UserMailbox"}|FL *

$adusers.count
$total.count
$failed
foreach ($u in $failed)
{
    (get-aduser $u.samaccountname).
}

$total| FT
$failed| FT Name


$remove=get-content c:\temp\remove.txt
foreach ($r in $remove)
{
    If ($r -match "@vwd.com")
    {
        $newupn=$r -replace "transactionsolutions.de","@vwdgroup.net"
        $upn=$r -replace "transactionsolutions.de","@vwd.com"
        get-aduser -filter {UserPrincipalName -eq $upn}|set-aduser -replace @{UserPrincipalName=$newupn}
        get-aduser -filter {Userprincipalname -eq $newupn}
    }
}
$users=@()
get-content c:\temp\list.txt|%{
    (get-adgroup -filter {displayname -eq $_})|get-adgroupmember|%{
        $users+=get-aduser $_ -properties *|select Userprincipalname,extensionAttribute1,extensionAttribute2,extensionAttribute3
    }
}

$users|?{$_.extensionAttribute1 -eq $null}
8+34+43+22+13+207+277+21+50+155

$adusers=@()
get-content c:\temp\missing.txt|%{
    $adusers+=get-aduser -filter{samaccountname -eq $_} -properties * #|select Userprincipalname,extensionAttribute1,extensionAttribute2,extensionAttribute3,memberof
}
$adusers|select Userprincipalname,extensionAttribute1,extensionAttribute2,extensionAttribute3|export-csv -path c:\temp\missing_users.csv -notypeinformation
$adusers.count
$adusers
(get-content c:\temp\missing.txt).count
$list=(get-content c:\temp\missing.txt)
compare-object $list $adusers.samaccountname
(compare-object $list $adusers.samaccountname|?{$_.inputobject -match "[[SMB]]"}).inputobject|%{
    $adusers+=get-aduser -filter{name -eq $_} -properties * #|select Userprincipalname,extensionAttribute1,extensionAttribute2,extensionAttribute3,memberof
}

get-aduser -filter{userprincipalname -eq "cbeer@vwd.com"}|set-aduser -replace @{
    extensionAttribute1="Christina.Beer@infrontfinance.com"
    extensionAttribute2="remaining"
    extensionAttribute3="T2T_Project"
}

$nomatch = get-content c:\temp\mailusers.txt
$prep=@()
$nomatch|%{
    $_
    $aduser=get-aduser -filter {samaccountname -eq $_} -properties *
    $object=New-Object -typename PSObject -property @{
        Name                        = $aduser.displayname
        ExternalEmailAddress        = $aduser.extensionAttribute1
        MicrosoftOnlineServicesID   = $aduser.extensionAttribute1
        Password                    = "NLAmsterdam2020#"
    }
    $object
    $prep+=$object
}
connect-msolservice
connect-exchangeonline -UserPrincipalName "dewagadm@infrontonline.onmicrosoft.com"
$prep| ForEach-Object {
    #get-recipient $_.MicrosoftOnlineServicesID
    New-MailUser -Name $_.Name -ExternalEmailAddress $_.MicrosoftOnlineServicesID -MicrosoftOnlineServicesID $_.MicrosoftOnlineServicesID -Password (ConvertTo-SecureString -String $_.Password -AsPlainText -Force)
    }
    
    $prep=$prep|?{$_.MicrosoftOnlineServicesID -ne $null}
    foreach ($user in $prep){
        Get-Recipient $user.MicrosoftOnlineServicesID |select Name,WhenCreated
    }

    Get-msoluser -UserPrincipalName Roy.Betschart.ext@infrontfinance.com| FL *
    $prep|?{$_.MicrosoftOnlineServicesID -ne $null}

$smbremovelic=get-content c:\temp\smb_removelic.txt
$smbremovelic.count
$fals=@()
$smbremovelic|%{
    (Get-MsolUser -UserPrincipalName $_)|?{$_.isLicensed -eq $true}<#|%{
        Get-Recipient $_.userprincipalname |?{$_.RecipientTypeDetails -eq "SharedMailbox"}|select Name,RecipientTypeDetails
        }
                    $License = (Get-MsolUser -UserPrincipalName $_).Licenses.AccountSkuId
                    foreach($lic in $license)
                    {
                        try 
                        {
                            Set-MsolUserLicense -UserPrincipalName $_ -RemoveLicenses $Lic -ErrorAction Stop
                        }
                        catch 
                        {
                            write-host "failed remove for $_"
                        }
                    }#>
                }



get-mailbox svonahnen_s7v63@infrontfinance.com | select *type*
connect-msolservice
$litigationhold=@()
$all|?{($_.WindowsLiveID -match "_")}|%{
    $litigationhold+=Get-MsolUser -UserPrincipalName $_.WindowsLiveID#|?{$_.isLicensed -eq $true}
}



($litigationhold|?{$_.isLicensed -eq $true}).count
$all|?{($_.WindowsLiveID -match "_")}|sort RecipientTypeDetails|FT lit*,WindowsLiveID,*typ*
$litigationhold.count


$all|?{($_.WindowsLiveID -match "_")}|%{
    $litigationhold+=Get-MsolUser -UserPrincipalName $_.WindowsLiveID|?{$_.isLicensed -eq $true}
}

|select Name,WindowsLiveID,RecipientTypedetails|sort RecipientTypedetails

Get-Recipient ams-smb-00003|fl *

connect-exchangeonline -UserPrincipalName "wagemakersd-adm@vwd.com"
$missingsmtp=get-content c:\temp\missing_smtp.txt
$rec=@()
$missingsmtp|%{
    $rec+=Get-Recipient $_
}
$rec|select DisplayName,RecipientTypedetails, CustomAttribute3
$all|select name,displayname


















get-casmailbox weigeltm |select name,displayname
Get-Recipient weigeltm |select name,displayname

$msolusers=get-msoluser -all
($msolusers|?{$_.ImmutableId -eq $null}).count
($msolusers|?{$_.ImmutableId -eq $null})[1]
|%{
    Get-Recipient Marco.Cariati@infrontfinance.com|fl *


    connect-msolservice


    get-msoluser -all |?{($_.UserPrincipalName -match "_") -and ($_.isLicensed -eq $true)}
    
    $allvwd=get-recipient -resultsize unlimited
    $vwdLicensed=@()
    $allvwd|?{$_.recipienttypedetails -eq "SharedMailbox"}|%{
        $_.WindowsLiveID
        $msol=get-msoluser -UserPrincipalName $_.WindowsLiveID|?{$_.isLicensed -eq $true}
        $msol|Add-Member -NotePropertyName CustomAttribute1 -NotePropertyValue $_.CustomAttribute1
        $msol|Add-Member -NotePropertyName ExtensionCustomAttribute1 -NotePropertyValue $_.ExtensionCustomAttribute1
        $vwdLicensed+=$msol
    }

        $vwdLicensed|select userprincipalname,displayname,CustomAttribute1|export-csv c:\temp\licensed_smb.csv -notypeinformation -encoding UTF8
        $allvwd[1]|fl *


        $litigationhold=$allvwd|?{($_.RecipientTypeDetails -match "SharedMailbox") -and ($_.LitigationHoldEnabled -eq $true)}
        $litigationhold | select lit*,CustomAttribute1|export-csv c:\temp\LitigationHoldEnabled.csv -notypeinformation -encoding UTF8

$csv=import-csv -path C:\temp\synced.csv
$changed=@()
        $all|?{($_.RecipientTypedetails -eq "usermailbox") -and ($_.WindowsLiveID -match "_")}|select name,RecipientTypedetails,WindowsLiveID

        $all|?{($_.RecipientTypedetails -ne "usermailbox") -and ($_.WindowsLiveID -match "_")}|%{
            If ($_.WindowsLiveID -in $litigationhold.CustomAttribute1)
            {
                If ($_.WindowsLiveID -in $csv.TargetUPN)
                {
                    $changed+=$_
                }
            }
        }
        $change|select name,RecipientTypedetails,WindowsLiveID
        $changed|select name,RecipientTypedetails,WindowsLiveID

        $failed=@()
        $success=@()
        foreach($entry in $litigationhold)
        {
            If($entry.CustomAttribute1 -in $csv.TargetUPN)
            {
                try
                {
                    $success+=get-recipient $entry.CustomAttribute1 -ErrorAction stop
                }
                catch
                {
                    $failed+=$entry
                }
            }
        }
        $success|select name,RecipientTypedetails,WindowsLiveID,LitigationHoldEnabled
        $success.count
        $success|?{($_.RecipientTypeDetails -eq "SharedMailbox") -and ($_.LitigationHoldEnabled -eq $true)}|%{
            get-msoluser -UserPrincipalName $_.WindowsLiveID |?{$_.isLicensed -eq $true}|%{
                $_.UserPrincipalName
                $License = (Get-MsolUser -UserPrincipalName $_.UserPrincipalName).Licenses.AccountSkuId
                    foreach($lic in $license)
                    {
                        $lic
                        try 
                        {
                            Set-MsolUserLicense -UserPrincipalName $_.UserPrincipalName -RemoveLicenses $Lic -ErrorAction Stop
                        }
                        catch 
                        {
                            write-host "Failed for $($_.UserPrincipalName)"
                        }
                    }
            }
        }

        $msol=$failed|%{get-msoluser -UserPrincipalName $_.customattribute1}
        foreach($user in $msol)
        {
            get-mailbox $user.UserPrincipalName
        }
        get-aduser -filter {userprincipalname -eq "hvassbotn.ext@vwd.com"}
        connect-msolservice


        $failed=@()
        $success=@()
        $smbs=$allvwd|?{($_.RecipientTypeDetails -match "SharedMailbox")}
        foreach($entry in $smbs)
        {
            If($entry.CustomAttribute1 -in $csv.TargetUPN)
            {
                try
                {
                    $success+=get-recipient $entry.CustomAttribute1 -ErrorAction stop
                }
                catch
                {
                    $failed+=$entry
                }
            }
        }

        ($success|?{($_.RecipientTypeDetails -eq "SharedMailbox")})|%{
            If(!($_.userprincipalname -eq $vwdLicensed.userprincipalname))
            {
                get-msoluser -UserPrincipalName $_.customattribute1
            }
        }
        $vwdLicensed.userprincipalname


        connect-msolservice