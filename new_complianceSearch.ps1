# Create Compliance Search - Export Email
#Cun529661
$username = "ams-admin@vwd365.onmicrosoft.com"
$password = ConvertTo-SecureString "X7h!741QQVX%u^kP" -AsPlainText -Force
$credential = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $password

$SccSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.compliance.protection.outlook.com/powershell-liveid/ -Credential $credential -Authentication "Basic" -AllowRedirection
Import-PSSession $SccSession -Prefix cc

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.compliance.protection.outlook.com/powershell-liveid/ -Credential $credential -Authentication Basic –AllowRedirection
Import-PSSession $Session
Connect-MsolService –Credential $credential



$SearchName = "Export - " + $term.Name
New-ComplianceSearch -ExchangeLocation ITSupport@vwd365.onmicrosoft.com -Name $SearchName
New-ComplianceSearch -ExchangeLocation ITSupport@vwd365.onmicrosoft.com -Name Support
# Start Compliance Search and wait to complete
$SearchName="AMSOPS"
Start-ComplianceSearch $SearchName
do
    {
        Start-Sleep -s 5
        $complianceSearch = Get-ComplianceSearch $SearchName
    }
while ($complianceSearch.Status -ne 'Completed')

# Create Compliance Search in exportable format
New-ComplianceSearchAction -SearchName $SearchName -Export -ArchiveFormat PerUserPST -EnableDedupe $true 
$ExportName = $SearchName + "_Export"

#Wait for Export to complete
do
    {
        Start-Sleep -s 5
        $complete = Get-ComplianceSearchAction -Identity $ExportName
    }
while ($complete.Status -ne 'Completed')