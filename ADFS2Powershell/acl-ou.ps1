﻿$OU="AD:\OU=nl,DC=vwdgroup,DC=net"
$ID="svc_tasks"

$acls=Get-Acl -Path "$OU" | Select-Object -ExpandProperty Access |?{$_.identityreference -match "$ID"}
$customarray=@()

Foreach($acl in $acls)
{
    $Objecttype=$acl.ObjectType
    $RawGuid = ([guid]"$Objecttype").toByteArray();
    $object=get-adobject -fi {schemaIDGUID -eq $rawGuid} -SearchBase (Get-ADRootDSE).schemaNamingContext -prop schemaIDGUID | Select name,@{Name='schemaIDGUID';Expression={[guid]$_.schemaIDGUID}}

    $customobj = New-Object PSObject
        $customobj | Add-Member -MemberType NoteProperty -Name “Identity” -Value $($acl.identityreference)
        $customobj | Add-Member -MemberType NoteProperty -Name “Object” -Value $($object.name)
        $customobj | Add-Member -MemberType NoteProperty -Name “Access” -Value $($acl.AccessControlType)
        $customobj | Add-Member -MemberType NoteProperty -Name “Inherited” -Value $($acl.IsInherited)

        $customarray += $customobj

}

$customarray | ft -AutoSize