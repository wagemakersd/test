Remove-Variable * -ErrorAction SilentlyContinue

#Needed to execute commands: Microsoft Online Services Sig-in and Windows Azure Active Directory Module
$user="ams-admin@vwd365.onmicrosoft.com"
$encrypted="01000000d08c9ddf0115d1118c7a00c04fc297eb0100000082b1a94d25ef6f4293cffb419dc60df50000000002000000000003660000c0000000100000006d14ca110f84a8cdf0e67a91ade8f2450000000004800000a00000001000000017f8276331de1c142ec17def86403f713000000086da6f1bed30f68c0203f862ac1daffd506a4537469b5a31a8a1207be35b8b0452851bfb66e9ad59340d8a272cb68fd214000000c7a1401e6ffd2592685878bd8c9a206cb8c0be68"
$password=ConvertTo-SecureString -string $encrypted
$msolcred = new-object -typename System.Management.Automation.PSCredential -argumentlist $user,$password
connect-msolservice -credential $msolcred

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session

# create User
$givenName=read-host "First Name"
$surName=read-host "Last Name"
$displayname = "$surName, $givenName"

$surName2 = $surName -replace "\s", ""
$emailprefix = $givenName.Substring(0,1)+"$surName2"
$emailprefix = $emailprefix.ToLower()
$UserPrincipalName = "$emailprefix"+"@vwd.com"

$DomainController = 'deffm-dc1'

$samAccountName = $surName2+$givenName.Substring(0,1)
$activeuser= Get-aduser -filter {sAMAccountName -eq $samAccountName} -Server $DomainController
If($activeuser -ne $null)
    {
	write-warning "The username $samAccountName already exists."
    $edit=read-host "Edit existing user (Y/N)"
    If ($edit -eq "Y")
        {
        $location = read-host "Location (f.e. DE/NL)"
        $prefix=read-host "Change current email prefix $emailprefix (Y/N)"
        If ($prefix -eq "Y")
            {
            $emailprefix = Read-host "Please supply new emailprefix"
            }
        }
    Elseif ($edit -eq "N" -or $edit -eq $Null)
        {
	    $samAccountName = Read-host "Please supply new username"
        $ffm=read-host "Location ffm (Y/N)"
        If ($ffm -eq "Y")
            {
            $targetOU = read-host "Department OU (f.e. Content)"
            If ($targetOU)
                {
                If($targetOU -notmatch "OU=de,DC=vwdgroup,DC=net")
                    {
                    $targetOU = "OU="+"$targetOU"+",OU=users,OU=ffm,OU=de,DC=vwdgroup,DC=net"
                    }
                }
            Else
                {
                $targetOU = "OU=users,OU=ffm,OU=de,DC=vwdgroup,DC=net"
                }
            }
        Else
            {
                $targetOU = read-host "OU (f.e. OU=users,OU=ffm,OU=de,DC=vwdgroup,DC=net)"
                If (!($targetOU))
                    {
                    $targetOU = "OU=users,OU=ffm,OU=de,DC=vwdgroup,DC=net"
                    }
            }
        If ($ffm -eq "N")
            {
            $location = read-host "Location (f.e. DE/NL)"
            }
        Else
            {
            $location="DE"
            }
        $activeprefix= Get-aduser -filter {UserPrincipalName -eq $UserPrincipalName} -Server $DomainController
        If ($activeprefix)
            {
            write-warning "The prefix $emailprefix already exists."
		    $emailprefix = Read-host "Please supply new emailprefix"
	        }
        $prefix=read-host "Change current email prefix $emailprefix (Y/N)"
        If ($prefix -eq "Y")
            {
            $emailprefix = Read-host "Please supply new emailprefix"
            }
        }
    Else
        {
        Write-Warning "No valid entry supplied..."
        exit
        }
    }

$License = Read-host "E3 or Exchange"
If ($license -eq "E3")
{
    $license = "E3"
}
Else
{
    $license = "Exchange"
}

$vwdEmail = "$emailprefix"+"@vwd.com"
$samAccountName = $samAccountName.ToLower()


"`n"
"Givenname:`t`t$givenName"
"Surname:`t`t$surName"
"Displayname:`t`t$displayname"
"sAMAccountName:`t`t$samAccountName"
"EmailPrefix:`t`t$emailprefix"
"Email:`t`t`t$vwdEmail"
"Target OU:`t`t$targetOU"
"Location:`t`t$location"
"License:`t`t$License"
"`n"
$correct=Read-host "Is the above correct (Y/N)"
If ($correct -eq "Y")
{
    If ($edit -eq "N")
    {
        new-aduser -name $samAccountName -givenName $givenName -surName $surName -displayname $displayname -samaccountname $samAccountName -UserPrincipalName $vwdEmail -server $DomainController -Path $targetOU -ErrorAction Stop
        Set-aduser -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
    }
    Elseif ($edit -eq "Y")
    {
        Set-aduser -Identity $samAccountName -Replace @{UserPrincipalName = "$vwdEmail"} -Server $DomainController -ErrorAction Stop
        Set-aduser -Identity $samAccountName -Replace @{displayName = "$displayname"} -Server $DomainController -ErrorAction Stop
        Set-aduser -Identity $samAccountName -Replace @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
    }
    Set-aduser -Identity $samAccountName -Add @{ProxyAddresses = "sip:$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Replace @{MailNickName = "$emailprefix"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Replace @{Mail = "$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -add @{msExchUsageLocation = "$location"} -Server $DomainController -ErrorAction Stop
    Write-Host -ForegroundColor Green "AD Account has been created"
    Write-Host -ForegroundColor DarkYellow "Synchronizing account to O365, wait 60 seconds"
    $sync=Start-ADSyncSyncCycle
    sleep 60
    $User = Get-MsolUser -UserPrincipalName $vwdEmail -ErrorAction SilentlyContinue
    If ($User -ne $Null) 
    { 
        If ($License -eq "E3")
        {
            #ENTERPRISE E3 License (only needed serviceplans)
            Set-MsolUserLicense -UserPrincipalName $vwdEmail -AddLicenses vwd365:ENTERPRISEPACK -ErrorAction SilentlyContinue
            $serviceplan = New-MsolLicenseOptions -AccountSkuId vwd365:ENTERPRISEPACK -DisabledPlans FORMS_PLAN_E3,STREAM_O365_E3,Deskless,FLOW_O365_P2,POWERAPPS_O365_P2,TEAMS1,PROJECTWORKMANAGEMENT,SWAY,INTUNE_O365,YAMMER_ENTERPRISE,RMS_S_ENTERPRISE,SHAREPOINTWAC,SHAREPOINTENTERPRISE
            Set-MsolUserLicense -UserPrincipalName $vwdEmail –LicenseOptions $serviceplan
        }
        Else
        {
            #EXCHANGE AND SKYPE ONLY License
            Set-MsolUserLicense -UserPrincipalName $vwdEmail -AddLicenses vwd365:EXCHANGEENTERPRISE  -ErrorAction SilentlyContinue
            Set-MsolUserLicense -UserPrincipalName $vwdEmail -AddLicenses vwd365:MCOSTANDARD  -ErrorAction SilentlyContinue
        }
    } 
    Else 
    {
        Write-Host -ForegroundColor DarkYellow "STILL synchronizing account to O365, wait another 60 seconds"
        $sync=Start-ADSyncSyncCycle
        sleep 60
        $User = Get-MsolUser -UserPrincipalName $vwdEmail -ErrorAction SilentlyContinue
        If ($User -ne $Null) 
        { 
            If ($License -eq "E3")
            {
                #ENTERPRISE E3 License (only needed serviceplans)
                Set-MsolUserLicense -UserPrincipalName $vwdEmail -AddLicenses vwd365:ENTERPRISEPACK -ErrorAction SilentlyContinue
                $serviceplan = New-MsolLicenseOptions -AccountSkuId vwd365:ENTERPRISEPACK -DisabledPlans FORMS_PLAN_E3,STREAM_O365_E3,Deskless,FLOW_O365_P2,POWERAPPS_O365_P2,TEAMS1,PROJECTWORKMANAGEMENT,SWAY,INTUNE_O365,YAMMER_ENTERPRISE,RMS_S_ENTERPRISE,SHAREPOINTWAC,SHAREPOINTENTERPRISE
                Set-MsolUserLicense -UserPrincipalName $vwdEmail –LicenseOptions  $serviceplan
            }
            Else
            {
                #EXCHANGE AND SKYPE ONLY License
                Set-MsolUserLicense -UserPrincipalName $vwdEmail -AddLicenses vwd365:EXCHANGEENTERPRISE  -ErrorAction SilentlyContinue
                Set-MsolUserLicense -UserPrincipalName $vwdEmail -AddLicenses vwd365:MCOSTANDARD  -ErrorAction SilentlyContinue
            }
        } 
        Else 
        {
            Write-Host -ForegroundColor Red "FAILED: O365 Account has not been created"
        }
    }
    PAUSE
}
Else
{
    Write-Warning "Account has not been created"
    PAUSE
}

Remove-PSSession $Session