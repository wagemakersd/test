Try {
    if(Test-Path "HKLM://SOFTWARE/ShareX"){
        if((Get-item "HKLM://SOFTWARE/ShareX").property -eq "PersonalPath"){
            if(Get-ItemProperty "HKLM://SOFTWARE/ShareX" |?{$_.PersonalPath -eq "%userprofile%\ShareX"}){
                write-host "PersonalPath found and set correctly, do nothing."
            }
            else {
                write-warning "PersonalPath found but not pointing to the correct location"
                #Change PersonalPath location
                Set-ItemProperty -Path "HKLM://SOFTWARE/ShareX" -Name PersonalPath -Value "%userprofile%\ShareX"
            }
        }
        Else{
            write-warning "HKLM://SOFTWARE/ShareX found but PersonalPath notfound"
            #create PersonalPath and location
            New-ItemProperty -Path "HKLM://SOFTWARE/ShareX" -Name PersonalPath -Value "%userprofile%\ShareX" -PropertyType String
        }
    }
    Else{
        write-warning "HKLM://SOFTWARE/ShareX not found"
        #Create ShareX, PersonalPath and location
        new-item  -Path "HKLM://SOFTWARE/ShareX" 
        New-ItemProperty -Path "HKLM://SOFTWARE/ShareX" -Name PersonalPath -Value "%userprofile%\ShareX" -PropertyType String
    }
} 
Catch {
    Write-Error $_.Exception
}