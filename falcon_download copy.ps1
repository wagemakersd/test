function Get-FalconInstaller {
    param (
        [Parameter(Mandatory=$true)]
        [string]$ClientId,
        
        [Parameter(Mandatory=$true)]
        [string]$ClientSecret,
        
        [Parameter(Mandatory=$false)]
        [ValidateSet("windows", "mac", "linux")]
        [string]$Platform = "windows"
    )

    # EU-1 Base URLs
    $authUrl = "https://api.eu-1.crowdstrike.com/oauth2/token"
    $sensorUrl = "https://api.eu-1.crowdstrike.com/sensors/combined/installers/v1"
    $downloadUrl = "https://api.eu-1.crowdstrike.com/sensors/entities/download-installer/v1"

    try {
        Write-Host "Authenticating..." -ForegroundColor Yellow
        # Get OAuth2 token
        $authBody = @{
            client_id = $ClientId
            client_secret = $ClientSecret
            grant_type = "client_credentials"
        }

        # Force TLS 1.2
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

        $authResponse = Invoke-RestMethod -Method Post -Uri $authUrl -Body $authBody
        $accessToken = $authResponse.access_token

        # Set up headers for subsequent requests
        $headers = @{
            'Authorization' = "Bearer $accessToken"
            'Content-Type' = 'application/json'
        }

        Write-Host "Getting available installers..." -ForegroundColor Yellow
        # Get available installer versions
        $installerResponse = Invoke-RestMethod -Method Get -Uri "$sensorUrl`?platform=$Platform" -Headers $headers

        if (-not $installerResponse.resources) {
            throw "No installers found for platform: $Platform"
        }

        # Get latest installer
        $latestInstaller = $installerResponse.resources | 
            Sort-Object -Property version -Descending | 
            Select-Object -First 1

        Write-Host "Found latest installer version: $($latestInstaller.version)" -ForegroundColor Yellow
        
        # Get the actual download URL
        $sha256 = $latestInstaller.sha256
        Write-Host "Requesting download URL for SHA256: $sha256" -ForegroundColor Yellow
        
        # Create filename with timestamp
        $timestamp = Get-Date -Format "yyyyMMdd"
        $fileName = "falcon_installer_${Platform}_${timestamp}.exe"
        $outputPath = Join-Path $PWD $fileName

        # Download installer directly using the download endpoint
        Write-Host "Downloading installer..." -ForegroundColor Yellow
        Invoke-WebRequest -Uri "$downloadUrl`?id=$sha256" -Headers $headers -OutFile $outputPath

        Write-Host "Download complete!" -ForegroundColor Yellow
        # Return the full path of downloaded file
        return $outputPath
    }
    catch {
        throw "Error downloading Falcon installer: $($_.Exception.Message)"
    }
}

# Example usage
try {
    # Replace with your API credentials
    $clientId = "b40367a0c5474beb9e6a9caa5587529e"
    $clientSecret = "ShF2Vi54oGvRegBWLaPZEDlyb3f71t6KXY8T90cs"

    $installerPath = Get-FalconInstaller -ClientId $clientId -ClientSecret $clientSecret
    Write-Host "Successfully downloaded Falcon installer to: $installerPath" -ForegroundColor Green
}
catch {
    Write-Host "Error: $($_.Exception.Message)" -ForegroundColor Red
}


