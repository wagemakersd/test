# create User
$givenName=read-host "First Name"
$surName=read-host "Last Name"
$displayname = "$surName, $givenName"
$emailprefix = "$givenName.Substring(0,1)"+"$surName"

$DomainController = 'deffm-dc1'
$targetOU = read-host "OU (f.e. OU=Content,OU=users,OU=ffm)"
$targetOU = "$targetOU"+",OU=de,DC=vwdgroup,DC=net"
$location = read-host "Location (f.e. DE/NL")

If ($surName.length -ge 9)
{
	$samAccountName = "$surName.Substring(0,9)"+"$givenName.Substring(0,1)"
	$activeuser= Get-aduser -Identity $samAccountName -Server $DomainController
	If($activeuser)
	{
		write-warning "The username $samAccountName already exists."
		$samAccountName = Read-host "Please supply new username"
		$emailprefix = Read-host "Please supply new emailprefix"
	}
}
Else
{
	$samAccountName = "$surName"+"$givenName"
	$samAccountName = "$samAccountName.Substring(0,10)"
	$activeuser= Get-aduser -Identity $samAccountName -Server $DomainController
	$activeuser
	If($activeuser)
	{
		write-warning "The username $samAccountName already exists."
		$samAccountName = Read-host "Please supply new username"
		$emailprefix = Read-host "Please supply new emailprefix"
	}
}
$vwdEmail = "$emailprefix"+"@vwd.com"

$givenName
$surName
$displayname
$samAccountName

<#
new-aduser -name $samAccountName -givenName $givenName -surName $surName -displayname $displayname -samaccountname $samAccountName -UserPrincipalName $vwdEmail -server $DomainController -Path $targetOU -ErrorAction Stop
Set-aduser -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
Set-aduser -Identity $samAccountName -Replace @{MailNickName = "$emailprefix"} -Server $DomainController -ErrorAction Stop
Set-aduser -Identity $samAccountName -Replace @{Mail = "$vwdEmail"} -Server $DomainController -ErrorAction Stop
Set-aduser -Identity $samAccountName -add @{MsExchUsageLocation = $location} -Server $DomainController -ErrorAction Stop
#>