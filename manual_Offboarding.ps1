Function Remove-AzureLicenses ($LicenseUser){
    $License = Get-MgUserLicenseDetail -UserID $LicenseUser.UserPrincipalName
    foreach($lic in $license)
    {  
        Try
        {
            Set-MgUserLicense -UserId $LicenseUser.UserPrincipalName -RemoveLicenses $lic.SkuId -AddLicenses @() 
            Add-Content $LOG "removed $($lic.SkuPartNumber) license for: $($LicenseUser.DisplayName)"
            $license_revoked += $LicenseUser
        }
        Catch
        {
            $fail += "<br>Failed removing $($lic.SkuPartNumber) license for: $($LicenseUser.DisplayName) <BR>"
            Add-Content $LOG "Failed removing $($lic.SkuPartNumber) license for: $($LicenseUser.DisplayName)"
        }
    }
}

Remove-Variable * -ErrorAction SilentlyContinue

$Thumbprint="45d2e0b4238489cdb93295996483305202ba50f6"
$Tenant="23371993-8e9a-4de5-97db-90d3e293e863"
$Org="infrontonline.onmicrosoft.com"
$App="cc8469f3-2bdf-4d2a-b430-636ea63a1691"
Connect-MicrosoftTeams -CertificateThumbprint $Thumbprint -ApplicationId $App -TenantId $Tenant
Connect-ExchangeOnline -CertificateThumbprint $Thumbprint -AppID $App -Organization $Org
Connect-MgGraph -CertificateThumbprint $Thumbprint -ClientId $App -TenantId $Tenant -nowelcome

$homedirs="\\deffm-fs1\user"
$CPath="\\deffm-deploy1.vwdgroup.net\Office365$"
$EAPath="$CPath\Expires"
$Date=get-date -Format yyyyMMdd
$LOG="\\deffm-deploy1.vwdgroup.net\ams-admin$\Scheduled_Tasks\log\Offboarding.log"
$LogDate =  get-date -Format "yyyy-MM-dd HH:mm"

#Build HTML REPORT
$ResultSet = @"
<html>
<font face="Arial">
<head>
<link rel="stylesheet" href=$css>
<title>Expire accounts report</title>
</head>
<style>
h1, h2, h5, th { text-align: center; }
table { margin: auto; font-family: Arial; box-shadow: 10px 10px 5px #888; border: thin ridge grey; }
th { background: #0046c3; color: #fff; max-width: 400px; padding: 5px 10px; }
td { font-size: 11px; padding: 5px 20px; color: #000; }
tr { background: #b8d1f3; }
tr:nth-child(even) { background: #dae5f4; }
tr:nth-child(odd) { background: #b8d1f3; }
</style>
<body>
<h2>Expire accounts report</h2>
<h5>Reported created on: $(Get-Date -Format "yyyy-MM-dd HH:mm:ss")<br>
</h5>
"@

    #Get all enabled AD users and include the account expiration property
    
    $DomainController="deffm-dc3.vwdgroup.net"
    $DomainController="SE8DC.osl.infront.as"
    $users = get-aduser -filter {Enabled -ne $True} -properties * -Server $DomainController
    $aadusers = Get-mguser -All -Property userprincipalname,AccountEnabled,id

    #Start searching for users with account expiration set
    $UsersWithExpirationSet = @()
    foreach ($user in $users) {
        $AE = $user.accountexpires
        #If Account Expiration is bigger than 16-11-4769, set it to 0
        if ($AE -gt 1000000000000000000) {
            $AE = 0
        }
        #Filter out accounts with Account Expiration set to 0
        if ($AE -gt 0) {
            $UsersWithExpirationSet += $user
        }
    }
    Write-Host "Accounts with expiration set"
    $UsersWithExpirationSet | ft Name, @{Name="FullName";Expression={$_.GivenName + " " + $_.Surname}}, @{Name="Expires";Expression={([System.DateTime]::FromFileTime($_.accountexpires)).ToString("dd-MM-yyyy")}}
    #$UsersToClean=$UsersWithExpirationSet 

    <#Set Expiration threshold to 0 days ago
    $expirationTH = (Get-Date)

    #Search for accounts expired before epiration threshold
    $UsersToClean = @()
    $Expired_no_cleaning = @()
    $not_expired=@()
    foreach ($user in $UsersWithExpirationSet) {
        $AE = $user.accountexpires
        $expdate = [System.DateTime]::FromFileTime($AE)
        #Check if account expired before Expiration threshold
        if($expdate -le $expirationTH)
        {
            $UsersToClean += $user
        }
        Else{
            $not_expired += $user
        }
    }
    Write-Host "Accounts expired"
    $UsersToClean | ft Name, @{Name="FullName";Expression={$_.GivenName + " " + $_.Surname}}, @{Name="Expired";Expression={([System.DateTime]::FromFileTime($_.accountexpires)).ToString("dd-MM-yyyy")}}

    Write-Host "Accounts not expired yet"
    $not_expired  | ft Name, @{Name="FullName";Expression={$_.GivenName + " " + $_.Surname}}, @{Name="Expired";Expression={([System.DateTime]::FromFileTime($_.accountexpires)).ToString("dd-MM-yyyy")}}
    


    If($UsersToClean)
    {
        $ResultSet += "To process<br>"
        $ResultSet += $($UsersToClean |select Name, @{Name="FullName";Expression={$_.GivenName + " " + $_.Surname}}, @{Name="Expired";Expression={([System.DateTime]::FromFileTime($_.accountexpires)).ToString("dd-MM-yyyy")}} | Sort-Object FullName | ConvertTo-Html -Fragment)
        $ResultSet += "<br>"
    }

    If($not_expired)
    {
        $ResultSet += "Not yet expired<br>"
        $ResultSet += $($not_expired |select Name, @{Name="FullName";Expression={$_.GivenName + " " + $_.Surname}},@{Name="Expired";Expression={([System.DateTime]::FromFileTime($_.accountexpires)).ToString("dd-MM-yyyy")}} | Sort-Object FullName |ConvertTo-Html -Fragment)
        $ResultSet += "<br>"
    }

    foreach($expire in $UsersToClean){
        $aaduser=$aadusers|?{$_.userprincipalname -eq $expire.UserPrincipalName}
        $aaduser.AccountEnabled
        Update-mguser -UserID $aaduser.id -AccountEnabled:$false
    }

    Add-Content $LOG "################################ Expired Accounts ############################### $LogDate"
    Add-Content $LOG "Accounts expired: $($UsersToClean.count)"
    foreach($U in $UsersToClean)
    {
        Add-Content $LOG  "$($U.DisplayName)"
    }
    Add-Content $LOG  "################################ Start ############################### $LogDate"

    $license_revoked=@()
    $no_license_revocation=@()
    $no_usermailbox=@()
    $no_vwdcom=@()
    #>

    If ($UsersToClean)
    {
        foreach ($UserToClean in $UsersToClean)
        {
            write-host -foregroundcolor Cyan $UserToClean.name
            <#O365
            if ($UserToClean.UserPrincipalName -match "@vwd.com" -or $DomainController -ne "deffm-dc3.vwdgroup.net")
            {
                if(get-recipient -identity $UserToClean.UserPrincipalName -ErrorAction silentlycontinue){
                    if ((get-recipient -identity $UserToClean.UserPrincipalName).RecipientType -eq "UserMailbox")
                    {
                        Try
                        {
                            Set-Mailbox $UserToClean.UserPrincipalName -LitigationHoldEnabled $true -ErrorAction Stop
                            Add-Content $LOG "Set litigationhold to enabled for: $($UsersToClean.DisplayName)"
                        }
                        Catch
                        {
                            $fail += "<br>Failed setting litigationhold to enabled for: $($usertoclean.DisplayName) <BR>"
                            Add-Content $LOG "Failed setting litigationhold to enabled for: $($UsersToClean.DisplayName)"
                        }
                        Try
                        {
                            Set-Mailbox $UserToClean.UserPrincipalName -Type Shared -ErrorAction Stop
                            Add-Content $LOG "Set account type to shared for: $($UsersToClean.DisplayName)"
                        }
                        Catch
                        {
                            $fail += "<br>Failed setting the account type to shared for: $($usertoclean.DisplayName) <BR>"
                            Add-Content $LOG "Failed setting the account type to shared for: $($UsersToClean.DisplayName)"
                        }
                        Try
                        {
                            #remove teams
                            # This will deprovision the user's account for Direct Routing
                            # and set their account to use the global voice routing policy
                            Grant-CsOnlineVoiceRoutingPolicy -Identity $UserToClean.UserPrincipalName -PolicyName $null -erroraction Stop
                            Add-Content $LOG "Removeds team routing policy for: $($UsersToClean.DisplayName)"
                        }
                        Catch
                        {
                            $fail += "<br>Failed removing teams routing policy for: $($usertoclean.DisplayName) <BR>"
                            Add-Content $LOG "Failed removing teams routing policy for: $($UsersToClean.DisplayName)"
                        }
                        Try
                        {
                            #remove teams
                            # This will deprovision the user's account for Direct Routing
                            # and set their account to use the global voice routing policy
                            Grant-CsTenantDialPlan -Identity $UserToClean.UserPrincipalName -PolicyName $null -erroraction Stop
                            Add-Content $LOG "Removed teams Dail PLan for: $($UsersToClean.DisplayName)"
                        }
                        Catch
                        {
                            $fail += "<br>Failed removing teams Dail PLan for: $($usertoclean.DisplayName) <BR>"
                            Add-Content $LOG "Failed removing teams Dail PLan for: $($UsersToClean.DisplayName)"
                        }
                        Try
                        {
                            #remove teams
                            # This will deprovision the user's account for Direct Routing
                            # and set their account to use the global voice routing policy
                            Remove-CsPhoneNumberAssignment -Identity $UserToClean.UserPrincipalName -RemoveAll -erroraction Stop
                            Add-Content $LOG "Removed teams number assignment for: $($UsersToClean.DisplayName)"
                        }
                        Catch
                        {
                            $fail += "<br>Failed removing number assignment for: $($usertoclean.DisplayName) <BR>"
                            Add-Content $LOG "Failed removing number assignment for: $($UsersToClean.DisplayName)"
                        }
                    
                        $count=0
                        while (!(((Get-Mailbox -Identity $UserToClean.UserPrincipalName).LitigationHoldEnabled -match $true) -and ((Get-Mailbox -Identity $UserToClean.UserPrincipalName).RecipientTypeDetails -match "SharedMailbox")))
                        {
                            Start-Sleep 10
                            $count++
                            If ($count -eq 20)
                            {
                                Add-Content $LOG "Change for litigationhold or account type not done in time for: $($UsersToClean.DisplayName)"
                                Break
                            }
                        }
                        If ($count -ne 20)
                        {
                            $mguser=get-mguser -userid $UserToClean.UserPrincipalName -property AssignedLicenses,ID,DisplayName,Mail,userprincipalname
                            if($mguser){
                                $Skus = $mguser | Select -ExpandProperty AssignedLicenses | Select SkuID
                                foreach($sku in $skus){
                                    Set-MgUserLicense -UserId $mguser.id -AddLicenses @() -RemoveLicenses $Sku.SkuId
                                }
                            }
                            
                            try 
                            {
                                Disable-ADAccount -Identity $UserToClean -ErrorAction Stop
                                Add-Content $LOG "Disabled account for: $($UsersToClean.DisplayName)"
                            }
                            catch 
                            {
                                $fail += "<br>Failed Disabling $($usertoclean.DisplayName) <BR>"
                                Add-Content $LOG "Failed Disabling account for: $($UsersToClean.DisplayName)"
                            }
                            Try
                            {
                                Set-ADUser -Identity $UserToClean -server $DomainController -Replace @{msExchHideFromAddressLists="TRUE"}
                                Add-Content $LOG "Set account to hide from addresslist for: $($UsersToClean.DisplayName)"
                            }
                            Catch
                            {
                                $fail += "<br>Failed hiding from addresslist for: $($usertoclean.DisplayName)<BR>"
                                Add-Content $LOG "Failed hiding from addresslist for: $($UsersToClean.DisplayName)"
                            }
                            Try
                            {
                                Set-ADUser -Identity $UserToClean -Server $DomainController -Clear Manager
                                Add-Content $LOG "Cleared manager info for: $($UsersToClean.DisplayName)"
                            }
                            Catch
                            {
                                $fail += "<br>Failed clearing the manager for: $($usertoclean.DisplayName)<BR>"
                                Add-Content $LOG "Failed clearing the manager for: $($UsersToClean.DisplayName)"
                            }
                            $license_revoked += $UserToClean
                        }
                        Else
                        {
                            $no_license_revocation += $UserToClean
                        }
                    }
                    Elseif((get-recipient -identity $UserToClean.UserPrincipalName).RecipientType -eq "MailUser")
                    {
                        if ((get-mguser -userid $UserToClean.UserPrincipalName) -eq $true)
                        {
                            $mguser=get-mguser -userid $UserToClean.UserPrincipalName -property AssignedLicenses,ID,DisplayName,Mail,userprincipalname
                            if($mguser){
                                $Skus = $mguser | Select -ExpandProperty AssignedLicenses | Select SkuID
                                foreach($sku in $skus){
                                    Set-MgUserLicense -UserId $mguser.id -AddLicenses @() -RemoveLicenses $Sku.SkuId
                                }
                            }
                        }
                        try 
                        {
                            Disable-ADAccount -Identity $UserToClean -ErrorAction Stop
                            Add-Content $LOG "Disabled account for: $($usertoclean.DisplayName)"
                        }
                        catch 
                        {
                            $fail += "<br>Failed Disabling $($usertoclean.DisplayName) <BR>"
                            Add-Content $LOG "Failed disabling account for: $($usertoclean.DisplayName)"
                        }
                        Try
                        {
                            Set-ADUser -Identity $UserToClean -server $DomainController -Replace @{msExchHideFromAddressLists="TRUE"}
                            Add-Content $LOG "Set account to hide from addresslist for: $($UsersToClean.DisplayName)"
                        }
                        Catch
                        {
                            $fail += "<br>Failed hiding from addresslist for: $($usertoclean.DisplayName)<BR>"
                            Add-Content $LOG "Failed hiding from addresslist for: $($UsersToClean.DisplayName)"
                        }
                        Try
                        {
                            Set-ADUser -Identity $UserToClean -Server $DomainController -Clear Manager
                            Add-Content $LOG "Cleared manager info for: $($UsersToClean.DisplayName)"
                        }
                        Catch
                        {
                            $fail += "<br>Failed clearing the manager for: $($usertoclean.DisplayName)<BR>"
                            Add-Content $LOG "Failed clearing the manager for: $($UsersToClean.DisplayName)"
                        }
                        Try
                        {
                            Set-ADUser -Identity $UserToClean -Server $DomainController -Clear telephonenumber
                            Add-Content $LOG "Cleared Phone info for: $($UsersToClean.DisplayName)"
                        }
                        Catch
                        {
                            $fail += "<br>Failed clearing the phone for: $($usertoclean.DisplayName)<BR>"
                            Add-Content $LOG "Failed clearing the phone for: $($UsersToClean.DisplayName)"
                        }
                    }
                    Else
                    {
                        IF (((Get-Mailbox -Identity $UserToClean.UserPrincipalName).LitigationHoldEnabled -match $true) -and ((Get-Mailbox -Identity $UserToClean.UserPrincipalName).RecipientTypeDetails -match "SharedMailbox"))
                        {
                            $mguser=get-mguser -userid $UserToClean.UserPrincipalName -property AssignedLicenses,ID,DisplayName,Mail,userprincipalname
                            if($mguser){
                                $Skus = $mguser | Select -ExpandProperty AssignedLicenses | Select SkuID
                                foreach($sku in $skus){
                                    Set-MgUserLicense -UserId $mguser.id -AddLicenses @() -RemoveLicenses $Sku.SkuId
                                }
                            }
                            try 
                            {
                                Disable-ADAccount -Identity $UserToClean -ErrorAction Stop
                                Add-Content $LOG "Disabled account for $($usertoclean.DisplayName)"
                            }
                            catch 
                            {
                                $fail += "<br>Failed Disabling $($usertoclean.DisplayName) <BR>"
                                Add-Content $LOG "Failed disabling account for $($usertoclean.DisplayName)"
                            }
                            Try
                            {
                                Set-ADUser -Identity $UserToClean -server $DomainController -Replace @{msExchHideFromAddressLists="TRUE"}
                                Add-Content $LOG "Set account to hide from addresslist for: $($UsersToClean.DisplayName)"
                            }
                            Catch
                            {
                                $fail += "<br>Failed hiding from addresslist for: $($usertoclean.DisplayName)<BR>"
                                Add-Content $LOG "Failed hiding from addresslist for: $($UsersToClean.DisplayName)"
                            }
                            Try
                            {
                                Set-ADUser -Identity $UserToClean -Server $DomainController -Clear Manager
                                Add-Content $LOG "Cleared manager info for: $($UsersToClean.DisplayName)"
                            }
                            Catch
                            {
                                $fail += "<br>Failed clearing the manager for: $($usertoclean.DisplayName)<BR>"
                                Add-Content $LOG "Failed clearing the manager for: $($UsersToClean.DisplayName)"
                            }
                        
                            Try
                            {
                                Set-ADUser -Identity $UserToClean -Server $DomainController -Clear telephoneNumber
                                Add-Content $LOG "Cleared Phone info for: $($UsersToClean.DisplayName)"
                            }
                            Catch
                            {
                                $fail += "<br>Failed clearing the phone for: $($usertoclean.DisplayName)<BR>"
                                Add-Content $LOG "Failed clearing the phone for: $($UsersToClean.DisplayName)"
                            }
                            $no_usermailbox += $UserToClean
                        }
                    }
                }
                Else{
                    try 
                    {
                        Disable-ADAccount -Identity $UserToClean -ErrorAction Stop
                        Add-Content $LOG "Disabled account for $($usertoclean.DisplayName)"
                    }
                    catch 
                    {
                        $fail += "<br>Failed Disabling $($usertoclean.DisplayName) <BR>"
                        Add-Content $LOG "Failed disabling account for $($usertoclean.DisplayName)"
                    }
                    Try
                    {
                        Set-ADUser -Identity $UserToClean -server $DomainController -Replace @{msExchHideFromAddressLists="TRUE"}
                        Add-Content $LOG "Set account to hide from addresslist for: $($UsersToClean.DisplayName)"
                    }
                    Catch
                    {
                        $fail += "<br>Failed hiding from addresslist for: $($usertoclean.DisplayName)<BR>"
                        Add-Content $LOG "Failed hiding from addresslist for: $($UsersToClean.DisplayName)"
                    }
                    Try
                    {
                        Set-ADUser -Identity $UserToClean -Server $DomainController -Clear Manager
                        Add-Content $LOG "Cleared manager info for: $($UsersToClean.DisplayName)"
                    }
                    Catch
                    {
                        $fail += "<br>Failed clearing the manager for: $($usertoclean.DisplayName)<BR>"
                        Add-Content $LOG "Failed clearing the manager for: $($UsersToClean.DisplayName)"
                    }
                        
                    Try
                    {
                        Set-ADUser -Identity $UserToClean -Server $DomainController -Clear telephoneNumber
                        Add-Content $LOG "Cleared Phone info for: $($UsersToClean.DisplayName)"
                    }
                    Catch
                    {
                        $fail += "<br>Failed clearing the phone for: $($usertoclean.DisplayName)<BR>"
                        Add-Content $LOG "Failed clearing the phone for: $($UsersToClean.DisplayName)"
                    }
                    $no_usermailbox += $UserToClean
                }
            }
            Else
            {
                #$UserToClean = $UserToClean.UserPrincipalName
                try 
                {
                    Disable-ADAccount -Identity $UserToClean -ErrorAction Stop
                    Add-Content $LOG "Disabled account for $($usertoclean.DisplayName)"
                }
                catch 
                {
                    $fail += "<br>Failed Disabling $($usertoclean.DisplayName) <BR>"
                    Add-Content $LOG "Failed disabling account for $($usertoclean.DisplayName)"
                }
                $no_vwdcom += $UserToClean
            }
        #>
        #Clean AD grouplist of user
        $file = $UserToClean.name+"_manual"
        $fileAAD = $UserToClean.name+"_manual_AAD"
        try 
        {
            (Get-ADUser -Identity $UserToClean -Properties MemberOf -server $DomainController).MemberOf -replace '^CN=([^,]+),OU=.+$','$1' | Out-File "\\deffm-deploy1\AMS-ADMIN$\Scheduled_Tasks\Disabled\$file.txt"
            Add-Content $LOG "Group information written to E:\AMS-ADMINS\Scheduled_Tasks\Disabled\$file.txt"
        }
        catch 
        {
            $fail += "<br> Failed writing group information to E:\AMS-ADMINS\Scheduled_Tasks\Disabled\$file.txt <BR>"
            Add-Content $LOG "Failed writing group information to E:\AMS-ADMINS\Scheduled_Tasks\Disabled\$file.txt"
        }
        try 
        {
            (Get-MgUserMemberOf -userid $UserToClean.userprincipalname|?{$_.additionalProperties['onPremisesSyncEnabled'] -ne $true } |select  @{N='Displayname';E={$_.additionalProperties['displayName']}}) | Out-File "\\deffm-deploy1\AMS-ADMIN$\Scheduled_Tasks\Disabled\$fileAAD.txt"
            Add-Content $LOG "Group information written to E:\AMS-ADMINS\Scheduled_Tasks\Disabled\$fileAAD.txt"
        }
        catch 
        {
            $fail += "<br> Failed writing group information to E:\AMS-ADMINS\Scheduled_Tasks\Disabled\$fileAAD.txt <BR>"
            Add-Content $LOG "Failed writing group information to E:\AMS-ADMINS\Scheduled_Tasks\Disabled\$fileAAD.txt"
        }

        (Get-MgUserMemberOf -userid $UserToClean.userprincipalname)|%{
        Remove-MgGroupOwnerByRef -GroupId $_.id -DirectoryObjectId ($aadusers|?{$_.userprincipalname -eq $UserToClean.userprincipalname}).id  


        (Get-MgUserMemberOf -userid $UserToClean.userprincipalname)|%{
            if(get-mggroup -GroupId $_.id|?{($_.OnPremisesSyncEnabled -ne $true) -and ($_.grouptypes -notcontains "DynamicMembership")}){
                #get-mggroup -GroupId $_ |select DisplayName,OnPremisesSyncEnabled,grouptypes,Id
                if (get-mggroup -GroupId $_.id |?{($_.MailEnabled -ne $True) -or ($_.grouptypes -contains "Unified")}){
                    try{
                        (get-mggroup -GroupId $_.id).DisplayName
                        Remove-MgGroupMemberByRef -GroupId $_.id -DirectoryObjectId ($aadusers|?{$_.userprincipalname -eq $UserToClean.userprincipalname}).id  
                        Add-Content $LOG "$($usertoclean.DisplayName) removed from $($_.additionalProperties['displayName'])"   
                    }
                    catch{
                        $fail += "<br> Failed removing $($usertoclean.DisplayName) from $($_.additionalProperties['displayName']) <BR>"
                        Add-Content $LOG "Failed removing $($usertoclean.DisplayName) from $($_.additionalProperties['displayName'])"
                    }            
                }
                Else{
                    write-host "Mail enabled group" $_.additionalProperties['displayName']
                    try{
                        #DL
                        remove-distributiongroupmember -Identity $_.id -Member $UserToClean.UserPrincipalName -BypassSecurityGroupManagerCheck -Confirm:$false -ErrorAction stop
                        Add-Content $LOG "DistributionGroup $($_.additionalProperties['displayName']) cleared for: $($usertoclean.DisplayName)"
                        Write-Host "DistributionGroup $($_.additionalProperties['displayName']) cleared for: $($usertoclean.DisplayName)"
                    }
                    catch{
                            $fail += "<br> Failed clearing DistributionGroup $($_.additionalProperties['displayName']) for: $($usertoclean.DisplayName) <BR>"
                            Add-Content $LOG "Failed clearing DistributionGroup $($_.additionalProperties['displayName']) for: $($usertoclean.DisplayName)"
                    }  
                }
            }
        }
        
        #Remove user from on-prem group 
        (Get-ADUser -Identity $UserToClean.samaccountname -Properties MemberOf -server $DomainController).MemberOf|%{
            try{
                write-host "Cleaning $($usertoclean.DisplayName) from onprem group $_"
                get-adobject -filter {Distinguishedname -eq $_} -server $domaincontroller|remove-adgroupmember -members $UserToClean.samaccountname -Confirm:$false
                Add-Content $LOG "Cleaning $($usertoclean.DisplayName) from onprem group: $_"
            }
            Catch{
                write-host "Failed cleaning $($usertoclean.DisplayName) from onprem group $_"
                Add-Content $LOG "Failed cleaning $($usertoclean.DisplayName) from onprem group: $_"
            }
        }

        <#Clear targetaddress
        try 
        {
            Set-aduser -Identity $UserToClean -Clear targetaddress -ErrorAction Stop
            Add-Content $LOG "Cleared targetaddress for: $($usertoclean.DisplayName)"
        }
        catch 
        {
            $fail += "<br> Failed clearing taregetaddress for: $($usertoclean.DisplayName) <BR>"
            Add-Content $LOG "Failed clearing taregetaddress for: $($usertoclean.DisplayName)"
        }
        

        #Move homedir to archive folder
        If($DomainController -eq "deffm-dc3.vwdgroup.net")
            {
            $samAccountName=$UserToClean.samaccountname
            $homedir=Test-Path "$homedirs\$samAccountName"
            If ($homedir -eq $true)
                {
                    #"$homedirs\$samAccountName"
                    try
                    {
                        Move-Item -Path "$homedirs\$samAccountName" -Destination "$homedirs\_Archived\$samAccountName" -ErrorAction SilentlyContinue
                        Add-Content $LOG "Moved homedir to archive for: $($usertoclean.DisplayName)"
                    }
                    catch
                    {
                        $fail += "<br> Failed moving homedir for $($usertoclean.DisplayName) <BR>"
                        Add-Content $LOG "Failed moving homedir to archive for: $($usertoclean.DisplayName)"
                    }
                }
            }#>
        }
    }
    Add-Content $LOG  "################################ End ############################### $LogDate"


    ################################ Set OoO Expiring acounts ###############################
    #########################################################################################
    $EAList=Get-ChildItem -Path $EAPath -Name *.csv
    #|?{$_ -notmatch "@infront"}
    Add-Content $LOG "################################ Auto Reply ############################### $LogDate"
    Add-Content $LOG "Accounts to set auto reply: $($EAList.count)"
    Add-Content $LOG  "################################ Start ############################### $LogDate"

    foreach($Entry in $EAList)
    {   
        $csv= Import-Csv -Path "$EAPath\$Entry" -Delimiter "|"
        $Expiry_Date = [datetime]::parseexact($csv.Expiry_csv, 'yyyyMMdd', $null)
        $endtime = [datetime]::parseexact($csv.endtime, 'yyyyMMdd', $null)
        $UserPrincipalName = $csv.UserPrincipalName
        $message=$csv.Automatic_reply
        $overwrite=$csv.overwrite
        $RemoveExpiry=$csv.RemoveExpiry
        Add-Content $LOG  "$($csv.DisplayName)"
        
        
        $enabledmailbox = get-mailbox -identity $UserPrincipalName -ErrorAction SilentlyContinue
        If($enabledmailbox)
        {
            $autoreply=Get-MailboxAutoReplyConfiguration -Identity $UserPrincipalName
            If($RemoveExpiry -eq $True)
            {
                try 
                {
                    Set-MailboxAutoReplyConfiguration -Identity $UserPrincipalName -AutoReplyState Disabled -ExternalAudience All
                    Add-Content $LOG "Autoreply config disabled for: $($csv.DisplayName)"
                }
                catch 
                {
                    $fail += "<br> Failed disabling Autoreply config for: $($csv.DisplayName) <BR>"
                    Add-Content $LOG "Failed disabling Autoreply config for: $($csv.DisplayName)"
                }
            }
            ElseIf($autoreply.AutoReplyState -eq "Disabled")
            {
                If((Get-Date -Date $Expiry_Date) -lt (Get-date)){
                    try 
                    {
                        Set-MailboxAutoReplyConfiguration -Identity $UserPrincipalName -AutoReplyState Enabled -ExternalAudience All -InternalMessage $message -ExternalMessage $message
                        Add-Content $LOG "Autoreply config aneabled for: $($csv.DisplayName)"
                    }
                    catch 
                    {
                        $fail += "<br> Failed setting Autoreply config for: $($csv.DisplayName) <BR>"
                        Add-Content $LOG "Failed setting Autoreply config for: $($csv.DisplayName)"
                    }
                }
                Else{
                    try 
                    {
                        Set-MailboxAutoReplyConfiguration -Identity $UserPrincipalName -AutoReplyState scheduled -ExternalAudience All -InternalMessage $message -ExternalMessage $message -StartTime $Expiry_Date -EndTime $endtime
                        Add-Content $LOG "Autoreply config set for: $($csv.DisplayName)"
                    }
                    catch 
                    {
                        $fail += "<br> Failed setting Autoreply config for: $($csv.DisplayName) <BR>"
                        Add-Content $LOG "Failed setting Autoreply config for: $($csv.DisplayName)"
                    }
                }
            }
            ElseIf($overwrite -eq "True")
            {
                If((Get-Date -Date $Expiry_Date) -lt (Get-date)){
                    try 
                    {
                        Set-MailboxAutoReplyConfiguration -Identity $UserPrincipalName -AutoReplyState Enabled -ExternalAudience All -InternalMessage $message -ExternalMessage $message
                        Add-Content $LOG "Autoreply config aneabled for: $($csv.DisplayName)"
                    }
                    catch 
                    {
                        $fail += "<br> Failed setting Autoreply config for: $($csv.DisplayName) <BR>"
                        Add-Content $LOG "Failed setting Autoreply config for: $($csv.DisplayName)"
                    }
                }
                Else{
                    try 
                    {
                        Set-MailboxAutoReplyConfiguration -Identity $UserPrincipalName -AutoReplyState scheduled -ExternalAudience All -InternalMessage $message -ExternalMessage $message -StartTime $Expiry_Date -EndTime $endtime
                        Add-Content $LOG "Autoreply config overwritten for: $($csv.DisplayName)"
                    }
                    catch 
                    {
                        $fail += "<br> Failed overwriting Autoreply config for $($csv.DisplayName) <BR>"
                        Add-Content $LOG "Failed overwriting Autoreply config for $($csv.DisplayName)"
                    }
                }
            }
        }
        $NewName=$Entry -replace "csv", "$Date"
        Rename-Item -Path "$EAPath\$Entry" -Newname $NewName
        Add-Content $LOG "CSV file renamed for: $($csv.DisplayName)"
        $CSV | select * -ExcludeProperty Automatic_reply |Export-csv -Path "$CPath\Rename_Accounts\to_rename.csv" -NoTypeInformation -Append -Force
    }
    Add-Content $LOG  "################################ End ############################### $LogDate"

    #Build HTML REPORT
    If ($license_revoked)
    {
        $ResultSet += $($license_revoked |select Name, @{Name="Users processed succesfully";Expression={$_.GivenName + " " + $_.Surname}}, @{Name="Expired";Expression={([System.DateTime]::FromFileTime($_.accountexpires)).ToString("dd-MM-yyyy")}}  | Sort-Object FullName | ConvertTo-Html -Fragment)
        $ResultSet += "<br>"
    }

    If ($no_license_revocation)
    {
        $ResultSet += $($no_license_revocation |select Name, @{Name="Users processed succesfully";Expression={$_.GivenName + " " + $_.Surname}}, @{Name="Expired";Expression={([System.DateTime]::FromFileTime($_.accountexpires)).ToString("dd-MM-yyyy")}} | Sort-Object FullName | ConvertTo-Html -Property Name,FullName,Expired -Fragment)
        $ResultSet += "<br>"
    }

    If ($no_usermailbox)
    {
        $ResultSet += $($no_usermailbox |select Name, @{Name="Users without UserMailbox";Expression={$_.GivenName + " " + $_.Surname}}, @{Name="Expired";Expression={([System.DateTime]::FromFileTime($_.accountexpires)).ToString("dd-MM-yyyy")}} | Sort-Object FullName | ConvertTo-Html -Property Name,FullName,Expired -Fragment)
        $ResultSet += "<br>"
    }

    If ($no_vwdcom)
    {
        $ResultSet += $($no_vwdcom |select Name, @{Name="Invallid mailaddress";Expression={$_.GivenName + " " + $_.Surname}}, @{Name="Expired";Expression={([System.DateTime]::FromFileTime($_.accountexpires)).ToString("dd-MM-yyyy")}} | Sort-Object FullName | ConvertTo-Html -Property Name,FullName,Expired -Fragment)
        $ResultSet += "<br>"
    }


    #####################################################################################
    ################################ Change account names ###############################
    #####################################################################################

    #Set Expiration threshold to 60 days ago
    $expirationTH2rename = (Get-Date).AddDays(-60)

    #Get all enabled AD users and include the account expiration property
    if($DomainController -eq "deffm-dc3.vwdgroup.net"){
        $Disabledusers = get-aduser -filter {(Enabled -eq $False) -and (UserPrincipalName -like "*@vwd.com*")} -properties * -server $DomainController |?{($_.canonicalname -notmatch "vwdgroup.net/Global/") -and ($_.canonicalname -notmatch "vwdgroup.net/Migration/")}
    }
    Else{
        $Disabledusers = get-aduser -filter {(Enabled -eq $False)} -properties Displayname,accountexpires,canonicalname -Server $DomainController
    }

    #Start searching for users with account expiration set
    $DisabledUsersWithExpirationSet = @()
    foreach ($user in $Disabledusers) {
        $AE = $user.accountexpires
        #If Account Expiration is bigger than 16-11-4769, set it to 0
        if ($AE -gt 1000000000000000000) {
            $AE = 0
        }
        #Filter out accounts with Account Expiration set to 0
        if ($AE -gt 0) {
            $DisabledUsersWithExpirationSet += $user
        }
    }
    write-host "Disabled users with ExpirationDate set: $($DisabledUsersWithExpirationSet.count)"

    #Search for accounts expired before epiration threshold
    $UsersTorename = @()
    foreach ($user in $DisabledUsersWithExpirationSet) {
        $AE = $user.accountexpires
        $expdate = [System.DateTime]::FromFileTime($AE)
        #Check if account expired before Expiration threshold
        if (($expdate -le $expirationTH2rename) -and ($user.DistinguishedName -match "OU=users") -and ($user.name -notmatch "_")) {
            $UsersTorename += $user
        }
    }
    write-host "Disabled users to rename: $($UsersTorename.count)"
    Add-Content $LOG "################################ Change account names ############################### $LogDate"
    Add-Content $LOG "Disabled users to rename: $($UsersTorename.count)"
    foreach($U in $UsersTorename)
    {
        Add-Content $LOG  "$($U.DisplayName)"
    }
    Add-Content $LOG  "################################ Start ############################### $LogDate"

    $renamedarr=@()
    foreach($line in $UsersTorename)
    {
        ###Set variables for new username###
        $line
        $radomizer= "_" + -join ((48..57) + (97..122) | Get-Random -Count 5 | % {[char]$_})
        $user=get-aduser -Identity $line.samAccountName -Properties proxyaddresses,DisplayName -server $DomainController
        $proxys=@()
        $proxyaddresses=@()
        $proxyaddresses2=$null
        $proxys=$user.proxyaddresses
        Foreach($proxy in $proxys)
        {
            $1=$proxy.split('@')[0]
            $2=$radomizer+"@"
            $3=$proxy.split('@')[1]
            $complete=$1+$2+$3
            $proxyaddresses += $complete
            $proxyaddresses2 += $complete +";" 
        }
    
        $upn = $user.userprincipalname.split('@')[0] + $radomizer+"@" + $user.userprincipalname.split('@')[1]
        $SamAcc= $user.SamAccountName + $radomizer
        $newusername=get-aduser -filter {SamAccountName -eq $SamAcc} -server $DomainController
        ### Check if new username doesn't exist###
        ### CHange useraccount###
        If(!($newusername))
        {
            $count=0
            While((get-aduser -Identity $user.samAccountName -Properties ProxyAddresses -Server $DomainController).ProxyAddresses)
            {
                try 
                {
                    Set-aduser -Identity $user.samAccountName -Clear ProxyAddresses -Server $DomainController -ErrorAction Stop
                    Add-Content $LOG  "ProxyAddresses cleared for: $($user.DisplayName)"
                    $count++
                    sleep 5
                }
                catch 
                {
                    $fail += "<br> Rename disabled user: Failed clearing the proxyaddresses for: $($user.DisplayName) <BR>"
                    Add-Content $LOG  "Rename disabled user: Failed clearing the proxyaddresses for: $($user.DisplayName)"
                }           
            }
            foreach($pr in $proxyaddresses)
            {
                try 
                {
                    Set-aduser -Identity $user.samAccountName -Add @{ProxyAddresses = "$pr"} -Server $DomainController -ErrorAction Stop
                    Add-Content $LOG  "ProxyAddress $pr added for: $($user.DisplayName)"
                }
                catch 
                {
                    $fail += "<br> Failed setting the proxyaddresses for: $($user.DisplayName) <BR>"
                    Add-Content $LOG  "Failed setting the proxyaddresses for: $($user.DisplayName)"
                }
            }
            
            try 
            {
                Set-aduser -Identity $user.samAccountName -Replace @{Mail = "$upn"} -Server $DomainController -ErrorAction Stop
                Add-Content $LOG  "Mail for $($user.DisplayName) changed to: $upn"
            }
            catch 
            {
                $fail += "<br> Failed changing Mail for: $($user.DisplayName) <BR>"
                Add-Content $LOG  "Failed changing Mail for: $($user.DisplayName)"
            }
            try 
            {
                Set-aduser -Identity $user.samAccountName -Replace @{MailNickName = "$SamAcc"} -Server $DomainController -ErrorAction Stop
                Add-Content $LOG  "MailNickName for $($user.DisplayName) changed to: $SamAcc"
            }
            catch 
            {
                $fail += "<br> Failed changing MailNickName for: $($user.DisplayName) <BR>"
                Add-Content $LOG  "Failed changing MailNickName for: $($user.DisplayName)"
            }
            
            try 
            {
                Set-aduser -Identity $user.samAccountName -clear telephoneNumber  -Server $DomainController
                Add-Content $LOG  "Cleared telephonenumber for: $($user.DisplayName)"
            }
            catch 
            {
                $fail += "<br> Failed Clearing telephonenumber for: $($user.DisplayName) <BR>"
                Add-Content $LOG  "Failed Clearing telephonenumber for: $($user.DisplayName)"
            }
            try 
            {
                Set-aduser -Identity $user.samAccountName -clear Mobile  -Server $DomainController
                Add-Content $LOG  "Cleared Mobile for: $($user.DisplayName)"
            }
            catch 
            {
                $fail += "<br> Failed Clearing Mobile for: $($user.DisplayName) <BR>"
                Add-Content $LOG  "Failed Clearing Mobile for: $($user.DisplayName)"
            }
            try 
            {
                Set-aduser -Identity $user.samAccountName -Replace @{userprincipalname = $upn} -Server $DomainController -ErrorAction Stop
                Add-Content $LOG  "userprincipalname for $($user.DisplayName) changed to: $upn"
            }
            catch 
            {
                $fail += "<br>  Failed changing userprincipalname for: $($user.DisplayName) <BR>"
                Add-Content $LOG  "Failed changing userprincipalname for: $($user.DisplayName)"
            }
            try 
            {
                Set-aduser -Identity $user.samAccountName -Replace @{SamAccountName = "$SamAcc"} -Server $DomainController -ErrorAction Stop
                Add-Content $LOG  "SamAccountName for $($user.DisplayName) changed to: $SamAcc"
            }
            catch 
            {
                $fail += "<br> Failed changing SamAccountName for: $($user.DisplayName) <BR>"
                Add-Content $LOG  "Failed changing SamAccountName for: $($user.DisplayName)"
            }
            try 
            {
                get-aduser -Filter {Name -eq $user.Name} -server $DomainController | Rename-ADObject -NewName $SamAcc -server $DomainController
                Add-Content $LOG  "Renamed object to: $SamAcc"
            }
            catch 
            {
                $fail += "<br> Failed renaming object for: $($user.DisplayName) <BR>"
                Add-Content $LOG  "Failed renaming object for: $($user.DisplayName)"
            }
            $count=0
            While(!(get-aduser -filter {SamAccountName -eq $SamAcc} -server $DomainController -ErrorAction SilentlyContinue))
            {
                $count++
                sleep 5
            }
            ###Fill renamed.csv###
            $renamedobj = New-Object PSObject
                $renamedobj | Add-Member -MemberType NoteProperty -Name "New_UPN" -Value $($upn)
                $renamedobj | Add-Member -MemberType NoteProperty -Name "Old_UPN" -Value $($line.UserPrincipalName)
                $renamedobj | Add-Member -MemberType NoteProperty -Name "New_SamAccountName" -Value $($SamAcc)
                $renamedobj | Add-Member -MemberType NoteProperty -Name "Old_SamAccountName" -Value $($line.samAccountName)
                $renamedobj | Add-Member -MemberType NoteProperty -Name "New_ProxyAddresses" -Value $($proxyaddresses2)
                $renamedobj | Add-Member -MemberType NoteProperty -Name "Date_renamed" -Value $($Date)
                    
            $renamedarr += $renamedobj
            Add-Content $LOG  "$renamedobj"
        }
    }
    Add-Content $LOG  "################################ End ############################### $LogDate"
    $renamedarr.count

    If ($renamedarr)
    {
        $ResultSet += "Renamed accounts:<br>"  
        $ResultSet += $($renamedarr | Sort-Object Old_UPN | ConvertTo-Html -Property Old_UPN,New_UPN,Old_SamAccountName,New_SamAccountName,New_ProxyAddresses,Date_renamed -Fragment)  -replace 'Old_UPN','Users renamed'
        $ResultSet += "<br>"
    }


    $ResultSet += "<br>"
    $ResultSet += "</body></html>"
}

if ($fail){
$Errorrep = @"
<html>
<font face="Arial">
<head>
<link rel="stylesheet" href=$css>
<title>Expire accounts Error reporting</title>
</head>
<style>
h1, h2, h5, th { text-align: center; }
table { margin: auto; font-family: Arial; box-shadow: 10px 10px 5px #888; border: thin ridge grey; }
th { background: #0046c3; color: #fff; max-width: 400px; padding: 5px 10px; }
td { font-size: 11px; padding: 5px 20px; color: #000; }
tr { background: #b8d1f3; }
tr:nth-child(even) { background: #dae5f4; }
tr:nth-child(odd) { background: #b8d1f3; }
</style>
<body>
<h2>Expire accounts Error reporting</h2>
<h5>Reported created on: $(Get-Date -Format "yyyy-MM-dd HH:mm:ss")<br>
</h5>
"@
    $Errorrep += $fail
    $Errorrep += "</body></html>"
    Send-MailMessage -To "Amsterdam - Beheer [SMB] <beheer-eu@vwd.com> " -from "Offboarding Error <logging-eu@vwd.com>" -subject "Offboarding Error" -BodyAsHtml -Body $Errorrep -SmtpServer "deffm-relay1.vwdgroup.net"
}
If ($no_license_revocation -or $UsersToClean -or $Expired_no_cleaning -or $license_revoked -or $no_license_revocation -or $no_usermailbox -or $no_vwdcom -or $renamedarr)
{
    Send-MailMessage -To "Amsterdam - Logging [SMB] <logging-eu@vwd.com>" -from "No-Reply <logging-eu@vwd.com>" -subject "Expired Accounts report" -BodyAsHtml -Body $ResultSet -SmtpServer "deffm-relay1.vwdgroup.net"
}