﻿#Needed to execute commands: Microsoft Online Services Sig-in and Windows Azure Active Directory Module
$user="ams-admin@vwd365.onmicrosoft.com"
$encrypted="01000000d08c9ddf0115d1118c7a00c04fc297eb0100000082b1a94d25ef6f4293cffb419dc60df50000000002000000000003660000c0000000100000006d14ca110f84a8cdf0e67a91ade8f2450000000004800000a00000001000000017f8276331de1c142ec17def86403f713000000086da6f1bed30f68c0203f862ac1daffd506a4537469b5a31a8a1207be35b8b0452851bfb66e9ad59340d8a272cb68fd214000000c7a1401e6ffd2592685878bd8c9a206cb8c0be68"
$password=ConvertTo-SecureString -string $encrypted
$msolcred = new-object -typename System.Management.Automation.PSCredential -argumentlist $user,$password
connect-msolservice -credential $msolcred

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session