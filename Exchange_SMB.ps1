$vwdsession=New-ExoPSSession -UserPrincipalName wagemakersd-adm@vwd.com
$infsession=New-ExoPSSession -UserPrincipalName dewagadm@osl.infront.as

Import-PSSession $vwdsession -AllowClobber
$SMBs=import-csv -Path C:\temp\smb3.csv
($smbs.access).split("|")
$SMBs|FT Displayname,access
$array=@()
foreach($smb in $SMBs)
{
    $smb.DisplayName
    $access=($smb.access).split("|")
    $groupaccess=($access|?{$_ -match "ZZ - "})
    If($groupaccess)
    {
        $groupaccess=$groupaccess.Trim()
        $groupaccess|%{
            $adgroup=get-adgroup -filter {(Displayname -eq $_) -or (cn -eq $_) -or (samaccountname -eq $_)}
            $adgroup.name
            $members=$null
            $members=Get-ADGroupMember -Identity $adgroup -Recursive |%{(get-aduser -Identity $_.samaccountname -properties Displayname).Displayname}
            $object=New-Object -TypeName psobject -Property @{
                Displayname     = $smb.DisplayName
                Enabled         = $smb.Enabled
                Mailaddresses   = ($smb.MailAddresses).Replace(", "," | ")
                access         = $smb.Access
                detailedaccess = $members -join " | "
            }
            $array+=$object
        }
    }
}
$array |export-csv -Path C:\temp\smb4.csv -NoTypeInformation
$array.count

$remaining=$array|?{$_.detailedaccess -eq ""}
$remaining.count
$array2=@()
foreach ($smb in $remaining){
    $smb.DisplayName
    $count=$count
    $access=($smb.access).split("|")
    $groupaccess=($access|?{$_ -match "ZZ - "})
    If($groupaccess)
    {
        $groupaccess=$groupaccess.Trim()
        $groupaccess|%{
            $input=$_+"*"
            $adgroup=get-adgroup -filter {(Displayname -like $input) -or (cn -like $input) -or (samaccountname -like $input)}
            $adgroup.SamAccountName
            $members=$null
            $members=Get-ADGroupMember -Identity $adgroup -Recursive |%{(get-aduser -Identity $_.samaccountname -properties Displayname).Displayname}
            $object=New-Object -TypeName psobject -Property @{
                Displayname     = $smb.DisplayName
                Enabled         = $smb.Enabled
                Mailaddresses   = ($smb.MailAddresses).Replace(", "," | ")
                access         = $smb.Access
                detailedaccess = $members -join " | "
            }
            $array2+=$object
        }
    }
}
$array2 |export-csv -Path C:\temp\smb5.csv -NoTypeInformation


$ad=get-aduser -filter {extensionAttribute2 -like "SMB*"} -properties *
$remaining=get-aduser -filter {extensionAttribute2 -like "remaining"} -properties *
$remaining.count
$ad|%{get-msoluser -UserPrincipalName $_.extensionAttribute1 -erroraction silentlycontinue}
$remaining|%{get-msoluser -UserPrincipalName $_.extensionAttribute1 -erroraction silentlycontinue}
