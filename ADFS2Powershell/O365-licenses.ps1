﻿$Users=Get-ADUser -Filter * -properties Enabled,description |?{<#$_.userprincipalname -notmatch "SMB" -and $_.userprincipalname -notmatch "-RES" -and $_.userprincipalname -notmatch "@vwdgroup.net" -and #>$_.userprincipalname -ne $NULL}
$usrlics=@()
foreach($usr in $Users)
{
    If (Get-MsolUser -UserPrincipalName $usr.userprincipalname -erroraction silentlycontinue)
    {
        $license=(Get-MsolUser -UserPrincipalName $usr.userprincipalname).Licenses.AccountSkuId
        If(Get-Mailbox -Identity $usr.userprincipalname  -ErrorAction silentlycontinue)
        {
            $type=(get-mailbox -Identity $usr.userprincipalname).RecipientTypeDetails
            $litigationhold=(Get-Mailbox -Identity $usr.UserPrincipalName).LitigationHoldEnabled
        }
        Else
        {
            $type="No Mailbox"
            $litigationhold=""
        }
    }
    Else
    {
        $license="No Msol User"
        $type=""
        $litigationhold=""
    }
    $usrlic = New-Object PSObject
        $usrlic | Add-Member -MemberType NoteProperty -Name "UPN" -Value $($usr.userprincipalname)
        $usrlic | Add-Member -MemberType NoteProperty -Name "AD User Enabled" -Value $($usr.Enabled)
        $usrlic | Add-Member -MemberType NoteProperty -Name "License" -Value $($license)
        $usrlic | Add-Member -MemberType NoteProperty -Name "Mailbox Type" -Value $($type)
        $usrlic | Add-Member -MemberType NoteProperty -Name "litigationhold" -Value $($litigationhold)
        $usrlic | Add-Member -MemberType NoteProperty -Name "Description" -Value $($usr.description)
    $Usrlics += $usrlic
}
$usrlics|Sort-Object License #| ?{$_.description -ne $Null}

($usrlics|?{$_.License -contains "vwd365:VISIOCLIENT"}).upn
$usrlics|Export-Csv -Path "C:\AMS-ADMINS\Powershell\Output\licenses.csv" -Delimiter "|" -NoTypeInformation