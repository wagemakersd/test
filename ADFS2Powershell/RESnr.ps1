$counter = @()
$list =  Get-ADUser -Filter * -SearchBase "OU=Ressources,OU=Migration,DC=vwdgroup,DC=net" | select-object samaccountname
foreach($entry in $list){ $counter += $entry.samaccountname.substring($entry.samAccountName.Length - 5) }
$last_nr = $counter | Sort-Object |Select-Object -last 1
write-host "`r`nLast used sequence number for RES: $last_nr`r`n"  