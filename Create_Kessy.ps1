$log=@()
try{
    $cred=New-Object System.Management.Automation.PSCredential ("eos\kessy", (New-Object System.Security.SecureString))
    new-PSDrive -PSProvider FileSystem -Root "\\194.55.43.190\kessy" -Name X -Persist -Credential $cred -erroraction stop
    $log+= "Kessy mapping X: created."
}
Catch{
	IF(($_.Exception.message -match "Index was outside the bounds of the array.") -or ($_.Exception.message -match "Der Index war au")){
		Write-Output "Kessy mapping X: created."
    	}
    else{
        Write-Error $_.Exception
    }
}

if (Get-SmbMapping -LocalPath "U:" -ErrorAction SilentlyContinue) {
    Remove-SmbMapping -LocalPath "U:" -Confirm:$false
}
try{
    $cred=New-Object System.Management.Automation.PSCredential ("notus\kessy", (New-Object System.Security.SecureString))
    new-PSDrive  -PSProvider FileSystem -Root "\\194.55.42.178\kessy" -name U -Persist -Credential $cred -erroraction stop
    $log+= "Kessy mapping U: created."
}
Catch{
	IF(($_.Exception.message -match "Index was outside the bounds of the array.") -or ($_.Exception.message -match "Der Index war au")){
		Write-Output "Kessy mapping U: created."
    }
    else{
        Write-Error $_.Exception
    }
}