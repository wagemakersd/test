# Create Distribution Group

$displayname = 'vwd - Securityboard [List]'
$vwdEmail = 'securityboard@vwd.com'
$emailprefix = 'securityboard'
$samAccountName = 'vwd-LST-00236'
$DomainController = 'deffm-dc2'
$targetOU = 'OU=DL,OU=Groups,OU=Migration,DC=vwdgroup,DC=net'
$location = 'DE'
new-adgroup -name $displayname -displayname $displayname -samaccountname $samAccountName -GroupCategory Distribution -GroupScope Universal -server $DomainController -Path $targetOU -ErrorAction Stop
Set-adgroup -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
Set-adgroup -Identity $samAccountName -Replace @{MailNickName = "$emailprefix"} -Server $DomainController -ErrorAction Stop
Set-adgroup -Identity $samAccountName -Replace @{Mail = "$vwdEmail"} -Server $DomainController -ErrorAction Stop
Set-adgroup -Identity $samAccountName -add @{MsExchUsageLocation = $location} -Server $DomainController -ErrorAction Stop
