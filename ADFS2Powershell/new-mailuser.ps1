# Edit ad-user for Mailuser
Import-Module activedirectory
$givenName=read-host "First Name"
$surName=read-host "Last Name"
$displayname = "$surName, $givenName"
$extEmail = read-host 'Ext. Email'
$targetOU = read-host "OU (f.e. OU=users,OU=ffm,OU=de,DC=vwdgroup,DC=net)"


$surName2 = $surName -replace "\s", ""
$emailprefix = $givenName.Substring(0,1)+"$surName2"
$emailprefix = $emailprefix.ToLower()
$UserPrincipalName = "$emailprefix"+"@vwd.com"


$samAccountName = $surName2+$givenName.Substring(0,1)
$activeuser= Get-aduser -filter {sAMAccountName -eq $samAccountName} -Server $DomainController
If($activeuser -ne $null)
{
	write-warning "The username $samAccountName already exists."
	$samAccountName = Read-host "Please supply new username"
}

$activeprefix= Get-aduser -filter {UserPrincipalName -eq $UserPrincipalName} -Server $DomainController
If ($activeprefix)
        {
            write-warning "The prefix $emailprefix already exists."
		    $emailprefix = Read-host "Please supply new emailprefix"
	    }


$location = 'DE'
$DomainController = "deffm-dc1"

$vwdEmail = "$emailprefix"+"@vwd.com"
$samAccountName = $samAccountName.ToLower()


"`n"
"Givenname:`t`t$givenName"
"Surname:`t`t$surName"
"Displayname:`t`t$displayname"
"sAMAccountName:`t`t$samAccountName"
"EmailPrefix:`t`t$emailprefix"
"Email:`t`t`t$vwdEmail"
"extEmail:`t`t$extEmail"
"Target OU:`t`t$targetOU"
"Location:`t`t$location"
"`n"
$correct=Read-host "Is the above correct (Y/N)"
If ($correct -eq "Y")
{
    new-aduser -name $samAccountName -givenName $givenName -surName $surName -displayname $displayname -samaccountname $samAccountName -UserPrincipalName $vwdEmail -server $DomainController -Path $targetOU -ErrorAction Stop
    Set-ADUser -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-ADUser -Identity $samAccountName -Clear targetAddress -Server $DomainController -ErrorAction Stop
    Set-ADUser -Identity $samAccountName -Add @{targetAddress = "$extEmail"} -Server $DomainController -ErrorAction Stop 
    Set-ADUser -Identity $samAccountName -UserPrincipalName $vwdEmail -Server $DomainController -ErrorAction Stop
    Set-ADUser -Identity $samAccountName -DisplayName $DisplayName -Server $DomainController -ErrorAction Stop
    Set-ADUser $samAccountName -Replace @{MailNickName = "$samAccountName"} -Server $DomainController -ErrorAction Stop 
    Set-ADUser $samAccountName -add @{MsExchUsageLocation = $location} -Server $DomainController -ErrorAction Stop
}
Else
{
    Write-Warning "Mailuser has not been created"
    PAUSE
}