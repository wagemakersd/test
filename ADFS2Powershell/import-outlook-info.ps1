﻿$DomainController="deffm-dc1"
$Users=Import-Excel -Path "C:\AMS-ADMINS\Powershell\input\user_export_outlook_Sales_completed.xlsx"

Foreach($user in $users)
{
    If ($user.Useraccount)
    {
        If($user.Company)
        {
        Set-aduser -Identity $user.Useraccount -Replace @{Company = $user.Company} -Server $DomainController -ErrorAction Stop
        }
        If($user.physicalDeliveryOfficeName)
        {
        Set-aduser -Identity $user.Useraccount -Replace @{physicalDeliveryOfficeName = $user.physicalDeliveryOfficeName} -Server $DomainController -ErrorAction Stop
        }
        If($user.Department)
        {
        Set-aduser -Identity $user.Useraccount -Replace @{Department = $user.Department} -Server $DomainController -ErrorAction Stop
        }
        If($user.Job_Title)
        {
        Set-aduser -Identity $user.Useraccount -Replace @{Title = $user.Job_Title} -Server $DomainController -ErrorAction Stop
        }
        If($user.OfficePhone)
        {
        Set-aduser -Identity $user.Useraccount -Replace @{telephoneNumber = $user.OfficePhone} -Server $DomainController -ErrorAction Stop
        }
        If($user.MobilePhone)
        {
        Set-aduser -Identity $user.Useraccount -Replace @{Mobile = $user.MobilePhone} -Server $DomainController -ErrorAction Stop
        }
        If($user.StreetAddress)
        {
        Set-aduser -Identity $user.Useraccount -Replace @{StreetAddress = $user.StreetAddress} -Server $DomainController -ErrorAction Stop
        }
        If($user.Country)
        {
        Set-aduser -Identity $user.Useraccount -Replace @{Co = $user.Country} -Server $DomainController -ErrorAction Stop
        }
        If($user.City)
        {
        Set-aduser -Identity $user.Useraccount -Replace @{l = $user.City} -Server $DomainController -ErrorAction Stop
        }
        If($user.PostalCode)
        {
        $postal=$user.PostalCode
        Set-aduser -Identity $user.Useraccount -Replace @{postalCode = "$postal"} -Server $DomainController -ErrorAction Stop
        }

        If ($user.Manager -match "DC=vwdgroup,DC=net")
        {
            Set-aduser -Identity $user.Useraccount -Replace @{Manager = $user.Manager} -Server $DomainController -ErrorAction Stop
        }
        ElseIf($user.Manager -match "@vwd.com")
        {
            $manager=$user.manager
            $manager=Get-ADUser -Filter {UserPrincipalName -eq $manager}
            If($manager)
            {
                Set-aduser -Identity $user.Useraccount -Replace @{Manager = $manager.DistinguishedName} -Server $DomainController -ErrorAction Stop
            }
            Else
            {
                Write-host "Manager (UPN) not found"
            }
        }
    }
    Else
    {
        $user
    }
}