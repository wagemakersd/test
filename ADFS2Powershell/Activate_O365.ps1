﻿#ENTERPRISE E3 License (only needed serviceplans)
Set-MsolUserLicense -UserPrincipalName dwagemakers@vwd.com -AddLicenses vwd365:ENTERPRISEPACK -ErrorAction SilentlyContinue
$x = New-MsolLicenseOptions -AccountSkuId vwd365:ENTERPRISEPACK -DisabledPlans FORMS_PLAN_E3,STREAM_O365_E3,Deskless,FLOW_O365_P2,POWERAPPS_O365_P2,TEAMS1,PROJECTWORKMANAGEMENT,SWAY,INTUNE_O365,YAMMER_ENTERPRISE,RMS_S_ENTERPRISE,SHAREPOINTWAC,SHAREPOINTENTERPRISE
Set-MsolUserLicense -UserPrincipalName dwagemakers@vwd.com -AddLicenses $x

#EXCHANGE AND SKYPE ONLY License
Set-MsolUserLicense -UserPrincipalName dwagemakers@vwd.com -AddLicenses vwd365:EXCHANGEENTERPRISE  -ErrorAction SilentlyContinue
Set-MsolUserLicense -UserPrincipalName dwagemakers@vwd.com -AddLicenses vwd365:MCOSTANDARD  -ErrorAction SilentlyContinue

