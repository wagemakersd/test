##This script checks for devices registered to AzureAD and removes them so you can successfully perform an AzureAD join. 
# We recommend you backup your registry prior to running. We take no responisbility for the use of this script.
Add-Type -AssemblyName PresentationCore,PresentationFramework,WindowsBase
$sids = Get-ChildItem 'HKLM:\SOFTWARE\Microsoft\EnterpriseResourceManager\Tracked' -name |where-object {$_.Length -gt 25}


Foreach ($sid in $sids){

$removedevice=$true

if ($removedevice -eq $true) {

$enrollmentpath = "HKLM:\SOFTWARE\Microsoft\Enrollments\$($sid)"
$entresourcepath = "HKLM:\SOFTWARE\Microsoft\EnterpriseResourceManager\Tracked\$($sid)"


##Remove device from enrollments in registry
[System.Windows.MessageBox]::Show("Cleaning enrollment for $($sid)") | Out-Null
$value1 = Test-Path $enrollmentpath
If ($value1 -eq $true) {
   
Remove-Item -Path $enrollmentpath -Recurse -confirm:$false -Force
Remove-Item -Path $entresourcepath -Recurse -confirm:$false -Force


} 
Else {[System.Windows.MessageBox]::Show("The value does not exist, skipping") | Out-Null}



##Cleanup scheduled tasks related to device enrollment and the folder for this SID


#Get-ScheduledTask -TaskPath "\Microsoft\Windows\EnterpriseMgmt\$($sid)\*"| Unregister-ScheduledTask -Confirm:$false


#$scheduleObject = New-Object -ComObject Schedule.Service
#$scheduleObject.connect()
#$rootFolder = $scheduleObject.GetFolder("\Microsoft\Windows\EnterpriseMgmt")
#$rootFolder.DeleteFolder($sid,$null)

#Write-Host "Device registration cleaned up for $($sid). If there is more than 1 device registration, we will continue to the next one."
[System.Windows.MessageBox]::Show("Device registration cleaned up for $($sid). If there is more than 1 device registration, we will continue to the next one.") | Out-Null

} else { Write-host "Removal has been cancelled for $($sid)"}


}

[System.Windows.MessageBox]::Show("Cleanup of device registration has been completed. Starting Migration") | Out-Null

#write-host "Cleanup of device registration has been completed. Ensure you delete the device registration in AzureAD and you can now join your device."

start-process "$env:windir\system32\dsregcmd.exe" -ArgumentList "/leave"
start-process -FilePath "$env:windir\system32\sysprep\sysprep.exe" -ArgumentList "/oobe /quiet /reboot"