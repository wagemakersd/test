# create User
$givenName = ''
$surName = ''
$displayname = 'AMS-Support dev'
$vwdEmail = 'Support.development-eu@vwd.com'
$emailprefix = 'AMS-SMB-00170'
$samAccountName = 'AMS-SMB-00170'
$DomainController = 'deffm-dc1'
$targetOU = 'OU=Shared Mailboxes,OU=Migration,DC=vwdgroup,DC=net'
$location = 'NL'
new-aduser -name $samAccountName -givenName $givenName -surName $surName -displayname $displayname -samaccountname $samAccountName -UserPrincipalName $vwdEmail -server $DomainController -Path $targetOU -ErrorAction Stop
Set-aduser -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
Set-aduser -Identity $samAccountName -Replace @{MailNickName = "$emailprefix"} -Server $DomainController -ErrorAction Stop
Set-aduser -Identity $samAccountName -Replace @{Mail = "$vwdEmail"} -Server $DomainController -ErrorAction Stop
Set-aduser -Identity $samAccountName -add @{MsExchUsageLocation = $location} -Server $DomainController -ErrorAction Stop
