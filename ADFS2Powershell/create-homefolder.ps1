﻿$searchbase="OU=de,dc=vwdgroup,dc=net"
$users=Get-ADUser -Properties Enabled -Filter 'enabled -eq $True' -SearchBase $searchbase |select samaccountname |?{$_.samaccountname -notmatch "-adm" -and $_.samaccountname -notmatch "betriebsrat" -and $_.samaccountname -notmatch "deffm-" -and $_.samaccountname -notmatch "svc_" -and $_.samaccountname -notmatch "betriebsrat"}

Foreach($user in $users)
{
    
    $foldername=$user.SamAccountName
    $path="\\deffm-fs1\user\$foldername"

    If (!(Test-Path -Path $path))
    {

        New-Item -ItemType directory -Path $path -Force
        
        # get permissions
        $acl = Get-Acl -Path $path
        $acl.SetAccessRuleProtection($True, $Flase)
        $acl |Set-Acl

        # add a new permission (FullControl, Modify, Read)
        $permission = "$FolderName", 'FullControl', 'ContainerInherit, ObjectInherit', 'None', 'Allow'
        $rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permission
        $acl.SetAccessRule($rule)
      

        # add a new permission (FullControl, Modify, Read)
        $permission = "deffm-fs-admins", 'FullControl', 'ContainerInherit, ObjectInherit', 'None', 'Allow'
        $rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permission
        $acl.SetAccessRule($rule)
      

         # add a new permission (FullControl, Modify, Read)
        $permission = "deffm-backup", 'FullControl', 'ContainerInherit, ObjectInherit', 'None', 'Allow'
        $rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permission
        $acl.SetAccessRule($rule)
        # set new permissions
        $acl | Set-Acl -Path $path 

    }
    else 
    {
        #$user
    }
} 