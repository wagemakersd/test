﻿Import-Module ActiveDirectory

$userou = 'ou=de,dc=vwdgroup,dc=net'
$users = Get-ADUser -Filter * -SearchBase $userou -Properties SamAccountName, ProxyAddresses, givenName, Surname


Foreach ($user in $users) 
    {
    If ($user.UserPrincipalName -match "@vwd.com")
        {
        $newproxy = "sip:"+$user.UserPrincipalName
        Set-ADUser -Identity $user.SamAccountName -Add @{Proxyaddresses="$newproxy"} -whatif
        }
    } 