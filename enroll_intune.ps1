$path1="HKLM:SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\MDM\"
IF(!(get-item $path1 -ErrorAction SilentlyContinue)){
    new-item $path1
}
If((Get-Item $path1 -ErrorAction SilentlyContinue).property -contains "AutoEnrollMDM"){
    Set-ItemProperty -Path $path1 -Name AutoEnrollMDM -Value 1
}
else{
    New-ItemProperty -Path $path1 -Name AutoEnrollMDM -Value 1 -PropertyType DWord
}  

Start-Process "Deviceenroller.exe" -ArgumentList "/c /autoenrollmdm"