﻿$UsersToClean = get-aduser -filter {Enabled -eq $False} -properties accountexpires |?{$_.samaccountname -match "Grossens" -or $_.samaccountname -match "Boernera" -or $_.samaccountname -match "Juliusj" -or $_.samaccountname -match "Regnera" -or $_.samaccountname -match "Gattolliate" -or $_.samaccountname -match "Minderm" -or $_.samaccountname -match "Minderm" -or $_.samaccountname -match "Steputatm" -or $_.samaccountname -match "Steputatm" -or $_.samaccountname -match "Chandam" -or $_.samaccountname -match "Roehrnbacs" -or $_.samaccountname -match "Vanderhelh" -or $_.samaccountname -match "Verlagem" -or $_.samaccountname -match "Kellera" -or $_.samaccountname -match "Reiffensta"}

foreach ($UserToClean in $UsersToClean)
    {
        if ($UserToClean.UserPrincipalName -match "@vwd.com")
        {
            if ((get-recipient -identity $UserToClean.UserPrincipalName).RecipientType -eq "UserMailbox")
            {
                Set-Mailbox $UserToClean.UserPrincipalName -LitigationHoldEnabled $true
                Set-Mailbox $UserToClean.UserPrincipalName -Type Shared
                $litigationhold=(Get-Mailbox -Identity $UserToClean.UserPrincipalName).LitigationHoldEnabled
                $shared=(Get-Mailbox -Identity $UserToClean.UserPrincipalName).RecipientTypeDetails
                If ($litigationhold -match $true -and $shared -match "SharedMailbox")
                {
                    $License = (Get-MsolUser -UserPrincipalName $UserToClean.UserPrincipalName).Licenses.AccountSkuId
                    foreach($lic in $license)
                        {
                        Set-MsolUserLicense -UserPrincipalName $UserToClean.UserPrincipalName -RemoveLicenses $Lic
                        }
                    Disable-ADAccount -Identity $UserToClean
                    Set-ADUser -Identity $UserToClean -server deffm-dc1 -Replace @{msExchHideFromAddressLists="TRUE"}
                }
            }

        }
        Else
        {
            $UserToClean = $UserToClean.UserPrincipalName
            Write-Host "$UserToClean is not a valid emailaddress."
        }
    $file = $UserToClean.name
    (Get-ADUser -Identity $UserToClean -Properties MemberOf).MemberOf -replace '^CN=([^,]+),OU=.+$','$1' | Out-File C:\AMS-ADMINS\Scheduled_Tasks\Disabled\$file.txt
    (Get-ADUser -Identity $UserToClean -Properties MemberOf).MemberOf | Remove-ADGroupMember -Member $UserToClean -Confirm:$false
    }