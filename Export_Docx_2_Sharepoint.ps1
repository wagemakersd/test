
# Function to ensure OpenXML assembly is loaded
function Initialize-OpenXML {
    [CmdletBinding()]
    param()
    
    process {
        try {
            Write-Host "Installing and loading OpenXML..." -ForegroundColor Cyan
            
            # Install NuGet if not available
            if (-not (Get-PackageProvider -ListAvailable -Name NuGet -ErrorAction SilentlyContinue)) {
                Install-PackageProvider -Name NuGet -Force -Scope CurrentUser -MinimumVersion 2.8.5.201
            }
            
            # Install OpenXML if not available
            if (-not (Get-Package DocumentFormat.OpenXml -ErrorAction SilentlyContinue)) {
                Install-Package DocumentFormat.OpenXml -Force -Scope CurrentUser
            }
            
            # Get the latest installed version
            $openXmlPackage = Get-Package DocumentFormat.OpenXml | Sort-Object Version -Descending | Select-Object -First 1
            
            # Construct the assembly path
            $packagePath = Split-Path $openXmlPackage.Source -Parent
            $possiblePaths = @(
                "lib\net46\DocumentFormat.OpenXml.dll",
                "lib\net40\DocumentFormat.OpenXml.dll",
                "lib\net35\DocumentFormat.OpenXml.dll"
            )
            
            $dllPath = $null
            foreach ($path in $possiblePaths) {
                $testPath = Join-Path $packagePath $path
                if (Test-Path $testPath) {
                    $dllPath = $testPath
                    break
                }
            }
            
            if (-not $dllPath) {
                throw "Could not find OpenXML DLL in package directory"
            }
            
            # Load the assembly
            Write-Host "Loading assembly from: $dllPath" -ForegroundColor Yellow
            [System.Reflection.Assembly]::LoadFrom($dllPath)
            
            # Verify the type is available
            $type = [System.Type]::GetType("DocumentFormat.OpenXml.Packaging.WordprocessingDocument")
            if (-not $type) {
                throw "Failed to load WordprocessingDocument type"
            }
            
            Write-Host "OpenXML initialized successfully." -ForegroundColor Green
            return $true
        }
        catch {
            Write-Error "Failed to initialize OpenXML: $_"
            throw
        }
    }
}

function Update-WordDocumentContent {
    param (
        [string]$FilePath,
        [hashtable]$Variables
    )
    
    $wordDoc = $null
    try {
        # Load the assembly namespace
        $wordDoc = [DocumentFormat.OpenXml.Packaging.WordprocessingDocument]::Open($FilePath, $true)
        $mainPart = $wordDoc.MainDocumentPart
        $doc = $mainPart.Document
        $body = $doc.Body
        
        # Process all text elements
        $texts = $body.Descendants[[DocumentFormat.OpenXml.Wordprocessing.Text]]
        
        foreach ($text in $texts) {
            $content = $text.Text
            foreach ($key in $Variables.Keys) {
                $placeholder = "{{" + $key + "}}"
                if ($content.Contains($placeholder)) {
                    $text.Text = $content.Replace($placeholder, $Variables[$key])
                }
            }
        }
        
        $doc.Save()
        return $true
    }
    catch {
        Write-Error "Error updating document content: $_"
        return $false
    }
    finally {
        if ($wordDoc) {
            $wordDoc.Dispose()
        }
    }
}

function Update-WordTemplateAndEmail {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [string]$TemplatePath,
        
        [Parameter(Mandatory = $true)]
        [hashtable]$Variables,
        
        [Parameter(Mandatory = $true)]
        [string]$SmtpServer,
        
        [Parameter(Mandatory = $true)]
        [string]$FromEmail,
        
        [Parameter(Mandatory = $true)]
        [string]$ToEmail,
        
        [Parameter(Mandatory = $true)]
        [string]$Subject,
        
        [Parameter(Mandatory = $false)]
        [string]$Body = "Please find the attached document.",
        
        [Parameter(Mandatory = $false)]
        [System.Management.Automation.PSCredential]$Credentials,
        
        [Parameter(Mandatory = $false)]
        [switch]$UseSsl = $true
    )
    
    try {
        # Initialize OpenXML
        Initialize-OpenXML
        
        # Validate template path
        if (-not (Test-Path $TemplatePath)) {
            throw "Template file not found: $TemplatePath"
        }
        
        # Create temp file
        $tempFile = [System.IO.Path]::GetTempFileName() + ".docx"
        Copy-Item -Path $TemplatePath -Destination $tempFile -Force
        
        Write-Host "Processing document..." -ForegroundColor Yellow
        
        # Update document content
        if (-not (Update-WordDocumentContent -FilePath $tempFile -Variables $Variables)) {
            throw "Failed to update document content"
        }
        
        Write-Host "Sending email..." -ForegroundColor Yellow
        
        # Prepare email parameters
        $emailParams = @{
            From = $FromEmail
            To = $ToEmail
            Subject = $Subject
            Body = $Body
            SmtpServer = $SmtpServer
            Attachments = $tempFile
            UseSsl = $UseSsl
            Port = 587
        }
        
        if ($Credentials) {
            $emailParams.Add("Credential", $Credentials)
        }
        
        # Send email
        Send-MailMessage @emailParams
        
        Write-Host "Document generated and email sent successfully!" -ForegroundColor Green
        
        return $true
    }
    catch {
        Write-Host "Error: $_" -ForegroundColor Red
        return $false
    }
    finally {
        # Clean up temp file
        if (Test-Path $tempFile) {
            Remove-Item $tempFile -Force
        }
    }
}


# Example usage with better error handling
#try {
    $templateVariables = @{
        "Displayname" = "John Doe"
        "SamAccountName" = "johdoe"
        "Mail" = "John@doe.com"
        "Password" = "GoD123!@#"
        "Jira" = "ITIO-1234"
    }

    $emailParams = @{
        TemplatePath = "C:\Users\Wagemakersd\OneDrive - Infront ASA\Documents\Projecten\Onboarding\New user, Account credentials, English.docx"
        Variables = $templateVariables
        SmtpServer = "deffm-relay1.vwdgroup.net"
        FromEmail = "dennis.wagemakers@infront.co"
        ToEmail = "beheer-eu@vwd.com"
        Subject = "Onboarding - $($templateVariables['Displayname'])"
        Body = "Please find attached the onboarding details"
        Verbose = $true
    }

    #$result = 
    Update-WordTemplateAndEmail @emailParams

    <#
    if (-not $result) {
        throw "Template processing failed"
    }
}
catch {
    Write-Error "Script execution failed: $_"
    exit 1
}
    #>