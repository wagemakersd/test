Import-Module Microsoft.Graph.DeviceManagement.Actions

Select-MgProfile -Name beta

Connect-Mggraph -scopes DeviceManagementManagedDevices.ReadWrite.All, DeviceManagementManagedDevices.PrivilegedOperations.All



$alldevices = get-MgDeviceManagementManagedDevice | Where-Object {$_.OperatingSystem -eq "Windows"}
$device = get-MgDeviceManagementManagedDevice|?{$_.SerialNumber -match "F080"}
Foreach ($device in $alldevices) {
    Sync-MgDeviceManagementManagedDevice -ManagedDeviceId $device.id
    write-host "Sending device sync request to" $device.DeviceName -ForegroundColor yellow
}