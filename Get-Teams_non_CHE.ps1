get-content $file.FullName -raw -encoding UTF8
$team=get-team
$exclude=@()
foreach($entry in $team){
    $group=Get-UnifiedGroup -Identity $entry.mailnickname
    if($group.SharePointSiteUrl -notmatch "https://infrontonlineche.sharepoint.com"){
        write-host -foregroundcolor Orange "Exclude $($Entry.Displayname), $($Entry.Description)"
        $exclude+=new-object -typename PSObject -Property @{
            Displayname         = $Entry.Displayname
            Description         = $Entry.Description
            SharePointSiteUrl   = $group.SharePointSiteUrl
        }
    }
}

$exclude_org=import-csv -Path c:\temp\exclude.csv
$compared=compare-object -referenceobject $exclude_org.Displayname -DifferenceObject $exclude.Displayname

$new=$exclude|?{$_.displayname -in ($compared|?{$_.SideIndicator -eq "=>"}).inputobject}
$to_check=($compared|?{$_.SideIndicator -eq "<="}).inputobject

$exclude|export-csv -path c:\temp\exclude.csv -Force -Encoding UTF8

$date=get-date -format yyyyMM
$new | Export-Excel -path "\Exclude_TeamsBackup_$date.xlsx"