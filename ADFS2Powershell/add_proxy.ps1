﻿Import-Module ActiveDirectory

$userou = 'ou=de,dc=vwdgroup,dc=net'
$users = Get-ADUser -Filter * -SearchBase $userou -Properties SamAccountName, ProxyAddresses, givenName, Surname


Foreach ($user in $users) 
    {
    If ($user.UserPrincipalName -match "@vwd.com")
        {
        #write-host "`n"
            $proxylist=$user.proxyAddresses 
            foreach ($proxy in $proxylist)
            {
                If($proxy|?{$_ -match "vwd.com" -and $_ -notmatch "sip:"})
                    {
                        
                        $newproxy= $proxy  -replace "@vwd.com", "@vwd.de"
                        $newproxy=$newproxy.ToLower()
                        #$proxy
                        #newproxy
                        Set-ADUser -Identity $user.SamAccountName -Add @{Proxyaddresses="$newproxy"} #-whatif
                    }
            }
        }
    } 

