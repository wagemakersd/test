$domaincontroller="NO91DC01.osl.infront.as"
$vwdgroupdomaincontroller="deffm-dc3.vwdgroup.net"
$users=get-aduser -filter {(Userprincipalname -like "*@vwd.com") -and (Enabled -eq $True) -and (Displayname -notlike "*(Admin)")} -Properties * -Server $vwdgroupdomaincontroller
$users=get-aduser -filter * -Properties * -Server $domaincontroller
$notcorrect=@()
$correct=@()
#$domains=@()

foreach($user in $users){
    $mail=$null
    foreach($proxy in $user.proxyAddresses){
        IF($proxy -cmatch "SMTP"){
           switch -Wildcard ($proxy)
            {
                "*@infrontfinance.com" {
                    $mail=$proxy -replace "@infrontfinance.com","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@infront.biz" {
                    $mail=$proxy -replace "@infront.biz","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@lindeman.no" {
                    $mail=$proxy -replace "@lindeman.no","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@infront.no" {
                    $mail=$proxy -replace "@infront.no","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@sysdeco.no" {
                    $mail=$proxy -replace "@sysdeco.no","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@infrontanalytics.com" {
                    $mail=$proxy -replace "@infrontanalytics.com","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@tdndirekt.com" {
                    $mail=$proxy -replace "@tdndirekt.com","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@tdn.no" {
                    $mail=$proxy -replace "@tdn.no","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@direkt.se" {
                    $mail=$proxy -replace "@direkt.se","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@tdndirekt.com" {
                    $mail=$proxy -replace "@tdndirekt.com","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@insightdirekt.se" {
                    $mail=$proxy -replace "@tdndirekt.com","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@smabolagsnytt.se" {
                    $mail=$proxy -replace "@smabolagsnytt.se","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@oms.no" {
                    $mail=$proxy -replace "@oms.no","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@assetmax.ch" {
                    $mail=$proxy -replace "@assetmax.ch","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@infrontquant.com" {
                    $mail=$proxy -replace "@infrontquant.com","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@lp-software.de" {
                    $mail=$proxy -replace "@lp-software.de","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@vwd.com" {
                    $mail=$proxy -replace "@vwd.com","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@transactionsolutions.de" {
                    $mail=$proxy -replace "@transactionsolutions.de","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@lp-software.com" {
                    $mail=$proxy -replace "@lp-software.com","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                "*@vwd.de" {
                    $mail=$proxy -replace "@vwd.de","@infront.co"
                    $mail=$mail -replace "SMTP:","smtp:"
                    $primary=$proxy
                }
                default{
                    $mail=$null
                    write-warning "$proxy does not match requirements"
                    $notcorrect+=$proxy
                }
            }
            #$domains+=$proxy.split("@")[-1]
        }
    }
    if($mail -ne $null){
        $correct+= New-Object -TypeName PSObject -property @{
        New = $mail
        Primary = $primary
    }
        $getuser=get-aduser $user.samAccountName -Properties *
        If(!($getuser.proxyAddresses -contains $mail)){
            Set-aduser -Identity $user.samAccountName -add @{ProxyAddresses = "$mail"} -ErrorAction Stop -Server $domaincontroller
            write-host -ForegroundColor Green "Added $mail to $($user.displayname)"
        }
    }
}
#$correct
($notcorrect|?{$_ -match "@infrontquant.com"}).count
($notcorrect|?{$_ -match "@lp-software.de"}).count
($notcorrect|?{$_ -match "@vwd.com"}).count
($notcorrect|?{$_ -match "transactionsolutions.de"}).count
($notcorrect|?{$_ -match "@lp-software.com"}).count
($notcorrect|?{($_ -notmatch "transactionsolutions.de") -and ($_ -notmatch "@vwd.com") -and ($_ -notmatch "@lp-software.de") -and ($_ -notmatch "@infrontquant.com")})
$notcorrect.count