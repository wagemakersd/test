Connect-ExchangeOnline
$basefolder="C:\Users\Wagemakersd\OneDrive - Infront ASA\Documents\Projecten\Atlassian\"
$csv=import-csv "$basefolder\infrontfinance.csv"

$notequal=@()
$equal=@()
$doesnotexist=@()
Foreach($U in $csv){
    Try{
        $primary=(Get-Recipient $U.Email -ErrorAction stop).PrimarySmtpAddress
        if($U.email -eq $primary)
        {
            $object= new-object -TypeName PSObject -Property @{
                PrimarySmtpAddress      = $primary
                Attlassian_email        = $u.Email
                Name                    = $u.Name
                Status                  = $u.Status
                "Last active date"      = $u."Last active date"
                "Atlassian ID"          = $u."Atlassian ID"
            }
            $equal+= $object
            write-host -ForegroundColor DarkGreen "$($u.email) eq $primary" 
        }
        else {
            $object= new-object -TypeName PSObject -Property @{
                PrimarySmtpAddress      = $primary
                Attlassian_email        = $u.Email
                Name                    = $u.Name
                Status                  = $u.Status
                "Last active date"      = $u."Last active date"
                "Atlassian ID"          = $u."Atlassian ID"
            }
            $notequal += $object
            write-host -ForegroundColor DarkRed "$($u.email) ne $primary" 
        }
    }
    catch{
        $doesnotexist+=$u
    }
}