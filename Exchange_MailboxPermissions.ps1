connect-exchangeonline -UserPrincipalName "wagemakersd-adm@vwd.com"

$rec=get-recipient -resultsize unlimited|?{$_.RecipientType -eq "Usermailbox"}
$total=@()
Foreach ($item in $rec)
{
    $item.DisplayName
    $permissions=Get-mailboxpermission $item.name |?{($_.user -notmatch "NT AUTHORITY") -and ($_.IsInherited -eq $false)}
    $group=@()

    $object=New-Object -TypeName PsObject -Property @{
        Mailbox             =   $item.Name
        UPN                 =   $item.WindowsLiveID
        DisplayName         =   $item.DisplayName
        permissionsuser     =   $permissions.User -join "| "
    }
    $total+=$object
}
$total|export-csv -path c:\temp\Permissions_SMB02.csv -notypeinformation -encoding UTF8
$permissions|fl 
($total|?{$_.permissionsuser -ne ""}).count


Get-UnifiedGroup|?{$_.EmailAddresses -match "@vwd.com"}|sort DisplayName | Ft DisplayName,EmailAddresses,Notes,ManagedBy,AccessType
(Get-UnifiedGroup).count