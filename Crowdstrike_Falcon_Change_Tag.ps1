[CmdletBinding()]
param(
    [Parameter(Mandatory = $True)]
    [string]$GROUPING_TAGS,

    [Parameter(Mandatory = $False)]
    [switch]$OverwriteTag = $false
)

# Function to write formatted output
function Write-LogMessage {
    param(
        [string]$Message,
        [string]$ComputerName,
        [string]$Color = "White",
        [string]$LogFilePath = "$PSScriptRoot\Log\Falcon_Install.log"    # Default log path
    )
    
    # Create timestamp for the log entry
    $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    
    # Format the log message
    $logEntry = "$timestamp - $ComputerName - $Message"
    
    # Display message in console with color
    Write-Host "$ComputerName - $Message" -ForegroundColor $Color
    
    # Ensure log directory exists
    $logDirectory = Split-Path $LogFilePath -Parent
    if (!(Test-Path $logDirectory)) {
        New-Item -ItemType Directory -Path $logDirectory -Force | Out-Null
    }
    
    # Append message to log file
    Add-Content -Path $LogFilePath -Value $logEntry
}

$servers = Get-ADComputer -Server "deffm-dc3.vwdgroup.net" -Filter {
    enabled -eq "true" -and OperatingSystem -Like '*Windows Server*'
} -Properties * | Sort-Object DNSHostName

# Show server selection grid
$selectedServers = $servers | Select-Object DNSHostName, CanonicalName, Description, LastLogonDate, Modified | Out-GridView -PassThru
 
$selectedServers | ForEach-Object -ThrottleLimit 10 -Parallel {
    $server = $_
    # Check if CrowdStrike is already installed
    if (Get-CimInstance -ComputerName $server.DNSHostName -ClassName win32_service | Where-Object { $_.Name -eq "CSFalconService" }) {
      #  Write-LogMessage "CSFalconService already installed" -ComputerName $server.DNSHostName -Color Yellow
        If($using:OverwriteTag){
            #Change tag on Remote machine
            Invoke-command -ComputerName $server.DNSHostName -ScriptBlock {
                param($GROUPING_TAGS) 
                $SensorRegPath="HKLM:\\SYSTEM\CrowdStrike\{9b03c1d9-3138-44ed-9fae-d9f4c034b88d}\{16e0423f-7058-48c9-a204-725362b67639}\Default"
                $itemproperty=Get-ItemProperty -path $SensorRegPath
                if($itemproperty){
                    if(get-member -InputObject $itemproperty -Name "groupingtags"){
                        Write-host -ForegroundColor Green "$env:Computername - Registry path found!"
                        Set-ItemProperty -Path $SensorRegPath -Name "GroupingTags" -Value $GROUPING_TAGS
                    }
                    Else{
                        New-ItemProperty -Path $SensorRegPath -Name "GroupingTags" -PropertyType String  -Value $GROUPING_TAGS
                    }
                }
                else {
                    Write-host -ForegroundColor Yellow "$env:Computername - Registry path not found!"
                }
            } -ArgumentList $using:GROUPING_TAGS
        } 
        $result
    }
}