
get-mgDeviceAppManagementMobileApp |?{$_.displayname -match "Infront - Office 365"}|select Id,DisplayName
get-mgDeviceAppManagementMobileApp -MobileAppId ae5963b1-b9f9-4223-9e19-1e19ae9ebb6a|fl Id,Displayname,description
get-mggroup -GroupId ((Get-MgDeviceAppManagementMobileAppAssignment -MobileAppId ae5963b1-b9f9-4223-9e19-1e19ae9ebb6a).Id -split "_")[0]

$apps=get-mgDeviceAppManagementMobileApp |?{$_.displayname -notmatch "Update"}


$apppermissions=@()
$groups=@()
foreach ($app in $apps){
    $app.DisplayName
    $RequiredGroup =@()
    $AvailableGroup =@()
    $groupassignment=(Get-MgDeviceAppManagementMobileAppAssignment -MobileAppId $app.id)
    foreach ($assignment in $groupassignment){
        #change it to show all devices and all users

        if(($assignment) -and ($assignment.id -notmatch "acacacac-9df4-4c7d-9d50-4ef0226f57a9") -and ($assignment.id -notmatch "adadadad-808e-44e2-905a-0b7873a8a531")){
            $group=get-mggroup -GroupId ($assignment.Id -split "_")[0]
            If ($assignment.Intent -eq "required"){
                $RequiredGroup += $group.DisplayName
            }
            elseif($assignment.Intent -eq "available"){
                $AvailableGroup += $group.DisplayName
            }
            IF ($group.id -notin $groups.id){
                $groups += $group
            }
        }
        elseif($assignment.id -match "acacacac-9df4-4c7d-9d50-4ef0226f57a9"){
            If ($assignment.Intent -eq "required"){
                $RequiredGroup += "All Users"
            }
            elseif($assignment.Intent -eq "available"){
                $AvailableGroup += "All Users"
            }
        }
        elseif($assignment.id -match "adadadad-808e-44e2-905a-0b7873a8a531"){
            If ($assignment.Intent -eq "required"){
                $RequiredGroup += "All Devices"
            }
            elseif($assignment.Intent -eq "available"){
                $AvailableGroup += "All Devices"
            }
        }
    }

    $apppermissions+=New-Object -typename PSObject -Property @{
        AppID               = $app.ID
        AppDisplayname      = $app.DisplayName
        AppDescription      = $app.Description
        #GroupPermissions    = If($GroupPermissions){$GroupPermissions -join "`n"}else{$null}
        RequiredGroup       = If($RequiredGroup){$RequiredGroup -join ", "}else{$null}
        AvailableGroup      = If($AvailableGroup){$AvailableGroup -join ", "}else{$null}
    }
}

#$apppermissions |?{($_.AvailableGroup -ne $null) -or ($_.RequiredGroup -ne $null)}|select AppID,AppDisplayname,AvailableGroup,RequiredGroup|export-csv -Path c:\temp\intune_app_permissions.csv -NoTypeInformation -Delimiter "|"
#$groups=@()
#$apppermissions.requiredgroup|?{$_ -ne $null}|%{$groups+=$_ -split ", "}
$groups
|select -unique


$GroupApps=@()
Foreach ($group in $groups){
    $available=@()
    $required=@()
    $Required+=($apppermissions|?{$_.RequiredGroup -match $group.DisplayName}).AppDisplayname
    $available+=($apppermissions|?{$_.AvailableGroup -match $group.DisplayName}).AppDisplayname

    $GroupApps+=New-Object -typename PSObject -Property @{
        GroupDisplayname    = $group.DisplayName
        GroupID             = $group.id
        available           = $available -join ", "
        required            = $required -join ", "
    }
}


$Required+=($apppermissions|?{$_.RequiredGroup -match "All Users"}).AppDisplayname
$GroupApps+=New-Object -typename PSObject -Property @{
    GroupDisplayname    = "All Users"
    GroupID             = "acacacac-9df4-4c7d-9d50-4ef0226f57a9"
    available           = "Not listed"
    required            = $required -join ", "
}
$required=@()
$Required+=($apppermissions|?{$_.RequiredGroup -match "All Devices"}).AppDisplayname
$GroupApps+=New-Object -typename PSObject -Property @{
    GroupDisplayname    = "All Devices"
    GroupID             = "adadadad-808e-44e2-905a-0b7873a8a531"
    available           = "Not listed"
    required            = $required -join ", "
}


$GroupApps|select GroupID,GroupDisplayname,required,available|export-csv -Path c:\temp\intune_apps_per_group.csv -NoTypeInformation -Delimiter "|"

$Required+=($apppermissions|?{$_.AvailableGroup -match "All Users"}).AppDisplayname
$GroupApps+=New-Object -typename PSObject -Property @{
    GroupDisplayname    = "All Users"
    GroupID             = "acacacac-9df4-4c7d-9d50-4ef0226f57a9"
    available           = "Not listed"
    required            = $required -join ", "
}
$required=@()
$Required+=($apppermissions|?{$_.AvailableGroup -match "All Devices"}).AppDisplayname
$GroupApps+=New-Object -typename PSObject -Property @{
    GroupDisplayname    = "All Devices"
    GroupID             = "adadadad-808e-44e2-905a-0b7873a8a531"
    available           = "Not listed"
    required            = $required -join ", "
}


($apppermissions|?{$_.RequiredGroup}).count


($apppermissions|?{!$_.RequiredGroup}).count

