﻿#IMPORT USERS/CREATE FOLDERS

$usrsobj=@()
$users=Import-Csv "C:\AMS-ADMINS\Powershell\Input\TIJL_PRODUKTIE_Users.csv"
Foreach($user in $users)
    {
    $upn=$user.upn
    $usrobj=Get-ADUser -filter {UserPrincipalName -eq $upn}
    $usrsobj+=$usrobj
    }

Foreach($usr in $usrsobj)
    {
    $FolderName=$usr.samaccountname
    $Path = "\\deffm-fs1.vwdgroup.net\user\$FolderName"
    
    #Create new Folder
    New-Item -ItemType directory -Path "\\deffm-fs1.vwdgroup.net\user\$FolderName"

    # get permissions
    $acl = Get-Acl -Path $path

    # add a new permission (FullControl, Modify, Read)
    $permission = "$FolderName", 'FullControl', 'ContainerInherit, ObjectInherit', 'None', 'Allow'
    $rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permission
    $acl.SetAccessRule($rule)

    # set new permissions
    $acl | Set-Acl -Path $path 
    }