function Sync-Users ($Property,$value,$Destgroup,$property2,$value2){
    # Add users to Group
    If($property2){
        $value="*"+$value+"*"
        $value2="*"+$value2+"*"
        $filteredusers=Get-aduser -filter {($property -like $value) -and ($property2 -like $value2)} -Properties *
        #$filteredusers| FT Name, Office, Department
        #$filteredusers=Get-aduser -filter {($property -eq $value) -and ($property2 -eq $value2)} -Properties *
    }
    Else{
        $value="*"+$value+"*"
        $filteredusers=Get-aduser -filter {($property -like $value)} -Properties *
        #$filteredusers=Get-aduser -filter {($property -eq $value)} -Properties *
    }
    $filteredusers|?{($_.memberof -notcontains $Destgroup) -and ($_.enabled -ne $False) -and ($_.name -notmatch "(test)") -and ($_.name -notmatch " - ") -and ($_.name -notmatch "_") -and ($_.distinguishedName -notmatch "OU=Systems,") -and ($_.distinguishedName -notmatch "OU=External,") -and ($_.name -notmatch "-adm") -and ($_.UserPrincipalName -match "@vwd.com")} | ForEach-Object {
        Add-ADPrincipalGroupMembership –Identity $_ –MemberOf $Destgroup; 
        Add-Content $LOG "$mydate - User added:'$_' to '$group'";
        Write-Host "$mydate - User added:'$_' to '$group'";
    }

    # Remove users from Group if not within the properties/values
    Get-ADGroupMember –Identity $Destgroup | ForEach-Object {
        If ($_.objectClass -eq "user") {
            $aduser=get-aduser $_.samaccountname -Properties *
                If(($aduser.$property -notlike $value) -or ($aduser.$property2 -notlike $value2) -or ($aduser.name -match " - ") -or ($aduser.name -match "_") -or ($aduser.name -match "-adm") -or ($aduser.UserPrincipalName –notmatch "@vwd.com") -or ($aduser.enabled -eq $false) -or ($aduser.name -match "(test)")){
                #If(($aduser.$property -ne $value) -or ($aduser.$property2 -ne $value2) -or ($aduser.name -match " - ") -or ($aduser.name -match "_") -or ($aduser.name -match "-adm") -or ($aduser.UserPrincipalName –notmatch "@vwd.com") -or ($aduser.enabled -eq $false) -or ($aduser.name -match "(test)")){
                Remove-ADPrincipalGroupMembership –Identity $_ –MemberOf $Destgroup –Confirm:$false;
                Add-Content $LOG "$mydate - User removed:'$_' from '$Destgroup'"; 
                write-host "$mydate - User removed:'$_' from '$Destgroup'";
            }
        }
    }
}
function Sync-ReportLine ($Property,$value,$Destgroup){
    # Add users to Group
    $filteredusers=Get-aduser -filter {($property -eq $value)} -Properties *
    $filteredusers|?{($_.memberof -notcontains $Destgroup) -and ($_.enabled -ne $False) -and ($_.name -notmatch "(test)") -and ($_.name -notmatch " - ") -and ($_.name -notmatch "_") -and ($_.distinguishedName -notmatch "OU=Systems,") -and ($_.distinguishedName -notmatch "OU=External,") -and ($_.name -notmatch "-adm") -and ($_.UserPrincipalName -match "@vwd.com")} | ForEach-Object {
        Add-ADPrincipalGroupMembership –Identity $_ –MemberOf $Destgroup; 
        Add-Content $LOG "$mydate - User added:'$_' to '$Destgroup'";
        Write-Host "$mydate - User added:'$_' to '$Destgroup'";
    }

    # Remove users from Group if not within the properties/values
    Get-ADGroupMember –Identity $Destgroup | ForEach-Object {
        If ($_.objectClass -eq "user") {
            $aduser=get-aduser $_.samaccountname -Properties *
                If(($aduser.$property -ne $value) -or ($aduser.$property2 -ne $value2) -or ($aduser.name -match " - ") -or ($aduser.name -match "_") -or ($aduser.name -match "-adm") -or ($aduser.UserPrincipalName –notmatch "@vwd.com") -or ($aduser.enabled -eq $false) -or ($aduser.name -match "(test)")){
                Remove-ADPrincipalGroupMembership –Identity $_ –MemberOf $Destgroup –Confirm:$false;
                Add-Content $LOG "$mydate - User removed:'$_' from '$Destgroup'"; 
                write-host "$mydate - User removed:'$_' from '$Destgroup'";
                
                $Nestinggroup=("ReportLine_"+ $_.samaccountname)
                if(get-adgroup -filter {name -eq $Nestinggroup}){
                    $member=get-adgroup -filter {name -eq $Nestinggroup} -Properties MemberOf,displayname
                    $getdestgroup=get-adgroup $Destgroup
                    if($getdestgroup.DistinguishedName -in $member.memberof){
                        Remove-ADPrincipalGroupMembership –Identity $member –MemberOf $Destgroup –Confirm:$false;
                        Add-Content $LOG "$mydate - Group removed:'$($member.displayname)' from '$Destgroup'"; 
                        write-host "$mydate - Group removed:'$($member.displayname)' from '$Destgroup'";
                    }
                }
            }
        }
    }
}

$base="\\deffm-adfs2.vwdgroup.net\ams-admin$\Scheduled_Tasks"
$mydate =  get-date -Format "yyyy-MM-dd HH:mm"
$LOG="$base\log\Auto-groups-beta.log"
#$users=get-aduser -filter * -Properties company,department
#$users.department |select -Unique|out-file  "$base\Input\Departments.list"
$locations = import-csv -Path "$base\Input\locations.csv"
$companies= get-content -path "$base\Input\companies.list"
$departments = get-content -path "$base\Input\Departments_beta.list"

<#
$location=$locations|?{$_.usroffice -eq "Amsterdam"}
$property="Office"
$value="Amsterdam"
$property2="Department"
$value2="Operations"
$Department="Operations"
#>

#Maintain Location and Department groups
foreach ($location in $locations){  
    $officebase="Infront - Office "

    $group=$officebase + $location.usroffice + " [List]"
    write-host "$group"
    $Destgroup=(Get-adgroup -filter {name -eq $group}).DistinguishedName
    If($Destgroup){
        #sync-users -property Office -value $location.usroffice -Destgroup $Destgroup
    }
    else {
        Write-Warning "'$group' group has not been found"
        Add-Content $LOG "WARNING: '$group' group has not been found"
    }

    Foreach ($Department in $Departments){
        $group="autogroup-"+$Department + "_" + $location.usroffice
        write-host "$group"
        $Destgroup=(Get-adgroup -filter {name -eq $group}).DistinguishedName

        If($Destgroup){
            sync-users -property Office -value $location.usroffice -property2 Department -value2 $Department -Destgroup $Destgroup
        }
        else {
            <#Create groups
            Write-host -ForegroundColor Green "Creating '$group'"
            Add-Content $LOG "Creating '$group'"
            New-ADGroup -DisplayName $group -Name $group -GroupCategory Security -GroupScope Global -Path "OU=Test,OU=auto-groups,OU=groups,OU=Global,DC=vwdgroup,DC=net" -Description "Automatic user group based on Department and Location property"
             #>
            
            Write-Warning "'$group' group has not been found"
            Add-Content $LOG "WARNING: '$group' group has not been found"
        }
    }
}


#Maintain company groups 
$company="Infront Financial Technology B.V."
$value=$company
Foreach ($company in $companies){
    $Destgroup=(Get-adgroup -filter {Displayname -eq $company}).DistinguishedName
    If($Destgroup){
    sync-users -property Company -value $company -Destgroup $Destgroup
    }
    else {
        Write-Warning "'$company' group has not been found"
        Add-Content $LOG "WARNING: '$company' group has not been found"
    }
}

#Create and maintain a flat group from nested groups
$flatgroups=@("AMS-LIST-00050")
Foreach($group in $flatgroups){
    $nestedgroup=get-adgroup $group -Properties Displayname
    $flatgroup=$nestedgroup.Displayname -replace "list", "Flat"
    $FlatSamacc=$nestedgroup.SamAccountName + "_Flat"
    IF(!(get-adgroup -filter {(Displayname -eq $flatgroup) -or (Samaccountname -eq $FlatSamacc)})){
        New-ADGroup -DisplayName $flatgroup -Name $flatgroup -SamAccountName $FlatSamacc -GroupCategory Security -GroupScope Global -Path "OU=Test,OU=auto-groups,OU=groups,OU=Global,DC=vwdgroup,DC=net" -Description "Flat group for: $($nestedgroup.Displayname)"
        while (!(get-adgroup -filter {(Displayname -eq $flatgroup) -or (Samaccountname -eq $FlatSamacc)})){
            Write-Host "'$flatgroup' not yet created, please wait...";
            sleep 5
        }
        Write-host -ForegroundColor Green "$mydate - Created flatgroup '$flatgroup'"
        Add-Content $LOG "$mydate - Creating '$flatgroup'"
    }
    $members=Get-ADGroupMember -Identity $group -Recursive |?{$_.objectclass -eq "user"}
    $flatmembers=Get-ADGroupMember -Identity $FlatSamacc
    If($flatmembers){
        Compare-Object -ReferenceObject $members.samaccountname -DifferenceObject $flatmembers.samaccountname|%{
            If($_|?{$_.sideindicator -eq "=>"}){
                Remove-ADPrincipalGroupMembership –Identity $_.InputObject –MemberOf $FlatSamacc –Confirm:$false;
                Add-Content $LOG "$mydate - User removed:'$($_.InputObject)' from '$flatgroup'"; 
                write-host "$mydate - User removed:'$($_.InputObject)' from '$flatgroup'";
            }
            Elseif($_|?{$_.sideindicator -eq "<="}){
                Add-ADPrincipalGroupMembership –Identity $_.InputObject –MemberOf $FlatSamacc;
                Add-Content $LOG "$mydate - User added:'$($_.InputObject)' to '$flatgroup'";
                Write-Host "$mydate - User added:'$($_.InputObject)' to '$flatgroup'";
            }
        }
    }
    Else{
        $members|%{
            Add-ADPrincipalGroupMembership –Identity $_ –MemberOf $FlatSamacc;
            Add-Content $LOG "$mydate - User added:'$($_.samaccountname)' to '$flatgroup'";
            Write-Host "$mydate - User added:'$($_.samaccountname)' to '$flatgroup'";
        }
    }
}

################################################################
#ReporingLine groups
################################################################
#Get all unique managers (which have internal users)
$adusers= get-aduser -filter {EmployeeNumber -like "*"} -Properties manager,Employeenumber
$managers=$adusers.manager |select -Unique
$managers.count

#$list=@()
foreach($manager in $managers){
    #$managername=get-aduser vangoola -properties DisplayName,manager
    $managername=get-aduser -Filter{(DistinguishedName -eq $manager)} -properties DisplayName,manager
    If($managername){
        $groupname=("ReportLine_"+ $managername.SamAccountName)

        #Create reportline group if it doesn't exist
        if(!(get-adgroup -filter {name -eq $groupname})){
            new-adgroup -name $groupname -DisplayName $groupname -GroupScope Global -GroupCategory Security -Path "OU=Test,OU=auto-groups,OU=groups,OU=Global,DC=vwdgroup,DC=net" -Description "Automatic user group based on Reporting Lines"
            Add-Content $LOG "$mydate - Group created: $Groupname";
            Write-Host "$mydate - Group created: $Groupname";
        }

        #Collect information from that group.
        try{
            $group=get-adgroup $groupname -Properties * -ErrorAction stop
        }
        catch{
            Add-Content $LOG "$mydate - Group not found: $Groupname";
            Write-Host -ForegroundColor RED "$mydate - roup not found: $Groupname";
            break
        }

        #Create groupnesting for managers
        IF($managername.Manager){
            $Nestinggroupname=("ReportLine_"+ (get-aduser $managername.Manager).samaccountname)
            $Nestinggroup=get-adgroup -filter {Samaccountname -eq $Nestinggroupname}
            $groupname
            $Nestinggroupname
            if($Nestinggroup){
                If($group.memberof -notcontains $Nestinggroup.DistinguishedName){
                    Try{
                        Add-ADPrincipalGroupMembership –Identity $groupname –MemberOf $Nestinggroupname -ErrorAction stop
                        Add-Content $LOG "$mydate - Group added: '$groupname' to '$Nestinggroupname'";
                        Write-Host "$mydate - Group added: '$groupname' to '$Nestinggroupname'";
                    }
                    Catch{
                        Add-Content $LOG "$mydate - Could not add group: '$groupname' to '$Nestinggroupname'";
                        Write-Host -ForegroundColor RED "$mydate - Could not add group: '$groupname' to '$Nestinggroupname'";
                    }
                }
            }
        }
        
        #Sync users which have the specific manager filled in to the group. (See function in top)
        Sync-ReportLine -property Manager -value $Manager -Destgroup $group.DistinguishedName

<#
        $users=Get-ADUser -Filter {(Manager -eq $manager) -and (Enabled -eq $True)} -properties *
        foreach ($user in $users){
            $list+=New-Object -TypeName psobject -Property @{
                Manager         = $managername.DisplayName
                User            = $user.Displayname
                UPN             = $user.UserPrincipalName
                Department      = $user.Department
                Enabled         = $user.Enabled
                Office          = $user.office
                Title           = $user.Title
                City            = $user.City
                EmployeeNumber  = $user.EmployeeNumber
            }
        }
#>
    }
}
#$list|FT Manager,User,Department,Title,Office,EmployeeNumber

#Reportinglines groups clean-up
$groups=Get-ADGroup -Filter {Name -like "ReportLine_*"}
Foreach($group in $groups){
    if(!(get-adgroupmember $group)){
        write-host "NO reporting lines for $($group.name)"
        try{
            remove-adgroup $group  –Confirm:$false -ErrorAction stop
            Add-Content $LOG "$mydate - Group Removed: $group";
            Write-Host "$mydate - Group Removed: $group";
        }
        Catch{
            Add-Content $LOG "$mydate - Could not remove Group: $group";
            Write-Host -ForegroundColor RED "$mydate - Could not remove Group: $group";
        }
    }
}