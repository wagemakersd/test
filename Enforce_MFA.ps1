Connect-MsolService

$Users = Get-MSolUser -All
$All=@()
foreach ($user in $users)
{
    If (($User.StrongAuthenticationMethods.methodtype -notcontains "PhoneAppOTP") -and ($user.UserPrincipalName -match "@vwd.com"))
    {
        $Object = new-object -TypeName PSObject -Property @{
            Displayname     = $user.DisplayName
            UPN             = $user.UserPrincipalName
            Method          = $User.StrongAuthenticationMethods.methodtype
            Office          = $user.Office
            BlockCredential = $user.BlockCredential
            IsLicensed      = $user.IsLicensed
        }
        $All += $object
    }
  <#  If ($user.UserPrincipalName -notmatch "@vwd.com")
    {
        $Object = new-object -TypeName PSObject -Property @{
            Displayname     = $user.DisplayName
            UPN             = $user.UserPrincipalName
            Method          = $User.StrongAuthenticationMethods.methodtype
            Office          = $user.Office
            BlockCredential = $user.BlockCredential
            IsLicensed      = $user.IsLicensed
        }
        $All += $object
    }#> 
}
$all|ft
$all|?{($_.DisplayName -match "Nyhetsbyrån")}|ft
$all|?{($_.Method -ne $null)}|ft
($all|?{($_.Method -eq $null)}).count
$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.UPN -notmatch "-RES-") -and ($_.Office -ne $null)}|fT
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.UPN -notmatch "-RES-") -and ($_.Office -ne $null)}).count

$all|?{($_.BlockCredential -eq $false) -and (($_.DisplayName -match "[[SMB]]") -or ($_.UPN -match "-RES-"))}|FT
($all|?{($_.BlockCredential -eq $false) -and (($_.DisplayName -match "[[SMB]]") -or ($_.UPN -match "-RES-"))}).count

$all|?{($_.BlockCredential -eq $false)}|fT
($all|?{($_.BlockCredential -eq $false)}).count

$all|?{($_.DisplayName -match "[[SMB]]") -and ($_.BlockCredential -eq $false)}|fT
($all|?{($_.DisplayName -match "[[SMB]]") -and ($_.BlockCredential -eq $false)}).count

$all|?{($_.DisplayName -match "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.IsLicensed -eq $false)}|fT
($all|?{($_.DisplayName -match "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.IsLicensed -eq $false)}).count

$all|?{($_.DisplayName -match "[[SMB]]") -and ($_.BlockCredential -eq $true) -and ($_.IsLicensed -eq $true)}|fT
($all|?{($_.DisplayName -match "[[SMB]]") -and ($_.BlockCredential -eq $true) -and ($_.IsLicensed -eq $true)}).count

$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -eq "Amsterdam") -and ($_.UPN -notmatch "-RES-")}|ft
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -eq "Amsterdam") -and ($_.UPN -notmatch "-RES-")}).count
$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "frankfurt") -and ($_.UPN -notmatch "-RES-")}|ft
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "frankfurt") -and ($_.UPN -notmatch "-RES-")}).count
$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Kaiserslautern") -and ($_.UPN -notmatch "-RES-")}|ft
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Kaiserslautern") -and ($_.UPN -notmatch "-RES-")}).count
$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Milano") -and ($_.UPN -notmatch "-RES-")}|ft
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Milano") -and ($_.UPN -notmatch "-RES-")}).count
$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Dortmund") -and ($_.UPN -notmatch "-RES-")}|ft
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Dortmund") -and ($_.UPN -notmatch "-RES-")}).count
$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Antwerp") -and ($_.UPN -notmatch "-RES-")}|ft
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Antwerp") -and ($_.UPN -notmatch "-RES-")}).count
$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Zürich") -and ($_.UPN -notmatch "-RES-")}|ft
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Zürich") -and ($_.UPN -notmatch "-RES-")}).count
$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Schweinfurt") -and ($_.UPN -notmatch "-RES-")}|ft
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Schweinfurt") -and ($_.UPN -notmatch "-RES-")}).count
$all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Rimpar") -and ($_.UPN -notmatch "-RES-")}|ft
($all|?{($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false) -and ($_.Office -match "Rimpar") -and ($_.UPN -notmatch "-RES-")}).count
$all|?{($_.IsLicensed -eq $false) -and ($_.DisplayName -notmatch "[[SMB]]") -and ($_.BlockCredential -eq $false)} |ft


$groupmembers=Get-ADGroupMember Global-Azure-MFA_Infront
$tokengroupmembers=Get-ADGroupMember Global-Azure-MFA-Token
$OU=get-aduser -searchbase "OU=users,OU=FFM,OU=de,DC=vwdgroup,DC=net" -filter {(Enabled -eq $true) -and (Userprincipalname -like "*@vwd.com")} -Properties DisplayName,office,Title,Department |?{$_.distinguishedname -notmatch "OU=Systems"}
$Ou|ft
$added=@()
foreach ($usr in $OU)
{
    if (($usr.samaccountname -notin $groupmembers.samaccountname)-and ($usr.samaccountname -notin $tokengroupmembers.samaccountname))
    {
        "Enforcing $($usr.DisplayName)"
        add-ADGroupMember -Identity Global-Azure-MFA_Infront -Members $usr
        $added += $usr
    }
}
$added.DisplayName

|%{Add-ADGroupMember -Identity Global-Azure-MFA_Infront -Members $_ -ErrorAction SilentlyContinue}

get-aduser wagemakersd -Properties *


$groupmembers=Get-ADGroupMember Global-Azure-Lockdown_Datacenter
$groupmembers+=Get-ADGroupMember Global-Azure-Lockdown_Office
$groupmembers+=Get-ADGroupMember Global-Azure-Lockdown_Trusted_Locations
$Shared=$all|?{($_.BlockCredential -eq $false) -and (($_.DisplayName -match "[[SMB]]") -or ($_.UPN -match "-RES-"))}
$unristricted=@()
$ristricted=@()
foreach($entry in $shared)
{
    if($entry.displayname -in $groupmembers.name)
    {
        $ristricted+=$entry
    }
    else 
    {
        $displayname=$entry.displayname
        $found=$null
        $found=get-aduser -filter{Displayname -eq $displayname} -Properties *
        if ($found.name -in $groupmembers.name)
        {
            $ristricted+=$entry
        }
        else 
        {
            if ($found.DisplayName -match "vwd -"){
            $found|FL name,PasswordLastSet,lastlogondate,Modified,whenchanged,whenCreated
            }
            $unristricted+=$entry
        }
    }
}
$unristricted.Count
$unristricted.displayname|sort
$ristricted.count
$ristricted.displayname



$licensed=$all|?{($_.DisplayName -match "[[SMB]]") -and ($_.BlockCredential -eq $true) -and ($_.IsLicensed -eq $true)}
$array=@()
foreach ($entry in $licensed)
{
    
$array+=Get-Mailbox $entry.upn -resultsize unlimited | Select-Object name,@{n="PrimarySize";e={(Get-MailboxStatistics $_.identity).totalItemsize}}, 
@{n="Primary Item Count";e={(Get-MailboxStatistics $_.identity).ItemCount}},  
@{n="ArchiveSize";e={(Get-MailboxStatistics -archive $_.identity).TotalItemSize}}, 
@{n="Archive Item Count";e={(Get-MailboxStatistics -archive $_.identity).ItemCount}}

}
$array|?{$_.PrimarySize -match "GB"}|sort ArchiveSize|ft