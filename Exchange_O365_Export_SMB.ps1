Connect-ExchangeOnline

$SMB=Get-mailbox -RecipientTypeDetails Shared |?{$_.LitigationHoldEnabled -eq $false}
$SMB[1]|fl *
$include = ($SMB|gm |?{($_.MemberType -eq "Property") -and ($_.Definition -notmatch "Deserialized")}).name
#$dl[1]|select $include

$SMB |select $include |Write-SqlTableData -ServerInstance "CL-DS-MG1" -DatabaseName "dbBeheer" -SchemaName "dbo" -TableName "O365_SMB" -force
$Alias_array=@()
foreach($entry in $SMB){

    $entry.EmailAddresses|%{
        $Object = new-object -TypeName PSObject -Property @{
            EmailAddress        = $_
            PrimarySmtpAddress  = $entry.PrimarySmtpAddress
            Id                  = $entry.ID
        }
        $Alias_array += $object
    }
}

$Alias_array |Write-SqlTableData -ServerInstance "CL-DS-MG1" -DatabaseName "dbBeheer" -SchemaName "dbo" -TableName "O365_SMB_Alias" -force

$Members_array=@()
foreach($entry in $SMB){
    $entry.PrimarySmtpAddress
    $members=Get-mailboxpermission -Identity $entry.PrimarySmtpAddress|?{($_.IsInherited -eq $false) -and ($_.user -notmatch "NT AUTHORITY")}
    $members|%{
        $Object = new-object -TypeName PSObject -Property @{
            User                    = $_.User
            AccessRights            = $_.AccessRights -join ","
            SMB_name                = $entry.name
            SMB_PrimarySmtpAddress  = $entry.PrimarySmtpAddress
        }
        $Members_array += $object
    }
}

$Members_array |Write-SqlTableData -ServerInstance "CL-DS-MG1" -DatabaseName "dbBeheer" -SchemaName "dbo" -TableName "O365_SMB_Members" -force