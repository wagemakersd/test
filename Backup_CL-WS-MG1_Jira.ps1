﻿clear
<#
        .SYNOPSIS
        Backup files from source location to backup location.

        If $Zip is $True, subdirectories are always included.
        If $Zip is $False, subdirectories are only included if $Recursive is set to $True
#>

$date = Get-Date -Format yyyyMMdd
$time = Get-Date -Format HHmmss

##########################################################################
# Variables
##########################################################################
$JobName = 'CL-WS-MG1 (Jira)'
$Source = '\\cl-ws-mg1\d$\JIRA\'
$Destination = '\\cl-mg-bu1\pcbackup$\cl-ws-mg1\Jira\'
$Retention = 8
$Zip = $True
$Recursive = $True 

### DO NOT CHANGE VARIABLES BELOW ###
$logLoction = "$env:Temp"
$logFile = "$JobName-$date-$time.log"
$log = $logLoction + '\' + $logFile

$smtpServer = "KG-MG-MS2"
$msgFrom = "CL-MG-DIV1 <no-reply@vwd.nl>"
$msgTo = "Logging vwd group Netherlands  <TBMLogging@vwd.nl>"
#$msgTo = "Vredevoort, Barry <bvredevoort@vwd.com>"
$msgSubject = "$JobName : File Backup finished"
$msgPriority = "Normal"
$msgBody = "Backup Job completed.`r`nFor results of Backup Job see attached log.`r`n`r`n"

##########################################################################
# Functions
##########################################################################
Function ErrorTrap
{
    $msgSubject = "$JobName : Error during file backup"
    $msgBody = "Errors occured during backup.`r`nSee attached log for details.`r`n`r`n"
    $msgPriority = "High"
    Send-MailMessage -From $msgFrom -To $msgTo -Subject $msgSubject -Body $msgBody -Priority $msgPriority -SmtpServer $smtpServer -Attachments $log
    exit
}

##########################################################################
# Initialize log
##########################################################################
#Remove old logs for this job
if ((Get-ChildItem -Path "$logLoction\$JobName*.log").Count -gt 0) {
    Remove-Item -Path "$logLoction\$JobName*.log"
}

"Backup Job: $JobName" | Out-File -FilePath $log
"Server: $env:COMPUTERNAME`r`n" | Out-File -FilePath $log -Append
"Script: " + $MyInvocation.MyCommand.Path | Out-File -FilePath $log -Append
"Started: " + $(Get-Date -f 'yyyy-MM-dd HH:mm') + "`r`n" | Out-File -FilePath $log -Append
"Source: $Source" | Out-File -FilePath $log -Append
"Destination: $Destination" | Out-File -FilePath $log -Append
"Retention: $Retention" | Out-File -FilePath $log -Append
"Compression: $Zip`r`n" | Out-File -FilePath $log -Append
if (-not $Zip) { "Recursive: $Recursive" | Out-File -FilePath $log -Append }

#"IIS Log Cleanup Results van " + $(Get-Date -f 'yyyy-MM-dd HH:mm') | Out-File -FilePath $log
#"Gebruikte csv-file: $csv" | Out-File -FilePath $log -Append
#"Te archiveren maand: $monthToArchive`r`n" | Out-File -FilePath $log -Append


##########################################################################
# Checks
##########################################################################
#7-zip check
if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {
    "ERROR: C:\ProgramFiles\7-Zip\7z.exe is not available."  | Out-File -FilePath $log -Append
    ErrorTrap
} else {
    set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
}

#Source Check
if (-not (Test-Path $Source)) {
    "ERROR: Source Path ($Source) is not available"  | Out-File -FilePath $log -Append
    ErrorTrap
}

#Destination Check
if (-not (Test-Path $Destination)) {
    "ERROR: Destination Path ($Destination) is not available" | Out-File -FilePath $log -Append
    ErrorTrap
}

#Make sure Source path ends with \
if ($Source.Substring($Source.Length-1,1) -ne '\') {
    $Source = $Source + '\'
}

#Make sure Destination path ends with \
if ($Destination.Substring($Destination.Length-1,1) -ne '\') {
    $Destination = $Destination + '\'
}

##########################################################################
# Main
##########################################################################
if ($zip) {
    $BackupFile = "$Destination$JobName $date-$time.zip"
    sz a -tzip -mx=9 -mmt=off $BackupFile $Source | Out-File -FilePath $log -Append
    if (($LASTEXITCODE -ne 0) -and ($LASTEXITCODE -ne 1)){ 
        ErrorTrap 
    }
    if ($LASTEXITCODE -eq 1){
        $msgSubject = "$JobName : File Backup finished with warnings"
        $msgBody = "Backup Job completed with warnings.`r`nFor details of Backup Job see attached log.`r`n`r`n"
    }
} else {
    $BackupLocation = "$Destination$JobName $date-$time"
    if (-not (Test-Path $BackupLocation)) {
        New-Item $BackupLocation -ItemType Directory | Out-Null
    }
    if ($Recursive) {
        Robocopy.exe "$Source" "$BackupLocation" /E /ZB /R:5 /W:5 | Select-Object -last 12 | Out-File -FilePath $log -Append
    } else {
        Robocopy.exe "$Source" "$BackupLocation" /ZB /R:5 /W:5 | Select-Object -last 12 | Out-File -FilePath $log -Append
    }
        if ($LASTEXITCODE -ne 1) { 
        ErrorTrap 
    }
}

##########################################################################
# Remove old backups
##########################################################################
$Backups = (Get-ChildItem -Path "$Destination$JobName*" | Sort-Object creationtime)
if (($Backups|measure).count -gt $Retention)
{
    $counter = 0
    while (((($Backups|measure).count) - $counter) -gt $Retention)
    {
        if ((Get-Item $Backups[$counter].FullName) -is [System.IO.DirectoryInfo])
        {
            Remove-Item $Backups[$counter].FullName -Force -Recurse
        } else {
            Remove-Item $Backups[$counter].FullName -Force
        }
        $counter++
    }
}

##########################################################################
# Send results
##########################################################################
Send-MailMessage -From $msgFrom -To $msgTo -Subject $msgSubject -Body $msgBody -Priority $msgPriority -SmtpServer $smtpServer -Attachments $log

##########################################################################
# Cleanup
##########################################################################
Remove-Item -Path $log