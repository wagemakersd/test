﻿<#
CHange DNS servers based on netconnectionID and dns domain
#>

param (
    [switch]$all
)

#$server_names = "kg-as-tst1","kg-cm-db-tst1","cl-er-db-tst1","kg-pu-wb-ref1","kg-pu-wb-ref2","kg-pu-wb-tst2"
#$server_names = "KG-MG-MS2"
$dnsservers =@("10.130.13.113","10.130.13.14")

if ($all) {
    $server_names = Get-ADComputer -LDAPFilter "(&(operatingsystem=*server*)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))"  | select name | sort name
    $server_names = $server_names.name
} else {
    $server_names = Read-Host -Prompt "Provide servername to change DNS servers"
}

foreach ($server in $server_names) { 
    $result =  get-wmiobject win32_pingstatus -filter "address='$server'" 
    if ($result.statuscode -eq 0) { 
        $remoteNic = get-wmiobject -class win32_networkadapter -computer $server | where-object {$_.netconnectionID -like "vwd*"} 
        $index = $remotenic.index 
        $DNSlist = $(get-wmiobject win32_networkadapterconfiguration -computer $server -Filter ‘IPEnabled=true’ | where-object {$_.index -eq $index}).dnsserversearchorder
        Write-host "Changing DNS IP's on $server $DNSlist"
            if ($DNSlist -eq $null) {
                $Networks = Get-WmiObject -Class Win32_NetworkAdapterConfiguration | where-object {$_.dnsdomain -eq "corp.tijdbeursmedia.com"}
                $DNSServers = $Network.DNSServerSearchOrder
                Write-host "Only one adapter on $server with servers $DNSServers" | ft name, netconnectionID
                }
               
                
            }
      
    else { 
        Write-host "$server is down cannot change IP address" -b "Red" -foregroundcolor "white" 
}

}