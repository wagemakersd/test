﻿Clear-Host
<#
Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\PowerShell\1\ShellIds" -Name "ConsolePrompting" -Value $True
Write-Host "Connecting to O365..."
$msolcred = get-credential -Message "O365 Admin Account"
connect-msolservice -credential $msolcred
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session
Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\PowerShell\1\ShellIds" -Name "ConsolePrompting" -Value $False
#>
Write-Host ""
$OldSAMAccount = Read-Host -Prompt "SAM Account to rename"
Write-Host ""

if ( -not [bool] (Get-ADUser -Filter { SamAccountName -eq $OldSAMAccount }) ) {
    Write-Host "SAM Account $OldSAMAccount does not exists or is not a user account"
    Break
}

$PDC = (Get-ADDomain).PDCEmulator
$ADUser = Get-ADUser -Identity $OldSAMAccount -Server $PDC -Properties *

Write-Host "Current settings for" $ADUser.SamAccountName
Write-Host "Display Name    :" $ADUser.DisplayName
Write-Host "Email Address   :" $ADUser.EmailAddress
Write-Host "Proxy Addresses :" $ADUser.proxyAddresses
Write-Host "SAM Account Name:" $ADUser.SamAccountName
Write-Host "Given Name      :" $ADUser.GivenName
Write-Host "Surname         :" $ADUser.Surname
Write-Host "UPN             :" $ADUser.UserPrincipalName
Write-Host "Mail Nickname   :" $ADUser.mailNickname
Write-Host ""

$NewGivenName = Read-Host -Prompt $("New Given Name, enter to keep current (" + $ADUser.GivenName + ")")
If ($NewGivenName -eq '') { $NewGivenName = $ADUser.GivenName }
$NewSurname = Read-Host -Prompt $("New Surname, enter to keep current(" + $ADUser.Surname + ")")
If ($NewSurname -eq '') { $NewSurname = $ADUser.Surname }
Write-Host

$NewSamAccountName = $(($NewSurname -replace '\s','') + $NewGivenName[0]).ToLower()
$NewDisplayName = $NewSurname + ", " + $NewGivenName
$NewEmail = $($NewGivenName[0] + ($NewSurname -replace '\s','') + '@vwd.com').ToLower()
$NewMailNickname = $($NewGivenName[0] + ($NewSurname -replace '\s','')).ToLower()
$NewUPN = $NewEmail

foreach ($address in $ADUser.proxyAddresses) {
    if ($address.StartsWith('SMTP:')) { $PrimaryEmailAddress = $address }
    if ($address.StartsWith('sip:')) { $SipAddress = $address }
}
$NewProxyAddresses = $ADUser.proxyAddresses
$NewProxyAddresses.Remove($SipAddress)
$NewProxyAddresses.Add("sip:" + $NewEmail) | Out-Null
$NewProxyAddresses.Remove($PrimaryEmailAddress)
$NewProxyAddresses.Add($PrimaryEmailAddress.ToLower()) | Out-Null
$NewProxyAddresses.Add("SMTP:" + $NewEmail ) | Out-Null

Write-Host "Checking new SAM account and email address (this may take several minutes)..."

if ( [bool] (Get-ADUser -Filter { SamAccountName -eq $NewSamAccountName }) ) {
    Write-Host "New SAMAccount $NewSamAccountName already exists"
    Break
}
if ( [bool] (get-recipient -ResultSize unlimited | where {$_.emailaddresses -like "*$NewEmail*"}) ) {
    Write-Host "New email address $NewEmail already exists"
    Break
}
Write-Host ""

Write-Host "New settings"
Write-Host "Display Name    :" $NewDisplayName
Write-Host "Email Address   :" $NewEmail
Write-Host "Proxy Addresses :" $NewProxyAddresses
Write-Host "SAM Account Name:" $NewSamAccountName
Write-Host "Given Name      :" $NewGivenName
Write-Host "Surname         :" $NewSurname
Write-Host "UPN             :" $NewUPN
Write-Host "Mail Nickname   :" $NewMailNickname
Write-Host

$Answer = Read-Host "Do you want to continue and submit the changes (Yes/No)"
While ($Answer -notin ("Yes","No")) {
    Write-Host "Please answer Yes or No."
    $Answer = Read-Host "Do you want to continue and submit the changes (Yes/No)"
}

If ($Answer -eq "No") { 
    Write-Host "Account has not been changed"
}
elseif ($Answer -eq "Yes") {
    Write-Host "Writing new settings for account"
    
    Get-ADUser -Identity $OldSAMAccount -Server $PDC -Properties * |`
    Set-ADUser -Server $PDC `
               -DisplayName $NewDisplayName `
               -EmailAddress $NewEmail `
               -SamAccountName $NewSamAccountName `
               -GivenName $NewGivenName `
               -Surname $NewSurname `
               -UserPrincipalName $NewUPN `
               -Replace @{MailNickName=$NewMailNickname} `
               -PassThru | `
    Set-ADUser -Server $PDC -Remove @{ProxyAddresses = $SipAddress} -PassThru | `
    Set-ADUser -Server $PDC -Add @{ProxyAddresses = ("sip:" + $NewEmail)} -PassThru | `
    Set-ADUser -Server $PDC -Remove @{ProxyAddresses = $PrimaryEmailAddress } -PassThru | `
    Set-ADUser -Server $PDC -Add @{ProxyAddresses = ($PrimaryEmailAddress.ToLower())} -PassThru | `
    Set-ADUser -Server $PDC -Add @{ProxyAddresses = ("SMTP:" + $NewEmail )} -PassThru | `
    Rename-ADObject -NewName $NewSamAccountName -Server $PDC -PassThru
    
    
} else {
    Write-Host "Unknown error occured"
}
