connect-msolservice
$totalcsv=import-csv -path "c:\temp\total-users.csv"
$totalcsv|ft targetupn,targetemail
$totalcsv.count
($totalcsv|?{$_.sourceemail -ne $_.sourceupn}).count
($totalcsv|?{$_.targetemail -ne $_.targetupn}).count


$found=@()
$failed=@()
$upn=@()
$Mail=@()
foreach($entry in $totalcsv)
{
    $entry.sourceemail
    try{
        $upn+=get-recipient $entry.sourceupn -erroraction stop
    }
    Catch{
        try{
            $mail+=get-recipient $entry.sourceemail -erroraction stop
        }
        catch
        {
            $failed+=$entry
        }
    }
}
$mail.count
$upn.count
$failed|ft


$revwd=get-recipient -resultsize unlimited
$casvwd=get-msoluser -all

$totalvwd=$recvwd|?{($_.RecipientType -eq "UserMailbox") -or ($_.RecipientTypedetails -eq "Mailuser")}
$totalvwd.count

compare-object $found.guid $totalvwd.guid

$recvwd.RecipientType|select -unique


$recinf=get-recipient -resultsize unlimited

$found=@()
$failed=@()
$upn=@()
$Mail=@()
foreach($entry in $totalcsv)
{
    $entry.targetupn
    try{
        $upn+=get-recipient $entry.targetupn -erroraction stop
    }
    Catch{
        try{
            $mail+=get-recipient $entry.targetemail -erroraction stop
        }
        catch
        {
            $failed+=$entry
        }
    }
}
$failed|ft
$upn.count
$mail.count

$totalinf=$recinf|?{($_.RecipientType -eq "UserMailbox") -or ($_.RecipientTypedetails -eq "Mailuser")}
$totalinf.count

compare-object $found. $totalinf

<#
Connect-ExchangeOnline
$recipients=@()
$failed=@()
foreach($user in $T2T)
{
    Try{
        $recipient=get-recipient $user.extensionAttribute1 -erroraction stop
        write-host "found"
        $recipients+=$user
    }
    Catch{
        write-host "not found"
        $failed+=$user
    }
}

(get-aduser -filter{(Userprincipalname -like "*@vwd.com") -and (extensionAttribute3 -eq "T2T_Project")}).count
(get-aduser -filter {extensionAttribute3 -eq "T2T_Project"}|select Userprincipalname).count
$notinscope=get-aduser -filter{Userprincipalname -like "*@vwd.com"} -properties *|?{$_.extensionAttribute3 -ne "T2T_project"}
($notinscope|?{$_.enabled -eq $false}).Userprincipalname
($notinscope|?{$_.enabled -eq $false}).count
($notinscope|?{($_.enabled -eq $false) -and ($_.Userprincipalname -match "-RES-")}).count
($notinscope|?{($_.enabled -eq $false) -and ($_.Userprincipalname -match "-adm@")}).count
($notinscope|?{($_.enabled -eq $false) -and ($_.Userprincipalname -match "inf_")}).count
($notinscope|?{($_.enabled -eq $false) -and ($_.Userprincipalname -notmatch "-RES-")})|ft displayname, Userprincipalname, Description,extensionAttribute1,extensionAttribute2,extensionAttribute3
$remaining=($notinscope|?{($_.enabled -eq $false) -and ($_.Userprincipalname -notmatch "-RES-")})

$recipients=@()
$failed=@()
foreach ($account in $remaining)
{
    #get-recipient $account.Userprincipalname|select name
    Try{
        $recipient=get-recipient $account.Userprincipalname -erroraction stop
        write-host "found"
        $recipients+=$account
        $newmail=$account.Userprincipalname -replace "@vwd.com","@infrontfinance.com"
        $newmail
        set-aduser $account.samaccountname -replace @{
            extensionAttribute1=$newmail
            extensionAttribute2="Disabled"
            extensionAttribute3="T2T_Project"
        }
        Add-ADGroupMember -Identity "vwd-LST-00317" -members $account.samaccountname
    }
    Catch{
        write-host "not found"
        $failed+=$account
    }
}
$recipients|select Userprincipalname,extensionAttribute1,extensionAttribute2,extensionAttribute3
$failed|select Userprincipalname,extensionAttribute1,extensionAttribute2,extensionAttribute3
$failed.count
$recipients|%{get-aduser -filter {Userprincipalname -eq $_.Userprincipalname} -properties *}|FT samaccountname,Userprincipalname,extensionAttribute1,extensionAttribute2,extensionAttribute3
$recipients|%{Add-ADGroupMember -Identity -members $_#>