﻿Clear-Host
Remove-Variable -Name * -ErrorAction SilentlyContinue
Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\PowerShell\1\ShellIds" -Name "ConsolePrompting" -Value $True

#Connect to Office 365
Write-Host "Connecting to O365..."
$msolcred = get-credential -Message "O365 Admin Account"
connect-msolservice -credential $msolcred
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session

Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\PowerShell\1\ShellIds" -Name "ConsolePrompting" -Value $False

Write-Host ""
Write-Host @"
This script will change the email address, UPN name and Mail Nickname
Old mail address will be remained in proxy addresses
SAM Account Name will not be changed
"@

#Request for Sam Account to change
Write-Host ""
$SAMAccount = Read-Host -Prompt "SAM Account to change"
Write-Host ""

#Validation
if ( -not [bool] (Get-ADUser -Filter { SamAccountName -eq $SAMAccount }) ) {
    Write-Host "SAM Account $SAMAccount does not exists or is not a user account"
    Break
}

$PDC = (Get-ADDomain).PDCEmulator
$ADUser = Get-ADUser -Identity $SAMAccount -Server $PDC -Properties *

Write-Host "Current settings for" $ADUser.SamAccountName
Write-Host "Display Name    :" $ADUser.DisplayName
Write-Host "Email Address   :" $ADUser.EmailAddress
Write-Host "Proxy Addresses :" $ADUser.proxyAddresses
Write-Host "SAM Account Name:" $ADUser.SamAccountName
Write-Host "Given Name      :" $ADUser.GivenName
Write-Host "Surname         :" $ADUser.Surname
Write-Host "UPN             :" $ADUser.UserPrincipalName
Write-Host "Mail Nickname   :" $ADUser.mailNickname
Write-Host ""

#Set current user
$currentmail=$ADUser.EmailAddress

#Request for new email address
$NewEmailAddress = Read-Host -Prompt $("New Email Address, enter to keep current (" + $ADUser.EmailAddress + ")")
If ($NewEmailAddress -eq '') { $NewEmailAddress = $ADUser.EmailAddress }
if ( -not [bool]($NewEmailAddress -as [Net.Mail.MailAddress])) {
    Write-Host "Email Address $NewEmailAddress does not exists or is not a email address"
Break
}


$NewProxyAddress = $NewEmailAddress
$NewEmail = $NewEmailAddress
$NewUPN = $NewEmailAddress
$NewMailNickname = $NewEmailAddress.Split("@")[0]

"New Email Address is $NewEmailAddress"
"New Proxy Address is $NewProxyAddress"
"New mail nickname is $NewMailNickname"
"New User Principal name is $NewUPN"

$ADUser.proxyAddresses 
foreach ($address in $ADUser.proxyAddresses) {
    if ($address.StartsWith('SMTP:')) { $PrimaryEmailAddress = $address }
    if ($address.StartsWith('sip:')) { $SipAddress = $address }
}
$NewProxyAddresses = $ADUser.proxyAddresses
$NewProxyAddresses.Remove($SipAddress)
$NewProxyAddresses.Add("sip:" + $NewEmail) | Out-Null
$NewProxyAddresses.Remove($PrimaryEmailAddress)
$NewProxyAddresses.Add($PrimaryEmailAddress.ToLower()) | Out-Null
$NewProxyAddresses.Add("SMTP:" + $NewEmail ) | Out-Null
$NewProxyAddresses.remove("smtp:" + $NewEmail ) 

#Check if email address already exists outside the given account
Write-Host "Checking email address (this may take several minutes)..."
if ( [bool] ($ExisitingUser = Get-recipient -ResultSize unlimited | where {$_.emailaddresses -like "*$NewEmail*" -and $_.PrimarySmtpAddress -ne "$currentmail"} | Select-Object DisplayName -first 1) ) {
    Write-Host ("New email address $NewEmail already exists, and belongs to " + $ExisitingUser.DisplayName)
    Break
}

Write-Host ""

Write-Host "New settings"
Write-Host "Display Name    :" $ADUser.DisplayName
Write-Host "Email Address   :" $NewEmail
Write-Host "Proxy Addresses :" $NewProxyAddresses
Write-Host "SAM Account Name:" $ADUser.SamAccountName
Write-Host "Given Name      :" $ADUser.GivenName
Write-Host "Surname         :" $ADUser.Surname
Write-Host "UPN             :" $NewUPN
Write-Host "Mail Nickname   :" $NewMailNickname
Write-Host

$Answer = Read-Host "Do you want to continue and submit the changes (Yes/No)"
While ($Answer -notin ("Yes","No")) {
    Write-Host "Please answer Yes or No."
    $Answer = Read-Host "Do you want to continue and submit the changes (Yes/No)"
}

If ($Answer -eq "No") { 
    Write-Host "Account has not been changed"
}
elseif ($Answer -eq "Yes") {
    Write-Host "Writing new settings for account"
    
    Get-ADUser -Identity $SAMAccount -Server $PDC -Properties * |`
    Set-ADUser -Server $PDC `
               -EmailAddress $NewEmail `
               -UserPrincipalName $NewUPN `
               -Replace @{MailNickName=$NewMailNickname} `
               -PassThru | `

#Set new proxyaddresses

    Set-ADUser -Server $PDC -clear ProxyAddresses
    Foreach($Proxyaddr in $NewProxyAddresses)
    {
        Set-ADUser -Identity $SAMAccount -Server $PDC -add @{ProxyAddresses=$Proxyaddr}
    }    
    
    
} else {
    Write-Host "Unknown error occured"
}

Start-ADSyncSyncCycle -policytype Delta


