﻿Remove-Variable * -ErrorAction SilentlyContinue

import-module ActiveDirectory
import-module ImportExcel

Get-ADUser -Filter {Enabled -eq $true} -server "deffm-dc1" -properties * -SearchBase "OU=users,OU=kl,OU=de,DC=vwdgroup,DC=net"|?{$_.displayname -notmatch "(admin)" -and $_.displayname -notmatch "(Test)"} |Select-Object Displayname,Mail,Company,physicalDeliveryOfficeName,Department,@{Name = "Job_Title";Expression={$_.Title}},OfficePhone,MobilePhone,@{Name = "Useraccount";Expression={$_.samaccountname}},@{Name = "Exchange_account";Expression={$_.UserPrincipalName}},StreetAddress,@{Name = "Country";Expression={$_.Co}},City,PostalCode,Manager|Export-Excel -FreezeTopRow -AutoFilter -AutoSize -Path "C:\AMS-ADMINS\Powershell\Output\user_export_outlook_kl.xlsx"


#?{$_.displayname -notmatch "(admin)" -and $_.displayname -notmatch "svc_" -and $_.DistinguishedName -notmatch "ou=disabled" -and $_.DistinguishedName -notmatch "ou=Migration" -and $_.mail -notmatch ".ext@" -and $_.mail -ne $null} | Select-Object Displayname,Mail,Company,Department,@{Name = "Job Title";Expression={$_.Title}},OfficePhone,MobilePhone,@{Name = "Useraccount";Expression={$_.samaccountname}},@{Name = "Exchange account";Expression={$_.UserPrincipalName}},AccountExpirationDate| Sort-Object displayname |Export-Excel -FreezeTopRow -AutoFilter -AutoSize -Path "C:\AMS-ADMINS\Powershell\Output\user_export_$date.xlsx"

<#$body=@"
Dear,

Attached the userlist of this month.

Kind Regards,
IT Support AMS
"@
#>
#Send-MailMessage -From "beheer-eu@vwd.com" -To "personal@vwd.com","jkoenig@vwd.com" -bcc "logging-eu@vwd.com" -Subject "User list HR" -Body $body -Attachments "C:\AMS-ADMINS\Powershell\Output\user_export_$date.xlsx" -SmtpServer "deffm-relay1.vwdgroup.net"
#Send-MailMessage -From "beheer-eu@vwd.com" -TO "dwagemakers@vwd.com" -Subject "User list outlook" -Attachments "C:\AMS-ADMINS\Powershell\Output\user_export_outlook_$department.xlsx" -SmtpServer "deffm-relay1.vwdgroup.net"