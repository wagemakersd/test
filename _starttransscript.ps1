$date=get-date -Format yyyyMMdd
$file=[environment]::GetFolderPath('MyDocuments')+"\Transcripts\$date.txt"
if(Test-path $file){
    Start-Transcript -Path $file -Append
}else{
    Start-Transcript -Path $file
}