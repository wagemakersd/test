Import-Module Microsoft.Graph

function Request-LibraryAccess {
    param (
        [Parameter(Mandatory = $true)]
        [string]$SiteUrl,
        
        [Parameter(Mandatory = $true)]
        [string]$LibraryName,
        
        [Parameter(Mandatory = $true)]
        [string]$ApplicationId,
        
        [Parameter(Mandatory = $true)]
        [string]$TenantId,
        
        [Parameter(Mandatory = $true)]
        [string]$Thumbprint,
        
        [Parameter(Mandatory = $false)]
        [string[]]$Permissions = @("read", "write")
    )
    
    try {
        # Connect to Microsoft Graph
        Write-Host "Connecting to Microsoft Graph..." -ForegroundColor Yellow
        
        
        # Connect using certificate
        Connect-MgGraph -ClientId $ApplicationId `
                       -TenantId $TenantId `
                       -CertificateThumbprint $Thumbprint 

        # Select beta profile for additional SharePoint endpoints
        Select-MgProfile -Name "beta"
        
        # Get site ID
        Write-Host "Getting site ID..." -ForegroundColor Yellow
        $site = Get-MgSite -Filter "webUrl eq '$SiteUrl'"
        
        if (-not $site) {
            throw "Could not find site with URL: $SiteUrl"
        }
        
        # Get drive (document library) ID
        Write-Host "Getting library ID..." -ForegroundColor Yellow
        $drives = Get-MgSiteDrive -SiteId $site.Id
        $drive = $drives | Where-Object { $_.Name -eq $LibraryName }
        
        if (-not $drive) {
            throw "Could not find library: $LibraryName"
        }
        
        # Prepare permission request body
        $permissionBody = @{
            roles = $Permissions
            grantedToIdentities = @(
                @{
                    application = @{
                        id = $ApplicationId
                        displayName = "Application Access Request"
                    }
                }
            )
        }
        
        # Convert to JSON
        $jsonBody = $permissionBody | ConvertTo-Json -Depth 10
        
        Write-Host "Requesting permissions..." -ForegroundColor Yellow
        
        # Request permissions using Invoke-MgGraphRequest
        $response = Invoke-MgGraphRequest `
            -Method POST `
            -Uri "https://graph.microsoft.com/v1.0/sites/$($site.Id)/drives/$($drive.Id)/permissions" `
            -Body $jsonBody `
            -ContentType "application/json"
        
        Write-Host "Successfully requested permissions!" -ForegroundColor Green
        Write-Host "Permission ID: $($response.id)" -ForegroundColor Green
        
        # Get current permissions to verify
        Write-Host "`nCurrent permissions:" -ForegroundColor Yellow
        $currentPermissions = Invoke-MgGraphRequest `
            -Method GET `
            -Uri "https://graph.microsoft.com/v1.0/sites/$($site.Id)/drives/$($drive.Id)/permissions"
        
        $currentPermissions.value | ForEach-Object {
            if ($_.grantedToIdentities.application.id -eq $ApplicationId) {
                Write-Host "- Roles: $($_.roles -join ', ')" -ForegroundColor Green
                Write-Host "- ID: $($_.id)" -ForegroundColor Green
            }
        }
        
        return $response
    }
    catch {
        Write-Host "Error requesting permissions: $_" -ForegroundColor Red
        throw $_
    }
    finally {
        # Disconnect from Graph
        Disconnect-MgGraph
    }
}

# Example usage:
$params = @{
    SiteUrl = "https://infrontonline.sharepoint.com/sites/ITSupport"
    LibraryName = "Documents"  # or your library name
    ApplicationId = "cc8469f3-2bdf-4d2a-b430-636ea63a1691"
    TenantId = "23371993-8e9a-4de5-97db-90d3e293e863"
    Thumbprint = "45d2e0b4238489cdb93295996483305202ba50f6"
    Permissions = @("read", "write")
}

# Execute the function
Request-LibraryAccess @params