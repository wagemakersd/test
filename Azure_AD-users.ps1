$Thumbprint="6CD3ABA73FACC81DCA2DE902670F4CFE04CAB474"
$Tenant="23371993-8e9a-4de5-97db-90d3e293e863"
$Org="infrontonline.onmicrosoft.com"
$App="cc8469f3-2bdf-4d2a-b430-636ea63a1691"
Connect-MicrosoftTeams -CertificateThumbprint $Thumbprint -ApplicationId $App -TenantId $Tenant
Connect-ExchangeOnline -CertificateThumbprint $Thumbprint -AppID $App -Organization $Org
Connect-MgGraph -CertificateThumbprint $Thumbprint -ClientId $App -TenantId $Tenant #-Scopes 'User.Read.All'

get-mguser -UserId dwagemakers-tst@vwd.com -Property Mail, Givenname, Surname, DisplayName, OfficeLocation, CompanyName, AccountEnabled, UserPrincipalNam,CreatedDateTime,LicenseAssignmentStates,UserType,ScopedRoleMemberOf,AssignedLicenses|fl *
$AADUsers=get-mguser -All -Property Mail, Givenname, Surname, DisplayName, OfficeLocation, CompanyName, AccountEnabled, UserPrincipalName, CreatedDateTime, UserType, LicenseAssignmentStates,JobTitle `
|select Mail, Givenname, Surname, DisplayName, OfficeLocation, CompanyName, AccountEnabled, UserPrincipalName, CreatedDateTime, UserType, LicenseAssignmentStates,JobTitle

$skulist=Get-MgSubscribedSku -All |select skuid, skupartnumber
$result=@()
Foreach ($user in $AADUsers){
    if($user.LicenseAssignmentStates){
        $skupartnumber=@()
        foreach($entry in $skulist){
            if ($entry.SkuId -in $user.LicenseAssignmentStates.skuid){
                $skupartnumber+=$entry.SkuPartNumber
            }
        }
        $result+=$user|select *,@{name = 'LicenseAssignmentStates'; Expression ={($skupartnumber) -join " | "}} -ExcludeProperty LicenseAssignmentStates
    }
    Else{
        $result+=$user|select *,@{name = 'LicenseAssignmentStates'; Expression ={"No License assigned"}} -ExcludeProperty LicenseAssignmentStates
    }

    # $result+=
}
$result|select DisplayName,UserPrincipalName, LicenseAssignmentStates

    $user |select @{name = 'LicenseAssignmentStates'; Expression ={$skupartnumber} } -ExcludeProperty LicenseAssignmentStates

$user.LicenseAssignmentStates|fl *

$result|export-csv -Path c:\temp\AAD_user.csv -Encoding UTF8 -NoTypeInformation

(get-mguser -UserId pmeijne@vwd.com -Property SignInSessionsValidFromDateTime)|select SignInSessionsValidFromDateTime



Get-MgSubscribedSku -All |select skuid, skupartnumber, ConsumedUnits