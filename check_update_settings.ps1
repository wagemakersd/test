if((get-item "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\" -ErrorAction SilentlyContinue).property -gt 2){
    write-host "Needs cleaning"
    get-item "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\"
    Exit 1
}
else{
    get-item "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\" -ErrorAction SilentlyContinue
    write-host "Correct"
    exit 0 
}

