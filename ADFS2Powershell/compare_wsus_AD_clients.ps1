﻿$wsuscomps=import-csv -Path C:\AMS-ADMINS\Powershell\Input\wsus_computers.csv -Delimiter "|"
$adcomps=get-adcomputer -filter {(OperatingSystem -notlike "*server*") -and (Enabled -eq "True")} -Properties lastlogondate
$Date=(get-date).AddDays(-30)|get-date -Format MM/dd/yyyy
#$adcomps |select lastlogondate |Sort-Object lastlogondate

#$adcomps.dnshostname
#$wsuscomps.FullDomainName
$compared=Compare-Object -ReferenceObject $adcomps.dnshostname -DifferenceObject $wsuscomps.FullDomainName

$noad=@()
foreach($comp in $compared)
{
    foreach($wsuscomp in $wsuscomps)
    {
        if ($comp.inputobject -eq $wsuscomp.FullDomainName)
        {
            $noad += $wsuscomp
        }
    }
}
#$noad.FullDomainName

$nowsus=@()
foreach($comp in $compared)
{
    foreach($adcomp in $adcomps)
    {
        if ($comp.inputobject -eq $adcomp.dnshostname)
        {
            $nowsus += $adcomp
        }
    }
}
#$nowsus.dnshostname
$clientsnowsus=$nowsus |?{$_.LastLogonDate -gt $Date -and $_.DistinguishedName -notmatch "OU=Servers" -and $_.dnshostname -ne "mirko-gude-macb.vwdgroup.net"} |select dnshostname, Lastlogondate, DistinguishedName |Sort-Object dnshostname

$clientsnowsus.dnshostname
$clientsnowsus |export-csv -Path C:\AMS-ADMINS\Powershell\Output\Clients_not_registered_in_wsus.csv -NoTypeInformation -Delimiter "|"