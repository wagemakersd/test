
Try {
    if(Test-Path "HKLM://SOFTWARE/ShareX"){
        if((Get-item "HKLM://SOFTWARE/ShareX").property -eq "PersonalPath"){
            if(Get-ItemProperty "HKLM://SOFTWARE/ShareX" |?{$_.PersonalPath -eq "%userprofile%\ShareX"}){
                write-host "PersonalPath found and set correctly"
                Exit 0
            }
            else {
                write-warning "PersonalPath found but not pointing to the correct location"
                Exit 1
            }
        }
        Else{
            write-warning "HKLM://SOFTWARE/ShareX found but PersonalPath notfound"
            Exit 1
        }
    }
    Else{
        write-warning "HKLM://SOFTWARE/ShareX not found"
        Exit 1
    }
} 
Catch {
    Write-Error $_.Exception
    Exit 1
}