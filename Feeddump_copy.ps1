[CmdletBinding()] 
Param 
(            
    [parameter(Mandatory=$true)] 
    [String]$JobName,
    [String]$Source,
    [String]$Destination,
    [Int]$DestRetention
) 
<#
        .SYNOPSIS
        Backup Feeddump files from source location to backup location.
        If an instance is split in multiple, it will copy from the folders and renam the files to use the split instancename.
        F.E. Performing the operation "Copy File" on target "Item: \\CL-FH-VWD4\d$\Feeddump\VWD_51_1\VWD_51.20200609.132823.0000.dat.gz Destination: \\cl-st-nas\vwd_dump\MDPSFeed\VWD_51_1.20200609.132823.0000.dat.gz
#>

$error.clear()

##########################################################################
# Variables
##########################################################################
#$JobName = 'CL-FH-VWD4_VWDFEED'
#$Source = '\\CL-FH-VWD4\d$\Feeddump\'
#$Destination = '\\cl-st-nas\vwd_dump\MDPSFeed\'

$date = Get-Date -Format yyyyMMdd
$Yesterday = [DateTime]::Today.AddDays(-1).ToString("yyyyMMdd")

### DO NOT CHANGE VARIABLES BELOW ###
$logLoction = "$env:Temp"
$logFile = "$JobName-$date-$time.log"
$log = $logLoction + '\' + $logFile

$smtpServer = "KG-MG-MS2"
$msgFrom = "CL-MG-DIV1 <no-reply@vwd.com>"
$msgTo = "Amsterdam - Logging [SMB] <logging-eu@vwd.com>"
#$msgTo = "Wagemakers, Dennis <DW@Infront.rocks>"
$msgToErr = "Amsterdam - Logging [SMB] <logging-eu@vwd.com>, Office Automation <beheer-eu@vwd.com>"
#$msgToErr = "Wagemakers, Dennis <DW@Infront.rocks>"
$msgSubject = "$JobName : File Backup finished"
$msgPriority = "Normal"
$msgBody = "Backup Job completed.`r`nFor results of Backup Job see attached log.`r`n`r`n"

##########################################################################
# Functions
##########################################################################
Function ErrorTrap
{
    $msgSubject = "$JobName : Error during file backup"
    $msgBody = "Errors occured during backup.`r`nSee attached log for details.`r`n`r`n"
    $msgPriority = "High"
    Send-MailMessage -From $msgFrom -To $msgToErr -Subject $msgSubject -Body $msgBody -Priority $msgPriority -SmtpServer $smtpServer -Attachments $log
    exit
}

##########################################################################
# Initialize log
##########################################################################
#Remove old logs for this job
if ((Get-ChildItem -Path "$logLoction\$JobName*.log").Count -gt 0) {
    Remove-Item -Path "$logLoction\$JobName*.log"
}

"Backup Job: $JobName" | Out-File -FilePath $log
"Server: $env:COMPUTERNAME`r`n" | Out-File -FilePath $log -Append
"Script: " + $MyInvocation.MyCommand.Path | Out-File -FilePath $log -Append
"Started: " + $(Get-Date -f 'yyyy-MM-dd HH:mm') + "`r`n" | Out-File -FilePath $log -Append
"Source: $Source" | Out-File -FilePath $log -Append
"Destination: $Destination" | Out-File -FilePath $log -Append

##########################################################################
# Checks
##########################################################################
#Source Check
if (-not (Test-Path $Source)) {
    "ERROR: Source Path ($Source) is not available"  | Out-File -FilePath $log -Append
    ErrorTrap
}

#Destination Check
if (-not (Test-Path $Destination)) {
    "ERROR: Destination Path ($Destination) is not available" | Out-File -FilePath $log -Append
    ErrorTrap
}

#Make sure Source path ends with \
$sources=@()
If ($Source -match ",")
{
    $source=$source.split(",")
    foreach ($src in $source)
    {
        if ($src.Substring($src.Length-1,1) -ne '\') {
            $src = $src + '\'
        }
        $sources += $src
    }
}
else 
{
    if ($Source.Substring($Source.Length-1,1) -ne '\') {
        $Source = $Source + '\'
        $sources += $Source
    }
}

#Make sure Destination path ends with \
if ($Destination.Substring($Destination.Length-1,1) -ne '\') {
    $Destination = $Destination + '\'
}

##########################################################################
# Main
##########################################################################
$handlers=@()
foreach($Source in $sources)
{
    $DIRS=get-childitem $source -Directory
    If ($DIRS)
    {
        Foreach($DIR in $DIRS)
        {
            #Get Feedhandler
            $handler=$DIR.name.Substring(0, $DIR.name.lastIndexOf('_'))
            $handlers+=$handler

            Get-ChildItem "$Source$($DIR.name)" -Filter "$handler.*.dat.gz"|%{
                If ($_.CreationTime -le ((get-date).adddays(-1)))
                {
                    Try
                    {
                        #Remove files older then 1day
                        #Write-Host -ForegroundColor red ($_.name -replace "$handler", "$($dir.name)")
                        "Removing file: $($_.FullName)"  | Out-File -FilePath $log -Append
                    # Remove-Item $_.FullName -Force -ErrorAction stop
                    }
                    Catch
                    {
                        ($error -split '\n')[0]  | Out-File -FilePath $log -Append
                        ErrorTrap 
                    }
                }
                ElseIf ($_.LastWriteTime -le ((get-date).AddHours(-3)))
                {
                    Try
                    {
                        #Copy files older then 4hours
                        #Rename the file to the handler instance
                        #"Copy file to: $("$Destination"+$_.name -replace "$handler", "$($dir.name)")"  | Out-File -FilePath $log -Append
                        #copy-item $_.fullname ("$Destination"+$_.name -replace "$handler", "$($dir.name)") -force -ErrorAction stop
                        $newname= $_.name -replace "$handler", "$($dir.name)"
                        "Renaming file: $($_.name) to $newname"  | Out-File -FilePath $log -Append
                        rename-item $_.fullname $newname -force -ErrorAction stop
                        $newfile = "$($_.Directory)"+"\"+"$newname"
                        If ($newname -match $date)
                        {
                            $zip="$($_.Directory)"+"-"+"$date.zip"
                            "Adding file to: $zip"  | Out-File -FilePath $log -Append
                            Compress-Archive -Path $newfile -DestinationPath $zip -CompressionLevel Optimal -Update -verbose |Out-File -FilePath $log -Append
                            #copy-item $zip "$Destination" -force -ErrorAction stop
                        }
                        Elseif ($newname -match $Yesterday)
                        {
                            $zip="$($_.Directory)"+"-"+"$Yesterday.zip"
                            "Adding file to: $zip"  | Out-File -FilePath $log -Append
                            Compress-Archive -Path $newfile -DestinationPath $zip -CompressionLevel Optimal -Update -verbose |Out-File -FilePath $log -Append
                        }
                    }
                    Catch
                    {
                        ($error -split '\n')[0]  | Out-File -FilePath $log -Append
                        ErrorTrap 
                    }             
                }
            }
        } 
    }
    else 
    {
        Get-ChildItem "$Source" -Filter "VWD_*.dat.gz" -Recurse |%{
            $handler=$_.name.split(".")[0]
            $handlers+=$handler
            If ($_.CreationTime -le ((get-date).adddays(-1)))
            {
                Try
                {
                    #Remove files older then 1day
                    "Removing file: $($_.FullName)"  | Out-File -FilePath $log -Append
                    Remove-Item $_.FullName -Force -ErrorAction stop
                }
                Catch
                {
                    ($error -split '\n')[0]  | Out-File -FilePath $log -Append
                    ErrorTrap 
                }
            }
            If ($_.LastWriteTime -le ((get-date).AddHours(-1)))
            {
                Try
                {
                    #Copy files older then 4hours
                    "Copy file: $($_.fullname)"  | Out-File -FilePath $log -Append
                    copy-item $_.fullname "$Destination" -force -ErrorAction stop
                    If ($_.name -match $date)
                    {
                        Compress-Archive -Path $newname -DestinationPath "$Destination\$handler"+"_"+"$date.zip" -CompressionLevel Optimal -Update -verbose |Out-File -FilePath $log -Append
                    }
                    Elseif ($_.name -match $Yesterday)
                    {
                        Compress-Archive -Path $newname -DestinationPath "$Destination\$handler"+"_"+"$Yesterday.zip" -CompressionLevel Optimal -Update -verbose |Out-File -FilePath $log -Append
                    }
                }
                Catch
                {
                    ($error -split '\n')[0]  | Out-File -FilePath $log -Append
                    ErrorTrap 
                }
            }
        }
    }
}
#########################################################################
# Remove old backups Destination
#########################################################################
foreach ($entry in $handlers)
{
    Get-ChildItem $Destination -Filter $handler+"*.zip" -File|%{
        if (($_.LastWriteTime -le ((get-date).adddays(-$DestRetention))))
        {
            Try
            {
                #Remove files older then 31days
                write-host "Removing file: $($_.FullName)" 
                "Removing file: $($_.FullName)"  | Out-File -FilePath $log -Append
                Remove-Item $_.FullName -Force -ErrorAction stop
            }
            Catch
            {
                ($error -split '\n')[0]
                ($error -split '\n')[0]  | Out-File -FilePath $log -Append
                ErrorTrap
            }
        }
    }
}
##########################################################################
# Send results
##########################################################################
Send-MailMessage -From $msgFrom -To $msgTo -Subject $msgSubject -Body $msgBody -Priority $msgPriority -SmtpServer $smtpServer -Attachments $log

##########################################################################
# Cleanup
##########################################################################
Remove-Item -Path $log







    