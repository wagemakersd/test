﻿$Date = (Get-Date).AddDays(-42)
$list=Get-ADComputer -filter {OperatingSystem -notLike '*SERVER*' } -Properties LastLogonDate| ?{$_.LastLogonDate -lt $Date -and $_.Enabled -eq $true}|sort-object LastLogonDate
foreach($comp in $list)
{
    $comp|Disable-ADAccount
}