# Edit ad-user for Mailuser
Import-Module activedirectory

$location = 'DE'
$DomainController = 'deffm-dc2'

$samAccountName = read-host "Username"
$vwdEmail = read-host 'vwd Email'
$extEmail = read-host 'Ext. Email'
$DisplayName = read-host 'Displayname'


Set-ADUser -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
Set-ADUser -Identity $samAccountName -Clear targetAddress -Server $DomainController -ErrorAction Stop
Set-ADUser -Identity $samAccountName -Add @{targetAddress = "$extEmail"} -Server $DomainController -ErrorAction Stop 
Set-ADUser -Identity $samAccountName -UserPrincipalName $vwdEmail -Server $DomainController -ErrorAction Stop
Set-ADUser -Identity $samAccountName -DisplayName $DisplayName -Server $DomainController -ErrorAction Stop
Set-ADUser $samAccountName -Replace @{MailNickName = "$samAccountName"} -Server $DomainController -ErrorAction Stop 
Set-ADUser $samAccountName -add @{MsExchUsageLocation = $location} -Server $DomainController -ErrorAction Stop