$Path = "HKLM:HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModelUnlock\"
$Name = "AllowAllTrustedApps"
$Type = "DWORD"
$Value = 1

If((get-item -Path $path).property -eq $name){
    $Registry = Get-ItemProperty -Path $Path -Name $Name -ErrorAction Stop | Select-Object -ExpandProperty $Name
    If ($Registry -eq $Value){
        Write-Output "Compliant: AllowAllTrustedApps -eq 1"
        Exit 0
    } 
    Else{
        Write-Warning "Not Compliant: AllowAllTrustedApps -ne 1"
        Exit 1
    }
}
Else{
    Write-output "Compliant: AllowAllTrustedApps does not exist"
    Exit 0
}