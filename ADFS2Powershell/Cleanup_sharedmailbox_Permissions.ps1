﻿#Needed to execute commands: Microsoft Online Services Sig-in and Windows Azure Active Directory Module
$msolcred = Get-Credential
connect-msolservice -credential $msolcred

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session

$array=@()
$obj=@()

$SMBS=Get-Mailbox -RecipientTypeDetails SharedMailbox
foreach($SMB in $SMBS)
{
    $Permissions=Get-MailboxPermission -Identity $SMB.Identity
    $ID=$SMB.Identity
    $Users=$Permissions.user

    $obj=New-Object -TypeName PSObject
    $obj | add-member -MemberType NoteProperty -name "Permissions" -Value $Users
    $obj | add-member -MemberType NoteProperty -name "Identity" -Value $ID
    
    IF($obj.Permissions -match "ZZ - ")
    {
        $array += $obj
    }
}

foreach ($entry in $array)
{
    $group=$entry.Permissions|?{$_ -match "ZZ - "}
    $group=Get-ADGroup -filter ('Displayname -eq $group')
    $users=$entry.Permissions|?{$_ -match "@vwd.com"}
    foreach($user in $users)
    {
        $Member = get-aduser -filter ('userprincipalname -eq $user')
        $grpmember=Get-ADGroupMember -Identity $group
        if($grpmember.name -notcontains $Member.name)
        {
            Add-ADGroupMember -Identity $group -Members $Member -ErrorAction SilentlyContinue
        }
        Remove-MailboxPermission -Identity $entry.identity -User $user -AccessRights FullAccess
    }
}
