$Thumbprint="45d2e0b4238489cdb93295996483305202ba50f6"
$Tenant="23371993-8e9a-4de5-97db-90d3e293e863"
$Org="infrontonline.onmicrosoft.com"
$App="cc8469f3-2bdf-4d2a-b430-636ea63a1691"
Connect-ExchangeOnline -CertificateThumbprint $Thumbprint -AppID $App -Organization $Org
Connect-MgGraph -CertificateThumbprint $Thumbprint -ClientId $App -TenantId $Tenant

$groups=get-adgroup -filter {Displayname -like 'ZZ -*'} -Properties Displayname,mail -Server deffm-dc3.vwdgroup.net
$groups2=get-adgroup -filter {extensionattribute11 -like 'infrontgroupsynced'} -Properties Displayname,mail -Server deffm-dc3.vwdgroup.net
$groups3=$groups2|?{$_.DisplayName -notin $groups.DisplayName}
$groups4=$groups2|?{$_.GroupCategory -ne "Distribution"}
$groups4=$groups4|?{$_.mail -ne $null}

$groups4.count
$groups3.count
$groups2.count
$groups.count

$group=$groups4|?{$_.name -match "NoReply-IPT"}

$output=@()
$tocheck=@()
foreach($group in $groups){
    $mail=($group.mail -replace "-sg","")
    $mailbox=get-mailbox -filter "EMailaddresses -like '*$Mail'"
    If($mailbox){
        write-host -ForegroundColor Green $group.mail
        $access=Get-MailboxPermission -Identity $mailbox -User $group.mail

        $sgname=$access.User -replace "[[\]]",""
        $sgdisplayname=$sgname.Trim()
        $sgprefix="$sgdisplayname"+"-sg" -replace " ",""
        $sgmail=$sgprefix+"@infrontonline.onmicrosoft.com"
        $sgdisplayname=$sgdisplayname+" - SG"

        $output+=New-Object -TypeName PsObject -Property @{
            MailboxName     = $mailbox.DisplayName
            MailboxMail     = $mailbox.PrimarySmtpAddress
            Group           = $group.DisplayName
            Groupmail       = $group.Mail
            PermissionUser  = $access.User
            PermissionRight = $access.Accessrights
            NewGroupMail    = $sgmail
            NewGroup        = $sgdisplayname
        }
    }
    Else{
       write-host -ForegroundColor Yellow $group.mail
        $tocheck+=$group
    }
}

$output|FT MailboxName,Group,Newgroup,NewGroupMail
$tocheck|FT Name,Mail


$failed=@()
$existing=@()
foreach($entry in $againoutput){
    $newmail=$entry.NewGroupMail
    IF(!(get-mggroup -Filter "Mail eq '$newmail'")){
        $entry.NewGroup
        $alias=$newmail.split("@")[0]
        New-DistributionGroup -Name $entry.NewGroup -Alias $alias -Type security -Description "For mailbox: $($entry.MailboxMail) | $($entry.MailboxName)"# -ManagedBy $entry.SOwnerUPN
        Set-DistributionGroup -Identity $entry.NewGroup -HiddenFromAddressListsEnabled $true
    
        $count=0
        While(!(Get-DistributionGroup $entry.NewGroup)){
            $count++
            Start-Sleep 10
            If ($count -eq 20)
            {
                write-host -foregroundcolor RED "Failed: $($entry.NewGroup)"
                Break
            }
        }
        IF($count -ne 20){

            $members= get-adgroup -filter "mail -eq '$($entry.groupmail)'"|get-adgroupmember|%{(get-aduser -Identity $_.SamAccountName|?{$_.userprincipalname -match "@vwd.com"}).userprincipalname}
            #Add members to group
            If($Members)
            {
                write-host "Adding members to $($entry.NewGroup)"
                foreach ($Member in $Members)
                {
                    Try
                    {
                        Add-DistributionGroupMember -Identity $entry.NewGroup -Member $Member -BypassSecurityGroupManagerCheck -ErrorAction stop
                        write-host -ForegroundColor Green "Added $Member to $($entry.NewGroup)"
                    }
                    Catch
                    {
                        write-host -ForegroundColor RED "Failed to add $Member to $($entry.NewGroup)"
                    }
                }
            }

            Add-MailboxPermission -Identity $entry.MailboxName -User $entry.newGroup -AccessRights FullAccess -InheritanceType All
    	    Set-Mailbox -Identity $entry.MailboxName -GrantSendOnBehalfTo $entry.newGroup
        }
        Else{
            $failed +=  $entry
        }
    }
    Else{
        $existing+=$entry
    }
}

$failed=@()
$existing=@()
foreach($entry in $output){
    $newmail=$entry.NewGroupMail
    IF(!(get-mggroup -Filter "Mail eq '$newmail'")){
        $failed+=$entry
    }
    else {
        $existing+=$entry
    }
}


New-DistributionGroup -Name "ZZ - KL - SMB artpma - SG" -Alias "ZZ-KL-SMBartpma-sg" -Type security -Description "For mailbox: dekl-artpma@vwd.com | Kaiserslautern - ARTPMA [SMB]"# -ManagedBy $entry.SOwnerUPN
Set-DistributionGroup -Identity "ZZ - KL - SMB artpma - SG" -HiddenFromAddressListsEnabled $true


$stillnotfound=@()
$newoutput=@()
foreach($Check in $tocheck){
    $samacc=$check.SamAccountName -replace "-sg",""
    $user=$null
    $user=get-aduser -filter ("Samaccountname -eq '$samacc'") -Properties Displayname,mail
    if($user){
        $newoutput+=New-Object -TypeName PsObject -Property @{
            MailboxName     = $user.DisplayName
            MailboxMail     = $user.mail
            Group           = $check.Name
            Groupmail       = $check.Mail
        }
    }
    else {
        $stillnotfound+=$check
    }
}

$newoutput|FT
$stillnotfound|FT Name,Mail

$againoutput=@()
$againnotfound=@()
foreach($entry in $newoutput){
    $mail=$entry.mailboxname
    $mailbox=get-mailbox -filter "Name -like '*$Mail'"
    If($mailbox){
        $mailbox.name
        $access=Get-MailboxPermission -Identity $mailbox -User $entry.Groupmail

        $sgname=$access.User -replace "[[\]]",""
        $sgdisplayname=$sgname.Trim()
        $sgprefix="$sgdisplayname"+"-sg" -replace " ",""
        $sgmail=$sgprefix+"@infrontonline.onmicrosoft.com"
        $sgdisplayname=$sgdisplayname+" - SG"

        $againoutput+=New-Object -TypeName PsObject -Property @{
            MailboxName     = $mailbox.DisplayName
            MailboxMail     = $mailbox.PrimarySmtpAddress
            Group           = $entry.Group
            Groupmail       = $entry.Groupmail
            PermissionUser  = $access.User
            PermissionRight = $access.Accessrights
            NewGroupMail    = $sgmail
            NewGroup        = $sgdisplayname
        }
    }
    else {
        $againnotfound+=$entry
    }
}

$againoutput|FT MailboxName,Group,NewGroup


####################################################
# Sanity check between cloud en on-prem membership #
####################################################
$failedgroups=@()
foreach ($group in $output){
    #$group.MailboxName
    $cloudgroup=Get-MgGroup -Filter "mail eq '$($group.NewGroupMail)'"
    $Onpremgroup=Get-MgGroup -Filter "mail eq '$($group.groupmail)'"
    if($cloudgroup -and $onpremgroup){
        
        $cloudmembers=(get-mggroupmember -GroupId $cloudgroup.Id).id
        $onpremmembers=(get-mggroupmember -GroupId $Onpremgroup.Id).id

        $Differences=$null
        if($onpremmembers -and $cloudmembers){
            $Differences=Compare-Object -ReferenceObject $onpremmembers -DifferenceObject $cloudmembers
        }

        If($Differences){
            write-host "`n Differences for $($Group.NewGroup)"
            $Differences
            foreach ($dif in $Differences){
                if($dif.SideIndicator -eq "<="){
                    write-host -ForegroundColor Cyan "Adding $($dif.InputObject) to $($Group.NewGroup)"
                    add-distributiongroupmember -Identity $cloudgroup.Id -Member $dif.InputObject -BypassSecurityGroupManagerCheck
                }
            }
        }
        
        #Get-ADGroup -Identity $Onpremgroup.OnPremisesSecurityIdentifier -Server deffm-dc3.vwdgroup.net -Properties * -Credential $cred|set-adgroup -clear extensionAttribute11
    }
    Else{
        $failedgroups+=$group
    }
}


#Clear infrontgroupsynced
foreach($group in $groups2){
    Get-ADGroup -Identity $group.ObjectGUID -Server deffm-dc3.vwdgroup.net -Properties * -Credential $cred|set-adgroup -clear extensionAttribute11
}


$failedgroups=@()
foreach ($group in $failedgroups){
    #$group.MailboxName
    $cloudgroup=Get-MgGroup -Filter "Startswith(Displayname,'$($group.Name)')"|?{$_.DisplayName -match "- SG"}
    $Onpremgroup=Get-MgGroup -Filter "Startswith(Displayname,'$($group.Name)')"|?{$_.DisplayName -notmatch "- SG"}

    if($cloudgroup -and $onpremgroup){
        
       <#
        $cloudmembers=(get-mggroupmember -GroupId $cloudgroup.Id).id
        $onpremmembers=(get-mggroupmember -GroupId $Onpremgroup.Id).id

        $Differences=$null
        if($onpremmembers -and $cloudmembers){
            $Differences=Compare-Object -ReferenceObject $onpremmembers -DifferenceObject $cloudmembers
        }

        If($Differences){
            write-host "`n Differences for $($cloudgroup.DisplayName)"
            $Differences
            foreach ($dif in $Differences){
                if($dif.SideIndicator -eq "<="){
                    write-host -ForegroundColor Cyan "Adding $($dif.InputObject) to $($cloudgroup.DisplayName)"
                    add-distributiongroupmember -Identity $cloudgroup.Id -Member $dif.InputObject -BypassSecurityGroupManagerCheck
                }
            }
        }#>
        
        Get-ADGroup -Identity $Onpremgroup.OnPremisesSecurityIdentifier -Server deffm-dc3.vwdgroup.net -Properties * -Credential $cred|set-adgroup -clear extensionAttribute11
    }
    Else{
       # $failedgroups+=$group
    }
}

$failedgroups.name




foreach ($Group in $groups2|?{$_.name -match "SWF - SMB newsletter-boerse ARD"}){}
     

        $sgname=$group.name -replace "[[\]]",""
        $sgdisplayname=$sgname.Trim()
        $sgprefix="$sgdisplayname"+"-sg" -replace " ",""
        $sgmail=$sgprefix+"@infrontonline.onmicrosoft.com"
        $sgdisplayname=$sgdisplayname+" - SG"


        New-DistributionGroup -Name $sgdisplayname -Alias $sgprefix -Type security -Description  "For mailbox: Schweinfurt - newsletter-boerse ARD [SMB] | newsletter-boerse.ARD.de@vwd.com"# -ManagedBy $entry.SOwnerUPN
        Set-DistributionGroup -Identity $sgdisplayname -HiddenFromAddressListsEnabled $true
}



$cloudgroup=Get-DistributionGroup 
$SG=$cloudgroup|?{$_.DisplayName -match "- SG"}
$failed=@()
foreach ($entry in $sg){
    $mailbox= $entry.description.split(" ") |?{$_ -match "@"}
    
    if ($mailbox){
        #get-mailboxpermission -identity $mailbox -user $entry.DisplayName
       # Add-RecipientPermission -Identity $mailbox -Trustee $entry.DisplayName -AccessRights SendAs -Confirm:$false
    }
    else{
        $failed+=New-Object -typename psobject -Property @{
            Mailbox = $mailbox
            Group   = $entry.DisplayName
        }
    }
}
$failed.Group