﻿    Add-Type -AssemblyName PresentationFramework
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null
$DomainController="DEFFM-DC1.vwdgroup.net"
##################################################################################################
#################################### Check Powershell Modules ####################################
##################################################################################################
IF (!(get-command connect-msolservice -erroraction silentlycontinue))
    {
    [System.Windows.MessageBox]::Show("O365 module is not installed","Connect MSOL error", "OK","Error")
    break
    } 
ElseIf (!(get-command get-aduser  -erroraction silentlycontinue))
    {
    [System.Windows.MessageBox]::Show("Active Directory module is not installed","Connect AD error", "OK","Error")
    break
    }
Else
    {
    ##################################################################################################
    ###################################### CONNECT TO OFFICE365 ######################################
    ##################################################################################################
    $Exception="1"
    while($Exception)
        {
        $msolcred=$null
        Try
            {
            $Exception=$null
            $msolcred = Get-Credential
            If(!($msolcred)){break}
            connect-msolservice -credential $msolcred -ErrorAction stop
            }
        Catch
            {
            $Exception=$_.Exception.Message
            [System.Windows.MessageBox]::Show("$Exception","MSOL credential error", "OK","Error")
            }
        If(!($msolcred)){break}
        }

    #If(!(Test-wsman DEFFM-DC1.vwdgroup.net -ErrorAction SilentlyContinue))
    If(!(Test-connection DEFFM-DC1.vwdgroup.net -Count 1 -ErrorAction SilentlyContinue))
        {
        [System.Windows.MessageBox]::Show("No connection to the domain controller","Error","OK","Error")
        }

    If(!($msolcred)){break}
    $Exception=$null
    $O365Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
    Import-PSSession $O365Session |out-null

    ##################################################################################################
    ######################################       FUNCTIONS      ######################################
    ##################################################################################################

    Function USRCheck-event ($FirstName, $LastName, $Location, $Office365, $USRCopy){
    ##################################################################################################
    ##################################### CHECK ALL DEPENDENCIES #####################################
    ##################################################################################################
    If ($FirstName -eq "" -or $LastName -eq "" -or $Location -eq "" -or $Office365 -eq "")
        {
        [System.Windows.MessageBox]::Show("Please enter all fields","Error","OK","Error")
        $SMBCreate.IsEnabled= $False
        $LSTCreate.IsEnabled= $False
        $USRCreate.IsEnabled= $False   
        $OoOCreate.IsEnabled= $False
        }
    Else
        {
    ##################################################################################################
    ####################################### SET ALL USERINFO ########################################
    ##################################################################################################
        $givenName=$FirstName
        $surName=$LastName

        $displayname = "$surName, $givenName"
        $surName2 = $surName -replace "\s", ""
        If($External.IsChecked)
            {
            $emailprefix = $givenName.Substring(0,1)+"$surName2"+".ext" 
            $emailprefix = $emailprefix.ToLower()
            }
        Else
            {
            $emailprefix = $givenName.Substring(0,1)+"$surName2"
            $emailprefix = $emailprefix.ToLower()
            }

        $samAccountName = $surName2+$givenName.Substring(0,1)
        $samAccountName = $samAccountName.tolower()
        $UserPrincipalName = "$emailprefix"+"@vwd.com"

        $activeuser= Get-aduser -filter {sAMAccountName -eq $samAccountName} -ErrorAction SilentlyContinue
        $activeprefix= Get-aduser -filter {UserPrincipalName -eq $UserPrincipalName} -ErrorAction SilentlyContinue
        If(!($Change.IsChecked))
            {
            while($activeuser -ne $null)
                {
                $samAccountName = [Microsoft.VisualBasic.Interaction]::InputBox("Enter a new username", "Username", "$samAccountName")
                if($samAccountName) 
                    {
                    $activeuser= Get-aduser -filter {sAMAccountName -eq $samAccountName} -ErrorAction SilentlyContinue
                    }
                Else
                    {
                    break
                    }
                }
            While ($activeprefix)
                {
                [System.Windows.MessageBox]::Show("The UPN $UserPrincipalName already exists.`nPlease enter a new mail address", "Warning", "OK", "Warning")
                $UserPrincipalName = [Microsoft.VisualBasic.Interaction]::InputBox("Enter a new UPN", "EmailPrefix", "$UserPrincipalName")
                If($UserPrincipalName)
                    {
                    If(!($UserPrincipalName -match "@vwd.com"))
                        {
                        $UserPrincipalName = [Microsoft.VisualBasic.Interaction]::InputBox("Enter a valid UPN (@vwd.com)", "EmailPrefix", "$UserPrincipalName")
                        If($UserPrincipalName)
                            {
                            $activeprefix= Get-aduser -filter {UserPrincipalName -eq $UserPrincipalName} -ErrorAction SilentlyContinue
                            }
                        Else
                            {
                            break
                            }
                        }
                    If($UserPrincipalName -match "@vwd.com")
                        {
                        $activeprefix= Get-aduser -filter {UserPrincipalName -eq $UserPrincipalName} -ErrorAction SilentlyContinue
                        }
                    Else
                        {
                        Break
                        }
                    }
                }
        
            If($activeuser -ne $null -or $activeprefix -ne $null)
                {
                $activeuser=$activeuser.SamAccountName
                [System.Windows.MessageBox]::Show("$activeuser or $UserPrincipalName does exist!", "Warning", "OK", "Warning")
                $USRCreate.IsEnabled= $False
                }
            Else
                {
                #Enable only user Create button
                $SMBCreate.IsEnabled= $False
                $LSTCreate.IsEnabled= $False
                $OoOCreate.IsEnabled= $False
                $USRCreate.content="Create user"
                $USRCreate.IsEnabled="True"
                }
            }
         Else
            {
            #Enable only user change button
            $SMBCreate.IsEnabled= $False
            $LSTCreate.IsEnabled= $False
            $OoOCreate.IsEnabled= $False
            $USRCreate.content="Change user"
            $USRCreate.IsEnabled="True"
            }

            If($Location -eq "Amsterdam")
                {
                $TargetOU="OU=Users,OU=am,OU=nl,DC=vwdgroup,DC=net"
                $Country="NL"
                }
            ElseIf($Location -eq "Antwerpen")
                {
                $TargetOU="OU=Users,OU=an,OU=be,DC=vwdgroup,DC=net"
                $Country="BE"
                }
            ElseIf($TargetOUation -eq "Berlin")
                {
                $TargetOU="OU=Users,OU=ber,OU=de,DC=vwdgroup,DC=net"
                $Country="DE"
                }
            ElseIf($Location -eq "Dortmund")
                {
                $TargetOU="OU=Users,OU=do,OU=de,DC=vwdgroup,DC=net"
                $Country="DE"
                }
            ElseIf($Location -eq "Frankfurt")
                {
                $TargetOU="OU=Users,OU=ffm,OU=de,DC=vwdgroup,DC=net"
                $Country="DE"
                }
            ElseIf($Location -eq "Frankfurt-TS")
                {
                $TargetOU="OU=Users,OU=ffm-ts,OU=de,DC=vwdgroup,DC=net"
                $Country="DE"
                }
            ElseIf($Location -eq "Frankfurt-EDG")
                {
                $TargetOU="OU=Users,OU=ffm-edg,OU=de,DC=vwdgroup,DC=net"
                $Country="DE"
                }
            ElseIf($Location -eq "Herzogenrath")
                {
                $TargetOU="OU=Users,OU=hzr,OU=de,DC=vwdgroup,DC=net"
                $Country="DE"
                }
            ElseIf($Location -eq "Kaiserslautern")
                {
                $TargetOU="OU=Users,OU=kl,OU=de,DC=vwdgroup,DC=net"
                $Country="DE"
                }
            ElseIf($Location -eq "Milan")
                {
                $TargetOU="OU=Users,OU=mi,OU=it,DC=vwdgroup,DC=net"
                $Country="IT"
                }
            ElseIf($Location -eq "Schweinfurt")
                {
                $TargetOU="OU=Users,OU=schw,OU=de,DC=vwdgroup,DC=net"
                $Country="DE"
                }
            ElseIf($Location -eq "Zurich")
                {
                $TargetOU="OU=Users,OU=zh-pcs,OU=ch,DC=vwdgroup,DC=net"
                $Country="CH"
                }
        
        If ($USRCopy)
            {
            If($USRCopy -match "@vwd")
                {
                $Copyfromusr = get-aduser -filter ('userprincipalname -eq $USRCopy') -ErrorAction SilentlyContinue
                $Copyfromusr = $Copyfromusr.SamAccountName
                }
            Else
                {
                $Copyfromusr=Get-ADUser -Identity $USRCopy -ErrorAction SilentlyContinue
                }
            }

        $USRres.text = "
         Givenname:`t`t`t$givenName
         Surname:`t`t`t$surName
         Displayname:`t`t$displayname
         sAMAccountName:`t`t$samAccountName
         Email:`t`t`t$UserPrincipalName
         Location:`t`t`t$location
         Target OU:`t`t`t$TargetOU
         Country:`t`t`t$Country
         License:`t`t`t$Office365
         Copy from user:`t`t$Copyfromusr "
     
                $Global:usrcustomobj = New-Object PSObject
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name “givenName” -Value $($givenName)
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name “surName” -Value $($surName)
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name “displayname” -Value $($displayname)
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name “samAccountName” -Value $($samAccountName)
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name "UserPrincipalName" -Value $($UserPrincipalName) 
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name “location” -Value $($location)
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name “TargetOU” -Value $($TargetOU)
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name “Country” -Value $($Country)
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name “Office365” -Value $($Office365)
                $Global:usrcustomobj | Add-Member -MemberType NoteProperty -Name “UserCopy” -Value $($Copyfromusr)
    
        }
    }
    Function USRCreate-event ($usrcustomobj){

            $givenName=$usrcustomobj.givenName
            $surName=$usrcustomobj.surName
            $displayname=$usrcustomobj.displayname
            $samAccountName=$usrcustomobj.samAccountName
            $UserPrincipalName=$usrcustomobj.UserPrincipalName
            $location=$usrcustomobj.location
            $TargetOU=$usrcustomobj.TargetOU
            $Country=$usrcustomobj.Country
            $Office365=$usrcustomobj.Office365
            $CopyFromuser=$usrcustomobj.UserCopy

    ##################################################################################################
    ######################################## Create AD Account #######################################
    ##################################################################################################



    If (!($Change.IsChecked))
        {
            $USRres.text += "`n`n    Wait: Creating AD account `n"
            new-aduser -name $samAccountName -givenName $givenName -surName $surName -displayname $displayname -samaccountname $samAccountName -UserPrincipalName $UserPrincipalName -server $DomainController -Path $targetOU -ErrorAction Stop
            Set-aduser -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$UserPrincipalName"} -Server $DomainController -ErrorAction Stop
        }
        Elseif ($Change.IsChecked)
        {
            $USRres.text += "`n    Wait: Changing AD account `n"
            Set-aduser -Identity $samAccountName -Replace @{UserPrincipalName = "$UserPrincipalName"} -Server $DomainController -ErrorAction Stop
            Set-aduser -Identity $samAccountName -Replace @{displayName = "$displayname"} -Server $DomainController -ErrorAction Stop
            Set-aduser -Identity $samAccountName -Replace @{ProxyAddresses = "SMTP:$UserPrincipalName"} -Server $DomainController -ErrorAction Stop
        }
    Set-aduser -Identity $samAccountName -Add @{ProxyAddresses = "sip:$UserPrincipalName"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Replace @{MailNickName = "$UserPrincipalName"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Replace @{Mail = "$UserPrincipalName"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -add @{msExchUsageLocation = "$Country"} -Server $DomainController -ErrorAction Stop

    ##################################################################################################
    ####################################### Create O63 Account #######################################
    ##################################################################################################
    
    while(!(Get-aduser -Identity $samAccountName))
        {
        $USRres.text += "`n    Wait: AD Account is not yet created"
        start-sleep -seconds 30
        }

    If ($CopyFromuser)
        {
        $USRres.text += "`n    Wait: Copying group membership"
        $from=(Get-ADUser -Identity $CopyFromuser -Properties MemberOf).MemberOf
        Foreach($fr in $from)
            {
            Add-ADGroupMember -Identity $fr -Members $samAccountName
            }
        }

    $USRres.text += "`n    Success: AD Account has been created"
    $USRres.text += "`n    Wait: Synchronizing account to O365"
    $sync=Start-ADSyncSyncCycle
    
    $timeout = new-timespan -Minutes 5
    $sw = [diagnostics.stopwatch]::StartNew()
    $msolUser=$null
    While($sw.elapsed -lt $timeout)
        {
        start-sleep -seconds 30
        $msolUser = Get-MsolUser -UserPrincipalName $UserPrincipalName -ErrorAction SilentlyContinue
        If(!($msolUser))
            {
            $USRres.text += "`n  Wait: Still synchronizing account to O365 (Max 5 minutes)"
            }
        Else
            {
            If ($Office365 -eq "Full")
                {
                #Full License
                Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -AddLicenses vwd365:ENTERPRISEPACK -ErrorAction SilentlyContinue
                $serviceplan = New-MsolLicenseOptions -AccountSkuId vwd365:ENTERPRISEPACK -DisabledPlans FORMS_PLAN_E3,STREAM_O365_E3,Deskless,FLOW_O365_P2,POWERAPPS_O365_P2,PROJECTWORKMANAGEMENT,SWAY,INTUNE_O365,YAMMER_ENTERPRISE,RMS_S_ENTERPRISE,SHAREPOINTWAC,SHAREPOINTENTERPRISE
                Set-MsolUserLicense -UserPrincipalName $UserPrincipalName –LicenseOptions $serviceplan
                }
            Elseif  ($Office365 -eq "Exchange + Skype")
                {
                #EXCHANGE AND SKYPE License
                Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -AddLicenses vwd365:EXCHANGEENTERPRISE  -ErrorAction SilentlyContinue
                Set-MsolUserLicense -UserPrincipalName $UserPrincipalName -AddLicenses vwd365:MCOSTANDARD  -ErrorAction SilentlyContinue
                } 
            Break
            }
        }
    If($msolUser)
        {
        $USRres.text += "`n  Success: O365 Account has been created"
        }
    Else
        {
        $USRres.text += "`n  Warning: Office365 Account has not been synchronizerd"
        }
    }

    Function SMBCheck-event ($SName, $SMail, $SLocation, $SMember){
    ##################################################################################################
    ##################################### CHECK ALL DEPENDENCIES #####################################
    ##################################################################################################

     If ($SName -eq "" -or $SMail -eq "" -or $SLocation -eq "")
        {
        [System.Windows.MessageBox]::Show("Please enter name, mail and location fields","Error","OK","Error")
        $SMBCreate.IsEnabled= $False
        $LSTCreate.IsEnabled= $False
        $USRCreate.IsEnabled= $False   
        $OoOCreate.IsEnabled= $False
        }
     Elseif(Get-aduser -filter {displayname -eq $sname} -ErrorAction SilentlyContinue)
        {
        [System.Windows.MessageBox]::Show("DisplayName already exists","Error","OK","Error")
        $SMBCreate.IsEnabled= $False
        $LSTCreate.IsEnabled= $False
        $USRCreate.IsEnabled= $False   
        $OoOCreate.IsEnabled= $False
        }
     Elseif(Get-aduser -filter {UserPrincipalName -eq $SMail} -ErrorAction SilentlyContinue)
        {
        [System.Windows.MessageBox]::Show("Mail address already exists","Error","OK","Error")
        $SMBCreate.IsEnabled= $False
        $LSTCreate.IsEnabled= $False
        $USRCreate.IsEnabled= $False   
        $OoOCreate.IsEnabled= $False
        }
     Else
        {

    ##################################################################################################
    #########################################  SET SMB INFO  #########################################
    ##################################################################################################

        $SOU="OU=Shared Mailboxes,OU=Migration,DC=vwdgroup,DC=net"
        $counter = @()
        $list =  Get-ADUser -Filter * -SearchBase $SOU | select-object samaccountname
        foreach($entry in $list){ $counter += $entry.samaccountname.substring($entry.samAccountName.Length - 5) }
        $last_nr = $counter | Sort-Object |Select-Object -last 1
        $newnr=$last_nr/1 + 1

        If($SLocation -eq "Amsterdam")
            {
            $SLoc="AMS"
            $SCountry="NL"
            }
        ElseIf($SLocation -eq "Antwerpen")
            {
            $SLoc="ANT"
            $SCountry="BE"
            }
        ElseIf($SLocation -eq "Berlin")
            {
            $SLoc="BER"
            $SCountry="DE"
            }
        ElseIf($SLocation -eq "Dortmund")
            {
            $SLoc="DOR"
            $SCountry="DE"
            }
        ElseIf($SLocation -eq "Frankfurt")
            {
            $SLoc="FFM"
            $SCountry="DE"
            }
        ElseIf($SLocation -eq "Herzogenrath")
            {
            $SLoc="HER"
            $SCountry="DE"
            }
        ElseIf($SLocation -eq "Kaiserslautern")
            {
            $SLoc="KL"
            $SCountry="DE"
            }
        ElseIf($SLocation -eq "Milan")
            {
            $SLoc="MIL"
            $SCountry="IT"
            }
        ElseIf($SLocation -eq "Schweinfurt")
            {
            $SLoc="SWF"
            $SCountry="DE"
            }
        ElseIf($SLocation -eq "vwd")
            {
            $SLoc="vwd"
            $SCountry="DE"
            }
        ElseIf($SLocation -eq "Zurich")
            {
            $SLoc="ZUR"
            $SCountry="CH"
            }

        $SMBsamAccountName= "$SLoc"+"-SMB-"+"$newnr"
    
        #Enable only SMB Create button
        $USRCreate.IsEnabled= $False
        $LSTCreate.IsEnabled= $False
        $OoOCreate.IsEnabled= $False
        $SMBCreate.IsEnabled="True"

        $SMBRes.text = "
        Last SMB nr:`t`t`t$last_nr
    
        DisplayName:`t`t$SName
        Email:`t`t`t$SMail
        sAMAccountName:`t`t$SMBsamAccountName
        Location:`t`t`t$SLocation
        Country:`t`t`t$SCountry
        Member(s):`t`t`t$SMember "

        $Global:smbcustomobj = New-Object PSObject
            $Global:smbcustomobj | Add-Member -MemberType NoteProperty -Name “Displayname” -Value $($SName)
            $Global:smbcustomobj | Add-Member -MemberType NoteProperty -Name “SamAccountName” -Value $($samAccountName)
            $Global:smbcustomobj | Add-Member -MemberType NoteProperty -Name "UserPrincipalName" -Value $($SMail) 
            $Global:smbcustomobj | Add-Member -MemberType NoteProperty -Name “Location” -Value $($SLocation)
            $Global:smbcustomobj | Add-Member -MemberType NoteProperty -Name “Country” -Value $($SCountry)
            $Global:smbcustomobj | Add-Member -MemberType NoteProperty -Name “Members” -Value $($SMember)
            $Global:smbcustomobj | Add-Member -MemberType NoteProperty -Name “Sloc” -Value $($SLoc)
        }
    }
    Function SMBCreate-event ($smbcustomobj){

    $givenName = ''
    $surName = ''
    $displayname = $smbcustomobj.Displayname
    $vwdEmail = $smbcustomobj.UserPrincipalName
    $emailprefix = $vwdEmail -replace "@vwd.com"
    $samAccountName = $smbcustomobj.SamAccountName
    $targetOU = "OU=Shared Mailboxes,OU=Migration,DC=vwdgroup,DC=net"
    $location = $smbcustomobj.Location
    $sloc=$smbcustomobj.Sloc
    $Members=$smbcustomobj.Members

    new-aduser -name $samAccountName -givenName $givenName -surName $surName -displayname $displayname -samaccountname $samAccountName -UserPrincipalName $vwdEmail -server $DomainController -Path $targetOU -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Replace @{MailNickName = "$emailprefix"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -Replace @{Mail = "$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-aduser -Identity $samAccountName -add @{MsExchUsageLocation = $location} -Server $DomainController -ErrorAction Stop

    $sgdisplayname="ZZ - "+"$sloc"+"- SMB" 
    $sgmail="$samAccountName"+"-sg@vwd.com"
    $sgou="OU=Groups,OU=Migration,DC=vwdgroup,DC=net"
    New-ADGroup -Name $sgdisplayname -SamAccountName $samAccountName -Path $sgou -GroupScope Universal -GroupCategory Security
    Get-ADGroup -Name $sgdisplayname | Set-ADGroup -Add @{Mail="$sgmail"}

    while(!(Get-aduser -Identity $samAccountName))
        {
        $USRres.text += "`n    Wait: AD Account is not yet created"
        start-sleep -seconds 30
        }
    while(!(Get-ADGroup -Identity $sgdisplayname))
        {
        $USRres.text += "`n    Wait: AD Group is not yet created"
        start-sleep -seconds 30
        }

    If($Members)
        {
        $SMBRes.text += "    Wait: Adding members to SMB group"
        $Membersarr = $Members -split ","
        foreach ($Member in $Membersarr)
            {
            If($Member -match "@vwd")
                {
                Try
                    {
                    $Member = get-aduser -filter ('userprincipalname -eq $Member')
                    Add-ADGroupMember -Identity $vwdEmail -Members $Member
                    $Member = $Member.SamAccountName
                    $SMBRes.text += "    Success: User $Member added"
                    }
                Catch
                    {
                    $SMBRes.text += "    Fail: Cannot find user $Member.SamAccountName"
                    }
                }
            Else
                {
                Try
                    {
                    Add-ADGroupMember -Identity $vwdEmail -Members $Member
                    $SMBRes.text += "    Success: User $Member added"
                    }
                Catch
                    {
                    $SMBRes.text += "    Fail: Cannot find user $Member"
                    }
                }
            }
        }

    $sync=Start-ADSyncSyncCycle

    $timeout = new-timespan -Minutes 5
    $sw = [diagnostics.stopwatch]::StartNew()
    $msolUser=$null
    While($sw.elapsed -lt $timeout)
        {
        start-sleep -seconds 30
        $msolUser = Get-MsolUser -UserPrincipalName $vwdEmail -ErrorAction SilentlyContinue
        If(!($msolUser))
            {
            $USRres.text += "`n    Wait: Still synchronizing account to O365 (Max 5 minutes)"
            }
        Else
            {
             $USRres.text += "`n    Wait: Setting License and creating Mailbox"
             Set-MsolUserLicense -UserPrincipalName $vwdEmail -AddLicenses vwd365:EXCHANGEENTERPRISE  -ErrorAction SilentlyContinue
             Break
            } 
        }



    $timeout2 = new-timespan -Minutes 5
    $sw2 = [diagnostics.stopwatch]::StartNew()
    $enabledmailbox=$null
    While($sw2.elapsed -lt $timeout2)
        {
        start-sleep -seconds 30
        $enabledmailbox = get-recipient -identity $vwdEmail -ErrorAction SilentlyContinue
        If(!($enabledmailbox))
            {
            $USRres.text += "`n    Wait: Mailbox is not yet created."
            }
        Else
            {
            Set-Mailbox $vwdEmail -Type Shared
            Break
            } 
        }

    $USRres.text += "`n    Wait: Adding Mailbox permissions."
    $permission=add-mailboxpermission -identity $vwdEmail -user $sgmail -accessright Fullaccess

    $License = (Get-MsolUser -UserPrincipalName $vwdEmail).Licenses.AccountSkuId
    foreach($lic in $license)
        {
        Set-MsolUserLicense -UserPrincipalName $vwdEmail -RemoveLicenses $Lic
        }

    If($Enabledmailbox=get-recipient -identity $vwdEmail -ErrorAction SilentlyContinue)
        {
        $USRres.text += "`n    Success: Shared Mailbox Created."
        }
    Else
        {
        $USRres.text += "`n    Failed: Shared Mailbox not created coorectly. `n    Please Check."
        }
    }

    Function LSTCheck-event ($LName, $LMail,$LLocation, $LMember){
    ##################################################################################################
    ##################################### CHECK ALL DEPENDENCIES #####################################
    ##################################################################################################

    If ($LName -eq "" -or $LMail -eq "" -or $LLocation -eq "")
        {
        [System.Windows.MessageBox]::Show("Please enter name, mail and location fields","Error","OK","Error")
        $SMBCreate.IsEnabled= $False
        $LSTCreate.IsEnabled= $False
        $USRCreate.IsEnabled= $False   
        $OoOCreate.IsEnabled= $False
        }
     Else
        {

    ##################################################################################################
    #########################################  SET List INFO  #########################################
    ##################################################################################################

        $LOU="OU=DL,OU=Groups,OU=Migration,DC=vwdgroup,DC=net"
        $LSTcounter = @()
        $LSTlist =  Get-ADgroup -Filter * -SearchBase $LOU |?{$_.samaccountname -match "-LIST-0"}| select-object samaccountname
        foreach($LSTentry in $LSTlist){ $LSTcounter += $LSTentry.samaccountname.substring($LSTentry.samAccountName.Length - 5) }
        $LSTlast_nr = $LSTcounter | Sort-Object |Select-Object -last 1
        $LSTnewnr=$LSTlast_nr/1 + 1

        If($LLocation -eq "Amsterdam")
            {
            $LLoc="AMS"
            $LCountry="NL"
            }
        ElseIf($LLocation -eq "Antwerpen")
            {
            $LLoc="ANT"
            $LCountry="BE"
            }
        ElseIf($LLocation -eq "Berlin")
            {
            $LLoc="BER"
            $LCountry="DE"
            }
        ElseIf($LLocation -eq "Dortmund")
            {
            $LLoc="DOR"
            $LCountry="DE"
            }
        ElseIf($LLocation -eq "Frankfurt")
            {
            $LLoc="FFM"
            $LCountry="DE"
            }
        ElseIf($LLocation -eq "Herzogenrath")
            {
            $LLoc="HER"
            $LCountry="DE"
            }
        ElseIf($LLocation -eq "Kaiserslautern")
            {
            $LLoc="KL"
            $LCountry="DE"
            }
        ElseIf($LLocation -eq "Milan")
            {
            $LLoc="MIL"
            $LCountry="IT"
            }
        ElseIf($LLocation -eq "Schweinfurt")
            {
            $LLoc="SWF"
            $LCountry="DE"
            }
        ElseIf($LLocation -eq "vwd")
            {
            $LLoc="vwd"
            $LCountry="DE"
            }
        ElseIf($LLocation -eq "Zurich")
            {
            $LLoc="ZUR"
            $LCountry="CH"
            }
        $LSTsamAccountName= "$LLoc"+"-LST-"+"$LSTnewnr"
    
        $SMBCreate.IsEnabled= $False
        $USRCreate.IsEnabled= $False
        $OoOCreate.IsEnabled= $False            
        $LSTCreate.IsEnabled="True"

        $LSTRes.text = "
        Last LST nr:`t`t`t$LSTlast_nr
    
        DisplayName:`t`t$LName
        Email:`t`t`t$LMail
        sAMAccountName:`t`t$LSTsamAccountName
        Location:`t`t`t$LLocation
        Country:`t`t`t$LCountry
        Member(s):`t`t`t$LMember "
    
        $Global:lstcustomobj = New-Object PSObject
            $Global:lstcustomobj | Add-Member -MemberType NoteProperty -Name “Displayname” -Value $($LName)
            $Global:lstcustomobj | Add-Member -MemberType NoteProperty -Name “SamAccountName” -Value $($LSTsamAccountName)
            $Global:lstcustomobj | Add-Member -MemberType NoteProperty -Name "UserPrincipalName" -Value $($LMail) 
            $Global:lstcustomobj | Add-Member -MemberType NoteProperty -Name “Location” -Value $($LLocation)
            $Global:lstcustomobj | Add-Member -MemberType NoteProperty -Name “Country” -Value $($LCountry)
            $Global:lstcustomobj | Add-Member -MemberType NoteProperty -Name “Members” -Value $($LMember)
            $Global:lstcustomobj | Add-Member -MemberType NoteProperty -Name “Sloc” -Value $($LLoc)
    
        }
    }
    Function LSTCreate-event ($lstcustomobj){

    $displayname = $lstcustomobj.DisplayName
    $vwdEmail = $lstcustomobj.UserPrincipalName
    $emailprefix = $vwdemail -replace "@vwd.com"
    $samAccountName = $lstcustomobj.samAccountName
    $targetOU = 'OU=DL,OU=Groups,OU=Migration,DC=vwdgroup,DC=net'
    $location = $lstcustomobj.Country
    $Members = $lstcustomobj.Members

    $LSTRes.text += "    Creating DistributionList"
    new-adgroup -name $displayname -displayname $displayname -samaccountname $samAccountName -GroupCategory Distribution -GroupScope Universal -server $DomainController -Path $targetOU -ErrorAction Stop
    Set-adgroup -Identity $samAccountName -Add @{ProxyAddresses = "SMTP:$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-adgroup -Identity $samAccountName -Replace @{MailNickName = "$emailprefix"} -Server $DomainController -ErrorAction Stop
    Set-adgroup -Identity $samAccountName -Replace @{Mail = "$vwdEmail"} -Server $DomainController -ErrorAction Stop
    Set-adgroup -Identity $samAccountName -add @{MsExchUsageLocation = $location} -Server $DomainController -ErrorAction Stop
    sleep -Seconds 10

    If($Members)
        {
        $LSTRes.text += "    Wait: Adding members to distribution list"
        $Membersarr = $Members -split ","
        foreach ($Member in $Membersarr)
            {
            If($Member -match "@vwd")
                {
                Try
                    {
                    $Member = get-aduser -filter ('userprincipalname -eq $Member')
                    Add-ADGroupMember -Identity $vwdEmail -Members $Member
                    $Member = $Member.SamAccountName
                    $SMBRes.text += "    Success: User $Member added"
                    }
                Catch
                    {
                    $SMBRes.text += "    Fail: Cannot find user $Member.SamAccountName"
                    }
                }
            Else
                {
                Try
                    {
                    Add-ADGroupMember -Identity $vwdEmail -Members $Member
                    $SMBRes.text += "    Success: User $Member added"
                    }
                Catch
                    {
                    $SMBRes.text += "    Fail: Cannot find user $Member"
                    }
                }
            }
        }

    $adgroup=Get-ADGroup -Name $displayname
    $sync=start-adsyncsynccycle
    If ($adgroup)
        {
        $LSTRes.text += "    Success: DistributionList $displayname created."
        }
    Else
        {
        $LSTRes.text += "    Failed: DistributionList has not been created correctly."
        }
    }
    Function OoOCheck-event ($Global:OMail, $Global:OMSG, $Global:OFWD){
    ##################################################################################################
    ##################################  CHECK IF MAILBOX EXISTS  #####################################
    ##################################################################################################
     If (!($checkmail=Get-Mailbox -Identity $OMail -erroraction SilentlyContinue))
        {
        [System.Windows.MessageBox]::Show("Please enter a vwd.com email address.","Error","OK","Error")
        $SMBCreate.IsEnabled= $False
        $LSTCreate.IsEnabled= $False
        $USRCreate.IsEnabled= $False   
        $OoOCreate.IsEnabled= $False
        }
     Else
        {
    ##################################################################################################
    #####################################  SET FWD/OoO INFO  ###################################
    ##################################################################################################

        $OldForwarding=Get-Mailbox -Identity $OMail | select ForwardingAddress
        $OldOoO= get-MailboxAutoReplyConfiguration -Identity $OMail |select AutoReplyState, ExternalMessage
        $OldOMSG=$OldOoO.ExternalMessage
        $Oldstate=$OldOoO.AutoReplyState
        $OldForwarding=$OldForwarding.ForwardingAddress
        If($OldForwarding -ne $null -and $OFWd -eq $null)
            {
            $OFWD=Write-Warning "Forwarding will be removed"
            }
        $SMBCreate.IsEnabled= $False
        $LSTCreate.IsEnabled= $False
        $USRCreate.IsEnabled= $False   
        $OoOCreate.IsEnabled="True"

        $OoOres.text = "
        Email:`t`t`t`t$OMail
        Current Forwarding:`t`t$OldForwarding
        Forwarding:`t`t`t$OFWD
        Current Out of Office state:`t$Oldstate
        Current Out of Office message:`t$OldOMSG`n
        New Out of Office:`t`t$OMSG "
        }
    }
    Function OoOCreate-event ($Global:OMail, $Global:OMSG, $Global:OFWD){
    If($Global:OFWD)
        {
        Set-Mailbox $Global:OMail -ForwardingAddress $Global:OFWD
        $OoOres.text += "    Forwarding enabled"
        }
    Else
        {
        Set-Mailbox $Global:OMail -ForwardingAddress $null
        $OoOres.text += "    Forwarding disabled"
        }

    If($Global:OMSG)
        {
        Set-MailboxAutoReplyConfiguration -Identity $Global:OMail -AutoReplyState Enabled -InternalMessage $Global:OMSG -ExternalMessage $Global:OMSG
        $OoOres.text += "    Out of Office reply enabled"
        }    
    Else
        {
        Set-MailboxAutoReplyConfiguration -Identity $Global:OMail -AutoReplyState Disabled
        $OoOres.text += "    Out of Office reply disabled."
        }
    }

    Function SFCheck-event ($SearchMail, $SearchFolder){
    ##################################################################################################
    ####################################  CHECK MISSING FOLDER.SIZE  #################################
    ##################################################################################################
     $mailbox= Get-MailboxStatistics -Identity dwagemakers@vwd.com
     $mailboxdisplay=$mailbox.DisplayName
     $mailboxitemsize=$mailbox.TotalItemSize.Value
     $SFRes.text+="
        $mailboxdisplay
        $mailboxitemsize
        "
    If($SearchFolder)
        {
        $output=$null
        $folderinfo=$null
        $output=Get-Mailbox -Identity $SearchMail | Select-Object alias | foreach-object {Get-MailboxFolderStatistics -Identity $_.alias | select-object Identity, FolderSize|?{$_.identity -match $SearchFolder}}
        $Folderident=@()
        $FolderSize=@()
        $folderinfo=@()
        Foreach($ent in $output)
            {
            $Folderident = $ent.identity    
            $FolderSize = $ent.FolderSize 
            $folderinfo += "$Folderident `r $Foldersize`r`r"
            }

        $SFRes.text+="
        $folderinfo
        "
        }
    }


    ##################################################################################################
    ############################################# GUI ################################################
    ##################################################################################################
    [xml]$Form  = @"
    <Window xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
      xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
      Title="Office 365" Height="480"  Width="640"  ResizeMode="NoResize" WindowStartupLocation = "CenterScreen">
        <TabControl HorizontalAlignment="Left" Height="445" VerticalAlignment="Top" Width="630"  BorderBrush="LightBlue">
            <TabItem Name="New_User" Header="New User">
                <Grid Name="usergrid" Background="LightBlue" ShowGridLines="True" Margin="0,0,0,0">
                    <Label Name="USRFirstName" Content="First name:" HorizontalAlignment="Left" Height="27" Margin="23,12,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0" Grid.Row="0"/>
                    <Label Name="USRLastName" Content="Last name:" HorizontalAlignment="Left" Height="27" Margin="23,44,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <Label Name="USRLocation" Content="Location:" HorizontalAlignment="Left" Height="27" Margin="23,76,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <Label Name="USROffice365" Content="Office365:" HorizontalAlignment="Left" Height="27" Margin="23,108,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <Label Name="USRCopyPermissions" Content="Copy Permissions:" HorizontalAlignment="Left" Height="27" Margin="23,138,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <TextBox Name="USRFirstNameText" HorizontalAlignment="Left"  Width="152" Height="27" FontWeight="Regular" FontSize="14" Grid.Row="0" Margin="162,10,0,379.8"/>
                    <TextBox Name="USRLastNameText" HorizontalAlignment="Left"  Width="152" Height="27" FontWeight="Regular" FontSize="14" Margin="162,41,0,348.8"/>
                    <ComboBox Name="USRLocationBox" HorizontalAlignment="Left" Width="152" Height="27" FontWeight="Regular" FontSize="14" Margin="162,73,0,316.8">
                        <ComboBoxItem Name="AMS" Content="Amsterdam"/>
                        <ComboBoxItem Name="ANT" Content="Antwerpen"/>
                        <ComboBoxItem Name="BER" Content="Berlin"/>
                        <ComboBoxItem Name="DOR" Content="Dortmund"/>
                        <ComboBoxItem Name="FFM" Content="Frankfurt"/>
                        <ComboBoxItem Name="FFMEDG" Content="Frankfurt-EDG"/>
                        <ComboBoxItem Name="FFMTS" Content="Frankfurt-TS"/>
                        <ComboBoxItem Name="HER" Content="Herzogenrath"/>
                        <ComboBoxItem Name="KL"  Content="Kaiserslautern"/>
                        <ComboBoxItem Name="MIL" Content="Milan"/>
                        <ComboBoxItem Name="RIM" Content="Rimpar"/>
                        <ComboBoxItem Name="SWF" Content="Schweinfurt"/>
                        <ComboBoxItem Name="ZUR" Content="Zurich"/>
                    </ComboBox>
                    <ComboBox Name="USROffice365Box" HorizontalAlignment="Left"  Width="152" Height="27" FontWeight="Regular" FontSize="14" Margin="162,105,0,284.8">
                        <ComboBoxItem Name="E3" Content="Full"/>
                        <ComboBoxItem Name="Exchange" Content="Exchange + Skype"/>
                    </ComboBox>
                    <TextBox Name="USRCopyPermissionsText" HorizontalAlignment="Left"  Width="152" Height="27" FontWeight="Regular" FontSize="14" Margin="162,138,0,251.8" ToolTip="Existing SamAccountName or UPN (Optional)"/>
                    <CheckBox Name="USRExternal" Content="External Employee" HorizontalAlignment="Left" Margin="353,81,0,0" VerticalAlignment="Top"/>
                    <CheckBox Name="USRChange" Content="Change User" HorizontalAlignment="Left" Margin="353,49,0,0" VerticalAlignment="Top"/>
                    <Button Name="USRCheck" HorizontalAlignment="Left" Width="92"  Content='Check' Margin="353,134,0,259"/>
                    <Button Name="USRCreate" HorizontalAlignment="Right" Width="92"  Content='Create user' IsEnabled="False" Margin="0,134,10,259"/>
                    <ScrollViewer Margin="5,167,5,0" IsTabStop="True" VerticalScrollBarVisibility="Auto" Height="240" Width="606" Background="#FFE5E5E5">
                        <TextBlock Name="USRResult" HorizontalAlignment="Left" TextWrapping="Wrap" VerticalAlignment="Top" FontSize="14"/>
                    </ScrollViewer>
                </Grid>
            </TabItem>
            <TabItem Header="New SMB">
                <Grid Name="SMBGrid" Background="LightBlue" ShowGridLines="True" Margin="0,0,0,0">
                    <Label Name="SMBName" Content="Display name:" HorizontalAlignment="Left" Height="27" Margin="23,12,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0" Grid.Row="0"/>
                    <Label Name="SMBMail" Content="Email address:" HorizontalAlignment="Left" Height="27" Margin="23,44,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <Label Name="SMBMembers" Content="Members:" HorizontalAlignment="Left" Height="27" Margin="23,76,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <Label Name="SMBLocation" Content="Location:" HorizontalAlignment="Left" Height="27" Margin="23,108,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <TextBox Name="SMBNameText" HorizontalAlignment="Left"  Width="283" Height="27" FontWeight="Regular" FontSize="14" Grid.Row="0" Margin="162,12,0,382"/>
                    <TextBox Name="SMBMailText" HorizontalAlignment="Left"  Width="283" Height="27" FontWeight="Regular" FontSize="14" Margin="162,43,0,351"/>
                    <TextBox Name="SMBMembersText" HorizontalAlignment="Left"  Width="454" FontWeight="Regular" FontSize="14" Margin="162,76,0,318.2" ToolTip="Comma seperated: SamAccountName,UPN (Optional)"/>
                    <ComboBox Name="SMBLocationBox" HorizontalAlignment="Left" Width="152" Height="27" FontWeight="Regular" FontSize="14" Margin="162,105,0,284.8">
                        <ComboBoxItem Name="SMBAMS" Content="Amsterdam"/>
                        <ComboBoxItem Name="SMBANT" Content="Antwerpen"/>
                        <ComboBoxItem Name="SMBBER" Content="Berlin"/>
                        <ComboBoxItem Name="SMBDOR" Content="Dortmund"/>
                        <ComboBoxItem Name="SMBFFM" Content="Frankfurt"/>
                        <ComboBoxItem Name="SMBHER" Content="Herzogenrath"/>
                        <ComboBoxItem Name="SMBKL" Content="Kaiserslautern"/>
                        <ComboBoxItem Name="SMBMIL" Content="Milan"/>
                        <ComboBoxItem Name="SMBRIM" Content="Rimpar"/>
                        <ComboBoxItem Name="SMBSWF" Content="Schweinfurt"/>
                        <ComboBoxItem Name="SMBVWD" Content="vwd"/>
                        <ComboBoxItem Name="SMBZUR" Content="Zurich"/>
                    </ComboBox>
                    <Button Name="SMBCheck" HorizontalAlignment="Left" Width="92"  Content='Check' Margin="353,134,0,259"/>
                    <Button Name="SMBCreate" HorizontalAlignment="Right" Width="92"  Content='Create SMB' IsEnabled="False" Margin="0,134,10,259"/>
                    <ScrollViewer Margin="5,167,5,0" IsTabStop="True" VerticalScrollBarVisibility="Auto" Height="240" Width="606" Background="#FFE5E5E5">
                        <TextBlock Name="SMBResult" HorizontalAlignment="Left" TextWrapping="Wrap" VerticalAlignment="Top" FontSize="14"/>
                    </ScrollViewer>
                </Grid>
            </TabItem>
            <TabItem Header="New List">
                <Grid Name="LSTGrid" Background="LightBlue" ShowGridLines="True" Margin="0,0,0,0">
                    <Label Name="LSTName" Content="Display name:" HorizontalAlignment="Left" Height="27" Margin="23,12,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0" Grid.Row="0"/>
                    <Label Name="LSTMail" Content="Email address:" HorizontalAlignment="Left" Height="27" Margin="23,44,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <Label Name="LSTMembers" Content="Members:" HorizontalAlignment="Left" Height="27" Margin="23,76,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <Label Name="LSTLocation" Content="Location:" HorizontalAlignment="Left" Height="27" Margin="23,108,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <TextBox Name="LSTNameText" HorizontalAlignment="Left"  Width="283" Height="27" FontWeight="Regular" FontSize="14" Grid.Row="0" Margin="162,12,0,382"/>
                    <TextBox Name="LSTMailText" HorizontalAlignment="Left"  Width="283" Height="27" FontWeight="Regular" FontSize="14" Margin="162,43,0,351"/>
                    <TextBox Name="LSTMembersText" HorizontalAlignment="Left"  Width="454" FontWeight="Regular" FontSize="14" Margin="162,76,0,318.2" ToolTip="Comma seperated: SamAccountName,UPN (Optional)"/>
                    <ComboBox Name="LSTLocationBox" HorizontalAlignment="Left" Width="152" Height="27" FontWeight="Regular" FontSize="14" Margin="162,105,0,284.8">
                        <ComboBoxItem Name="LSTAMS" Content="Amsterdam"/>
                        <ComboBoxItem Name="LSTANT" Content="Antwerpen"/>
                        <ComboBoxItem Name="LSTBER" Content="Berlin"/>
                        <ComboBoxItem Name="LSTDOR" Content="Dortmund"/>
                        <ComboBoxItem Name="LSTFFM" Content="Frankfurt"/>
                        <ComboBoxItem Name="LSTHER" Content="Herzogenrath"/>
                        <ComboBoxItem Name="LSTKL" Content="Kaiserslautern"/>
                        <ComboBoxItem Name="LSTMIL" Content="Milan"/>
                        <ComboBoxItem Name="LSTRIM" Content="Rimpar"/>
                        <ComboBoxItem Name="LSTSWF" Content="Schweinfurt"/>
                        <ComboBoxItem Name="LSTVWD" Content="vwd"/>
                        <ComboBoxItem Name="LSTZUR" Content="Zurich"/>
                    </ComboBox>
                    <Button Name="LSTCheck" HorizontalAlignment="Left" Width="92"  Content='Check' Margin="353,134,0,259"/>
                    <Button Name="LSTCreate" HorizontalAlignment="Right" Width="92"  Content='Create List' IsEnabled="False" Margin="0,134,10,259"/>
                    <ScrollViewer Margin="5,167,5,0" IsTabStop="True" VerticalScrollBarVisibility="Auto" Height="240" Width="606" Background="#FFE5E5E5">
                        <TextBlock Name="LSTResult" HorizontalAlignment="Left" TextWrapping="Wrap" VerticalAlignment="Top" FontSize="14"/>
                    </ScrollViewer>
                </Grid>
            </TabItem>
            <TabItem Header="Out of Office / Forwarding">
                <Grid Name="OoOGrid" Background="LightBlue" ShowGridLines="True" Margin="0,0,0,0">
                    <Label Name="OoOMail" Content="Email:" HorizontalAlignment="Left" Height="27" Margin="23,12,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0" Grid.Row="0"/>
                    <Label Name="OoOmsg" Content="Out of Office text:" HorizontalAlignment="Left" Height="27" Margin="23,76,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <Label Name="OoOFWD" Content="Forwarding:" HorizontalAlignment="Left" Height="27" Margin="23,44,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <TextBox Name="OoOMailtext" HorizontalAlignment="Left"  Width="283" Height="27" FontWeight="Regular" FontSize="14" Grid.Row="0" Margin="162,12,0,382"/>
                    <TextBox Name="OoOFWDText" HorizontalAlignment="Left"  Width="283" Height="27" FontWeight="Regular" FontSize="14" Margin="162,43,0,351" ToolTip="If emty Forwarding will be disabled."/>
                    <TextBox Name="OoOMSGText" HorizontalAlignment="Left"  Width="454" FontWeight="Regular" FontSize="14" Margin="162,76,0,292" ToolTip="If emty Out of Office will be disabled."/>
                    <Button Name="OoOCheck" HorizontalAlignment="Left" Width="92"  Content='Check' Margin="353,134,0,259"/>
                    <Button Name="OoOCreate" HorizontalAlignment="Right" Width="92"  Content='Create msg' IsEnabled="False" Margin="0,134,10,259"/>
                    <ScrollViewer Margin="5,167,5,0" IsTabStop="True" VerticalScrollBarVisibility="Auto" Height="240" Width="606" Background="#FFE5E5E5">
                        <TextBlock Name="OoOResult" HorizontalAlignment="Left" TextWrapping="Wrap" VerticalAlignment="Top" FontSize="14"/>
                    </ScrollViewer>
                </Grid>
            </TabItem>
            <TabItem Header="Search Folder">
                <Grid Name="Search_Grid" Background="LightBlue" ShowGridLines="True" Margin="0,0,0,0">
                    <Label Name="SMail" Content="Email:" HorizontalAlignment="Left" Height="27" Margin="23,12,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0" Grid.Row="0"/>
                    <Label Name="SFolder" Content="Folder:" HorizontalAlignment="Left" Height="27" Margin="23,44,0,0" VerticalAlignment="Top" Width="152" FontWeight="Bold" FontSize="14" Grid.Column="0"/>
                    <TextBox Name="SearchMailText" HorizontalAlignment="Left"  Width="283" Height="27" FontWeight="Regular" FontSize="14" Grid.Row="0" Margin="162,12,0,382"/>
                    <TextBox Name="SearchFolderText" HorizontalAlignment="Left"  Width="283" Height="27" FontWeight="Regular" FontSize="14" Margin="162,43,0,351" ToolTip="(Optional)" />
                    <Button Name="SearchCheck" HorizontalAlignment="Left" Width="92"  Content='Check' Margin="353,134,0,259"/>
                    <ScrollViewer Margin="5,167,5,0" IsTabStop="True" VerticalScrollBarVisibility="Auto" Height="240" Width="606" Background="#FFE5E5E5">
                        <TextBlock Name="SearchResult" HorizontalAlignment="Left" TextWrapping="Wrap" VerticalAlignment="Top" FontSize="14"/>
                    </ScrollViewer>
                </Grid>
            </TabItem>
        </TabControl>
    </Window>
"@

    $NR=(New-Object System.Xml.XmlNodeReader $Form)
    $Win=[Windows.Markup.XamlReader]::Load( $NR )

    ##################################################################################################
    ##################################### VARIABLES FROM GUI #########################################
    ##################################################################################################
    $FN=$win.FindName("USRFirstNameText") 
    $LN=$win.FindName("USRLastNameText")
    $LB=$win.FindName("USRLocationBox")
    $O365B=$win.FindName("USROffice365Box")
    $USRCPT=$win.FindName("USRCopyPermissionsText")
    $External=$win.FindName("USRExternal")
    $Change=$win.FindName("USRChange")
    $USRCheck=$win.FindName("USRCheck")
    $USRRes=$win.FindName("USRResult")
    $USRCreate=$win.FindName("USRCreate")

    $SMBName=$win.FindName("SMBNameText")
    $SBMMail=$win.FindName("SMBMailText")
    $SMBLB=$win.FindName("SMBLocationBox")
    $SMBMember=$win.FindName("SMBMembersText")
    $SMBCheck=$win.FindName("SMBCheck")
    $SMBCreate=$win.FindName("SMBCreate")
    $SMBRes=$win.FindName("SMBResult")

    $LSTName=$win.FindName("LSTNameText")
    $LSTMail=$win.FindName("LSTMailText")
    $LSTLB=$win.FindName("LSTLocationBox")
    $LSTMember=$win.FindName("LSTMembersText")
    $LSTCheck=$win.FindName("LSTCheck")
    $LSTCreate=$win.FindName("LSTCreate")
    $LSTRes=$win.FindName("LSTResult")

    $OoOMail=$win.FindName("OoOMailtext")
    $OoOMSG=$win.FindName("OoOMSGText")
    $OoOFWD=$win.FindName("OoOFWDText")
    $OoOCheck=$win.FindName("OoOCheck")
    $OoOCreate=$win.FindName("OoOCreate")
    $OoORes=$win.FindName("OoOResult")

    $SFMail=$win.FindName("SearchMailText")
    $SFFolder=$win.FindName("SearchFolderText")
    $SFCheck=$win.FindName("SearchCheck")
    $SFRes=$win.FindName("SearchResult")


    ##################################################################################################
    ######################################    ADD CLICK EVENTS    ####################################
    ##################################################################################################

    $USRcheck.add_click({
    $FirstName=$FN.Text
    $LastName=$LN.Text
    $Location=$LB.Text
    $Office365=$O365B.Text
    $USRCopy=$USRCPT.Text
    USRCheck-event $FirstName $LastName $Location $Office365 $USRCopy
    })

    $USRCreate.add_click({
    USRCreate-event $usrcustomobj
    })


    $SMBCheck.add_click({
    $SName=$SMBName.Text
    $SMail=$SBMMail.Text
    $SLocation=$SMBLB.Text
    $SMember=$SMBMember.Text
    SMBCheck-event $SName $SMail $SLocation $SMember
    })

    $SMBCreate.add_click{
    SMBCreate-event $smbcustomobj
    }

    $LSTCheck.add_click({
    $LName=$LSTName.Text
    $LMail=$LSTMail.Text
    $LLocation=$LSTLB.Text
    $LMember=$LSTMember.Text
    LSTCheck-event $LName $LMail $LLocation $LMember
    })
    $LSTCreate.add_click{
    LSTCreate-event $lstcustomobj
    }

    $OoOCheck.add_click({
    $Global:OMail=$OoOMail.Text
    $Global:OMSG=$OoOMSG.Text
    $Global:OFWD=$OoOFWD.Text
    OoOCheck-event $Global:OMail $Global:OMSG $Global:OFWD
    })
    $OoOCreate.add_click{
    OoOCreate-event $Global:OMail $Global:OMSG $Global:OFWD
    }

    $SFCheck.add_click({
    $SearchMail=$SFMail.Text
    $SearchFolder=$SFFolder.Text
    SFCheck-event $SearchMail $SearchFolder
    })

    ##################################################################################################
    #########################################     SHOW GUI     #######################################
    ##################################################################################################
    $Win.showdialog() |Out-Null -ErrorAction SilentlyContinue
}
##################################################################################################
###################################    Clear session/Variables    ################################
##################################################################################################
If($O365Session)
    {
    Remove-PSSession -Session $O365Session
    $O365Session=$null
    }
Remove-Variable * -ErrorAction SilentlyContinue