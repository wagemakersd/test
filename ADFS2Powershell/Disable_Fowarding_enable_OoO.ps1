﻿ #Set recource properties capacity, location, phone
$msolcred = get-credential
connect-msolservice -credential $msolcred

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session
 
 Get-Mailbox @vwd.com | FL ForwardingAddress
 get-MailboxAutoReplyConfiguration -Identity @vwd.com |fl AutoReplyState  


$message = "Buongiorno. La Sig.ra Nicoletta Pica non è più dipendente della Società vwd group Italia Srl.  In caso di necessità si prega di inviare il messaggio  alla Sigra Margherita Gentile  al seguente indirizzo email  mgentile@vwd.com" 	
Set-Mailbox npica@vwd.com -ForwardingAddress $Null
Set-MailboxAutoReplyConfiguration -Identity npica@vwd.com -AutoReplyState Enabled -InternalMessage $message -ExternalMessage $message
Get-Mailbox npica@vwd.com| FL DeliverToMailboxAndForward
get-MailboxAutoReplyConfiguration -Identity npica@vwd.com |fl AutoReplyState, ExternalMessage