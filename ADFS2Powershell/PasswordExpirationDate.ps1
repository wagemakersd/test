﻿Get-ADUser -Identity eenhoornw –Properties "DisplayName", "msDS-UserPasswordExpiryTimeComputed" |

Select-Object -Property "Displayname",@{Name="ExpiryDate";Expression={[datetime]::FromFileTime($_."msDS-UserPasswordExpiryTimeComputed")}} 



-filter {Enabled -eq $True -and PasswordNeverExpires -eq $False}