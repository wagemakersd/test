$Thumbprint="45d2e0b4238489cdb93295996483305202ba50f6"
$Tenant="23371993-8e9a-4de5-97db-90d3e293e863"
$Org="infrontonline.onmicrosoft.com"
$App="cc8469f3-2bdf-4d2a-b430-636ea63a1691"
Connect-ExchangeOnline -CertificateThumbprint $Thumbprint -AppID $App -Organization $Org
Connect-MgGraph -CertificateThumbprint $Thumbprint -ClientId $App -TenantId $Tenant

$dc="deffm-dc3.vwdgroup.net"
$sqlserver="deffm-deploy1.vwdgroup.net"
$cred=Get-Credential

#load from AD
#$synced=get-adgroup -filter {(extensionattribute11 -like 'infrontgroupsynced') -and (GroupScope -ne 'universal') -and (Grouptype -eq 'Distribution')} -Properties * -Server $dc -Credential $cred
#$synced|?{$_.GroupScope -eq "universal"}|Ft DisplayName,mail,SamAccountName

#load from DB
$groupsdb=Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "select * From [Powershell].[dbo].[All_DLs] Where Cloud_Created = '$false'" -TrustServerCertificate
$groupsdb=Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "select * From [Powershell].[dbo].[All_DLs]" -TrustServerCertificate


#Get all properties to put in DB
$groups=@()
foreach($entry in $synced){
    $distributiongroup=(Get-DistributionGroup $entry.mail)
    $proxyAddresses = (($distributiongroup.emailaddresses) -replace "SMTP:",""|?{($_ -ne $entry.mail)}) -join ","
    $members = Get-ADGroupMember -Identity $entry.ObjectGUID -Server deffm-dc3.vwdgroup.net -Credential $cred
    $UsrUPN=@()
    $grpmail=@()
    If($members){
        write-host "Query members for $($entry.DisplayName)"
        $members|%{
            $_.SamAccountName
            if($_.ObjectClass -eq "group"){
                $grpmail+=(get-adgroup $_.ObjectGUID -server deffm-dc3.vwdgroup.net -Credential $cred -Properties * |?{$_.extensionAttribute11 -eq "InfrontGroupSynced"}).mail
            }
            if($_.ObjectClass -eq "User"){
                $UsrUPN+=(get-aduser -Identity $_.SamAccountName -server deffm-dc3.vwdgroup.net -Credential $cred -Properties *).UserPrincipalName
            }
        }
    }

    $acceptfrom=$null
    if($entry.msExchRequireAuthToSendTo){
        $acceptfrom=$distributiongroup.AcceptMessagesOnlyFrom -join ","
    }
    if($entry.ManagedBy){
        $manager=(get-aduser -identity $entry.ManagedBy -Server deffm-dc3.vwdgroup.net -Credential $cred).userprincipalname
    }
    else {
        $manager=$null
    }
    $groups += New-Object -typename PSObject -property @{
        DisplayName                 = $entry.DisplayName
        DLObjectGUID                = $entry.ObjectGUID
        Description                 = $entry.description
        Primarymail                 = $entry.mail
        ProxyAddresses              = $proxyAddresses
        SamAccountName              = $entry.SamAccountName
        Manager                     = $manager
        MemberUSR                   = $UsrUPN -join ","
        MemberGRP                   = $grpmail -join ","
        Membertypes                 = ($members.objectClass |select -Unique) -join ","
        MemberOff                   = $entry.memberof -join ","
        msExchHideFromAddressLists  = $entry.msExchHideFromAddressLists
        msExchRequireAuthToSendTo   = $entry.msExchRequireAuthToSendTo
        AcceptMessagesOnlyFrom      = $acceptfrom
        newPrimary                  = ($entry.mail.Split("@")[0]+"@infront.co")
        PrefixedDisplayName         = ("CloudOnly_"+$entry.DisplayName)
        Cloud_Created               = $false
        OnPrem_synced               = $true
    }
}

#Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "drop table All_DLs" -TrustServerCertificate
    #Write-SqlTableData -DatabaseName Powershell -SchemaName dbo -TableName All_DLs -ServerInstance $sqlserver -InputData $groups -force

    #Write-SqlTableData -DatabaseName Powershell -SchemaName dbo -TableName All_DLs-backup -ServerInstance $sqlserver -InputData $groupsdb -force
#Write-SqlTableData -DatabaseName Powershell -SchemaName dbo -TableName Existingmail_DLs -ServerInstance $sqlserver -InputData $groupsdb1 -force

#$groups|?{($_.Membertypes)}

#$groups|?{($_.Membertypes -match "Group")}|export-csv c:\temp\DL_member.csv -NoTypeInformation -encoding UTF8
#$groups|?{($_.Membertypes -eq "User")}|sort DisplayName|export-csv c:\temp\DL_memberUSR.csv -NoTypeInformation -encoding UTF8
#$groups|?{($_.Membertypes -eq "")}|sort DisplayName|export-csv c:\temp\DL_nomembers.csv -NoTypeInformation -encoding UTF8

($groups|?{($_.Membertypes -eq "User")}).count
($groups|?{($_.Membertypes -match "group")}).count
($groups|?{($_.Membertypes -eq "")}).count
$groups.count

foreach($grp in ($groups|?{($_.Membertypes -eq "")})){
    #set-adgroup -Identity $grp.DLObjectGUID -server deffm-dc3.vwdgroup.net -Clear extensionAttribute11 -Credential $cred
    #$grp.DisplayName
    #Get-DistributionGroupMember -Identity $grp.Primarymail
}

($groups|?{($_.Membertypes -eq "User")})|?{$_.Primarymail -match "@infront.co"}

($groupsdb|?{$_.Cloud_Created -eq $false}).Count

($groupsdb|?{($_.Cloud_Created -eq $false) -and ($_.PrefixedDisplayName -eq "cloudONly_")})|%{
    $prefix=$("CloudOnly_"+$_.SamAccountName)
    $prefix
    Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "Update [Powershell].[dbo].[All_DLs] set PrefixedDisplayName = '$prefix' where DLObjectGUID = '$($_.DLObjectGUID)'" -TrustServerCertificate
}


#check if cloudonly is created
Foreach($grp in $groupsdb){
    if(!($grp.Cloud_Created -eq $true)){
        $grp.PrefixedDisplayName
        if(Get-DistributionGroup -Filter "displayname -eq '$($grp.prefixeddisplayname)'"){
            Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "Update [Powershell].[dbo].[All_DLs] set Cloud_Created = 'True' Where DLObjectGUID = '$($grp.DLObjectGUID)'" -TrustServerCertificate
        }
    }
}

foreach($entry in $groupsdb1){
    $newprimary="CloudOnly_"+$entry.Primarymail
    Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "Update [Powershell].[dbo].[all_DLs] set newPrimary = '$newprimary'  Where DLObjectGUID = '$($entry.DLObjectGUID)'" -TrustServerCertificate
}

#
Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "Update [Powershell].[dbo].[All_DLs] set Cloud_Created = 'true' Where DLObjectGUID = 'ff6a5198-68c2-4332-8c0d-b7916929e677'" -TrustServerCertificate

### CREATE NEW DL IN THE CLOUD
$notprocessed=@()
$Processed=@()
foreach($grp in $groupsdb){
    $filter="*"+$grp.newPrimary
    if(!(Get-Recipient -Filter ("emailaddresses -like '$filter'"))){
        Write-host  -ForegroundColor Cyan "Processing $($grp.prefixeddisplayname)"
        #infront.co as primary
        #Add  Prefix to the displayname
        #Use msExchRequireAuthToSendTo vallue
        #Add user members
        If (($grp.Manager -match "@") -and ($grp.manager -notmatch "@bsd.local")){
            if((get-mguser -UserId $grp.Manager -Property AccountEnabled).AccountEnabled -eq $true){
                $managerenabled=$true
            }else{
                $managerenabled=$false
            }
        }Else{
            $managerenabled=$false
        }

        If ($managerenabled){
            try{           

                New-DistributionGroup -Name $grp.prefixeddisplayname -CopyOwnerToMember -managedby $grp.Manager -PrimarySmtpAddress $grp.newPrimary -Description $grp.Description -RequireSenderAuthenticationEnabled $grp.msExchRequireAuthToSendTo -ErrorAction Stop
                Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "Update [Powershell].[dbo].[All_DLs] set Cloud_Created = 'True' Where DLObjectGUID = '$($grp.DLObjectGUID)'" -TrustServerCertificate
                $Processed+=$grp
            }
            catch{
                Write-host -ForegroundColor RED "Failed to create $($grp.prefixeddisplayname)"
                write-error $_.exception.message
                break
            }
        }Else{
            try{  
                New-DistributionGroup -Name $grp.prefixeddisplayname -PrimarySmtpAddress $grp.newPrimary -Description $grp.Description -RequireSenderAuthenticationEnabled $grp.msExchRequireAuthToSendTo -ErrorAction Stop 
                Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "Update [Powershell].[dbo].[All_DLs] set Cloud_Created = 'True' Where DLObjectGUID = '$($grp.DLObjectGUID)'" -TrustServerCertificate
                $Processed+=$grp
            }
            catch{
                Write-host -ForegroundColor RED "Failed to create $($grp.prefixeddisplayname)"
                write-error $_.exception.message
                break
            }
        }
        #Hide from addresslist
        if($grp.AcceptMessagesOnlyFrom){
            Set-DistributionGroup -identity $grp.prefixeddisplayname -HiddenFromAddressListsEnabled $true -AcceptMessagesOnlyFrom $grp.AcceptMessagesOnlyFrom
        }Else{
            Set-DistributionGroup -identity $grp.prefixeddisplayname -HiddenFromAddressListsEnabled $true
        }
    }Else{
        Write-host -ForegroundColor Yellow "Couldn't processing $($grp.DisplayName)"

        $notprocessed+=$grp
    }
}

## ADD MEMBERS
$failed=@()
foreach($grp in $groups){
    if($grp.MemberUSR){
        Write-host  -ForegroundColor Cyan "Adding users to $($grp.prefixeddisplayname)"
        ($grp.MemberUSR -split ",")|%{
            try{
                Write-host  -ForegroundColor white "Adding user $_ to $($grp.newPrimary)"
                Add-DistributionGroupMember -identity $grp.newPrimary -Member $_ -ErrorAction stop
            }
            catch{
                Write-host  -ForegroundColor Yellow "Couldn't add $_ to $($grp.newPrimary)"
                $failed+=New-Object -typename PSObject -Property @{
                    Groupmail = $grp.newPrimary
                    Groupname = $grp.PrefixedDisplayName
                    User      = $_
                }
            }
        }
    }
}

foreach($grp in ($groups|?{$_.displayname -match 'FinanceControlling-Ffm'})){
    Get-DistributionGroupmember -identity $grp.Primarymail|%{$_
        Add-DistributionGroupMember -identity $grp.prefixeddisplayname -Member $_.name }
}

($groupsdb|?{$_.membergrp -match "FWD_"}).count
$fwddist=Get-DistributionGroup -filter "PrimarySmtpAddress -like 'FWD_*'"
#add FWD_ distribution lists to cloud_only
$notadded2=@()
foreach($grp in $notadded){
    $1=$grp.displayname -replace "_", " "
    $new=$1 -replace "FWD ", "CloudOnly_"
    $new
    $DL=Get-DistributionGroup -filter "Displayname -like '$new*'"
    Get-DistributionGroup -filter "Primarysmtpaddress -like 'cloud_kuendigungen@infront.co'"
    $DL
    <#
    IF($DL){
        Get-DistributionGroupmember -identity $grp.displayname|%{
            write-host "Adding $_ to $($DL.PrimarySmtpAddress)"
            Add-DistributionGroupMember -identity $DL.PrimarySmtpAddress -Member $_.name 
        }
    }Else{
        $notadded2+=$grp
    }
        #>
}






### REMOVE attribute SYNC AND CHECK IF THE OLD LISTS ARE REMOVED
$onpremsyncdisabled=@()
$stillonpremsynced=@()
foreach($grp in $Processed){
    Try{
        Write-host  -ForegroundColor Cyan "Processing $($grp.DisplayName)"
        #removing AD attribute
        set-adgroup -identity $grp.DLObjectGUID -Clear extensionAttribute11 -ErrorAction stop -Credential $cred -server $dc
        Invoke-Sqlcmd -ServerInstance $sqlserver -Database Powershell -Query "Update [Powershell].[dbo].[All_DLs] set OnPrem_synced = 'False' Where DLObjectGUID = '$($grp.DLObjectGUID)'" -TrustServerCertificate
        $onpremsyncdisabled+=$grp
    }
    Catch{
        Write-host  -ForegroundColor RED "Failed processing $($grp.DisplayName)"
        write-error $_.exception.message
        $stillonpremsynced+=$grp
    }
}
#sync on SE8DC.osl.infront.as or wait 30minutes
#Start-ADSyncSyncCycle

### EDIT NEW DL's to be equal to the old ones.
$Failed=@()
$success=@()
foreach($grp in $onpremsyncdisabled){
    
    $filter="*"+$grp.newPrimary
    if(Get-Recipient -Filter ("emailaddresses -like '$filter'")){
        Write-host  -ForegroundColor Cyan "Processing $($grp.newPrimary)"
        Write-host  -ForegroundColor white "Adding previous primary $($grp.Primarymail) to '$($grp.newPrimary)' as proxy"
        #Add the Primarymail (as proxy) of the old list
        #remove prefix from displayname
        Set-DistributionGroup -Identity $grp.newPrimary -EmailAddresses @{Add=("smtp:"+$grp.Primarymail)} -displayname $grp.displayname -name $grp.displayname 
        If($grp.msExchRequireAuthToSendTo){
            if($grp.AcceptMessagesOnlyFrom){
                Set-DistributionGroup -Identity $grp.newPrimary -RequireSenderAuthenticationEnabled $grp.msExchRequireAuthToSendTo -AcceptMessagesOnlyFrom $grp.AcceptMessagesOnlyFrom
            }else{
                Set-DistributionGroup -Identity $grp.newPrimary -RequireSenderAuthenticationEnabled $grp.msExchRequireAuthToSendTo
            }
        }
        #Remove msExchHideFromAddressLists if not set to old list
        if(!($grp.msExchHideFromAddressLists)){
            Write-host  -ForegroundColor White "Showing '$($grp.displayname)' in addresbook again"
            Set-DistributionGroup -Identity $grp.newPrimary -HiddenFromAddressListsEnabled $false
        }

        #remove onmicrosoft.com address
        (Get-DistributionGroup -Identity $grp.newPrimary).EmailAddresses |?{$_ -match "@infrontonline.onmicrosoft.com"}|%{
            Write-host  -ForegroundColor White "Removing temp onmicrosoft.com address: $_"
            Set-DistributionGroup -Identity $grp.newPrimary -EmailAddresses @{remove=($_)}
        }
        

        #Add the Proxyaddresses of the old list
        if($grp.ProxyAddresses){
            $grp.ProxyAddresses.split(",")|%{
                Write-host  -ForegroundColor White "Adding proxy $("smtp:"+$_) to $($grp.newPrimary)"
                Set-DistributionGroup -Identity $grp.newPrimary -EmailAddresses  @{Add=("smtp:"+$_)}
            }
        } 
        $success+=$grp
    }
    else{
        $Failed+=$grp
    }
}

#Add cloud groups as member
foreach($grp in $success|?{($_.Membertypes -match "Group")}){

    if(Get-DistributionGroup -Filter "PrimarySmtpAddress -eq '$($grp.newPrimary)'"){
        Write-host  -ForegroundColor white "Adding membergroups to $($grp.newPrimary)"
        ($grp.MemberGRP -split ",")|%{
            Write-host  -ForegroundColor white "Adding user $_ to $($grp.newPrimary)"
            Add-DistributionGroupMember -Identity $grp.newPrimary -Member $_
        }
    }

}