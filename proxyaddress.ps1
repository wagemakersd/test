connect-msolservice

$csv=import-csv -path c:\temp\proxy.csv
$csv=$csv|?{$_.extensionAttribute2 -eq "SMB_EDG"}
$domains=(get-msoldomain).name|?{$_ -eq "edg.ag"}
$array=@()
foreach($domain in $domains)
{
    $Domain
    foreach ($user in $csv)
    {
        if ($user.domain -eq $domain)
        {
            switch ($user.extensionAttribute2)
            {
                Infrontfinance
                {
                    #add proxy to user
                    $sip="sip:"+$user.extensionAttribute1
                    $proxys=$user.proxy -split ";"
                    foreach ($address in $proxys)
                    {
                            $address=$address.ToLower()
                            Set-ADUser -Identity $user.SamAccountName -Add @{Proxyaddresses=$address} -whatif
                    }
                    Set-ADUser -Identity $user.SamAccountName -Add @{Proxyaddresses=$sip} -whatif
                }
                EDG
                {
                    #add proxy to user
                    $proxys=$user.proxy -split ";"
                    foreach ($address in $proxys)
                    {
                        If($address.contains("SMTP:"))
                        {
                            $SIP=$address -replace "SMTP:","sip:"
                            Set-ADUser -Identity $user.SamAccountName -Add @{Proxyaddresses=$SIP} -whatif
                        }
                        Set-ADUser -Identity $user.SamAccountName -Add @{Proxyaddresses=$address} -whatif
                    }
                }
                default
                {
                    #add proxy to user
                    $proxys=$user.proxy -split ";"
                    foreach ($address in $proxys)
                    {
                        Set-ADUser -Identity $user.SamAccountName -Add @{Proxyaddresses=$address} -whatif
                    }
                }
            }
        }
    }
}
