﻿#Set recource properties capacity, location, phone
$msolcred = get-credential
connect-msolservice -credential $msolcred

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $msolcred -Authentication Basic -AllowRedirection
Import-PSSession $Session

Import-Module LyncOnlineConnector
$cssess=New-CsOnlineSession -Credential $cred
Import-PSSession $cssess -AllowClobber


Get-Mailbox -Identity CBoehm@vwd.com | Format-List
Get-Mailbox -Identity FFM-RES-00011@vwd.com | Format-List

Get-Mailbox -Identity AMS-RES-00006@vwd.com | Format-List Name,DisplayName,Alias,PrimarySmtpAddress,Database

Set-MailboxCalendarConfiguration -Identity FFM-RES-00011@vwd.com -WorkingHoursTimeZone "W. Europe Standard Time"

Get-Mailbox -ResultSize unlimited -Filter {(RecipientTypeDetails -eq 'RoomMailbox')} | Get-MailboxCalendarConfiguration | Where-Object {$_.WorkingHoursTimeZone -eq "Pacific Standard Time"} | Set-MailboxCalendarConfiguration -WorkingHoursTimeZone "W. Europe Standard Time"

Get-Mailbox -ResultSize unlimited -Filter {(RecipientTypeDetails -eq 'RoomMailbox')} | Select Name,DisplayName, @{Name="TimeZone"; Expression={(Get-MailboxCalendarConfiguration $_.Name).WorkingHoursTimeZone}}

Format-List Name,DisplayName,WorkingHoursTimeZone



Set-Mailbox AMS-RES-00006@vwd.com -Type Room
Set-Mailbox AMS-RES-00007@vwd.com -Type Room

Get-CalendarProcessing -Identity FFM-RES-00011@vwd.com | Format-list
Get-CalendarProcessing -Identity AMS-RES-00003@vwd.com | Format-list
Get-MailboxFolderPermission -Identity AMS-RES-00003@vwd.com | Format-List

Set-CalendarProcessing -Identity AMS-RES-00006@vwd.com -AutomateProcessing AutoAccept -DeleteComments $false -DeleteSubject $true -BookingWindowInDays 365 -ForwardRequestsToDelegates $false
Set-CalendarProcessing -Identity AMS-RES-00007@vwd.com -AutomateProcessing AutoAccept -DeleteComments $false -DeleteSubject $true -BookingWindowInDays 365 -ForwardRequestsToDelegates $false
Set-CalendarProcessing -Identity AMS-RES-00003@vwd.com -AutomateProcessing AutoAccept -DeleteComments $false -DeleteSubject $true -BookingWindowInDays 365 -ForwardRequestsToDelegates $false


(Get-MailboxJunkEmailConfiguration -Identity creich@vwd.com).TrustedSendersAndDomains
Get-MailboxJunkEmailConfiguration -Identity pvanderkooij.ext@vwd.com

Get-InboxRule -Mailbox SAlvarez@vwd.com -Identity "Index-update" | Select -Property *
Get-InboxRule -Mailbox creich@vwd.com | Select -Property *
Get-InboxRule -Mailbox pvanderkooij.ext@vwd.com | Select -Property *


$TimeZone = Get-ChildItem "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Time zones" | foreach {Get-ItemProperty $_.PSPath}; $TimeZone | sort PSChil | Format-Table -Auto PSChildname,Display
Start-ADSyncSyncCycle -policytype Delta


