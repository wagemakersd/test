param (
	[switch]$CollectPFInfo, #Collects Public Folder Details
    [switch]$CollectUGInfo # Collects Unified Groups Details
)

Begin{
    Function Get-ElapsedTime($stopwatch){


        $totalSecs =  [math]::Round($stopwatch.Elapsed.TotalSeconds,0)
        $stopwatch_time_elapsed = [timespan]::fromseconds($totalSecs).tostring()
        $stopwatch.Restart()

        return $stopwatch_time_elapsed
    }
    Function Process-SMTP-Address($object,$itemType){
        $processData =@()
        #Loop through each additional alias smtp address and add to the list
        foreach($emailaddress in $object.EmailAddresses){
            #Check to see if the address is an smtp address and if so we want to capture it, case sensitive to exclude primary address we already captured
            If($emailaddress -clike "SMTP:*"){
                #Check to see if the left over addresses are tenant addresses
                $exportSmtpAddress = ($emailaddress).split(":")[1]

                #Export AdditionalSMTP address data
                $processData += [PSCustomObject]@{
                    ItemType = $itemType
                    RecipientTypeDetails = $object.RecipientTypeDetails
                    AddressType = "PrimarySMTPAddress"
                    SMTPAddress = $exportSmtpAddress
                    SMTPDomain = (($exportSmtpAddress).split("@")[1]).ToLower()
                }

            }
            ElseIf($emailaddress -clike "smtp:*"){
                #Check to see if the left over addresses are tenant addresses
                $exportSmtpAddress = ($emailaddress).split(":")[1]

                #Export AdditionalSMTP address data
                $processData += [PSCustomObject]@{
                    ItemType = $itemType
                    RecipientTypeDetails = $object.RecipientTypeDetails
                    AddressType = "AdditionalSMTPAddress"
                    SMTPAddress = $exportSmtpAddress
                    SMTPDomain = (($exportSmtpAddress).split("@")[1]).ToLower()
                }
            }
            Else{
                #Skipping non smtp address values since we done need them, leaving else clause incase we want to do something in the future
            }
        }
        return $processData
    }
    Function Connect-Exchange-Online(){
        If(!(Get-Command | ? {$_.Name -like "Get-UnifiedGroup"})){
            if ((Test-ClickOnce -ApplicationName "Microsoft Exchange Online Powershell Module" ) -eq $false)  {
                Install-ClickOnce -Manifest "https://cmdletpswmodule.blob.core.windows.net/exopsmodule/Microsoft.Online.CSE.PSModule.Client.application"
             }
             #Load the Module
             $script = Load-ExchangeMFAModule
             #Dot Source the associated script
             . $Script
             
             #make sure the Exchange session uses the same proxy settings as IE/Edge 
             $ProxySetting = New-PSSessionOption -ProxyAccessType IEConfig
             Connect-EXOPSSession #-PSSessionOption $ProxySetting
        }
        Get-MsolDomain -ErrorAction SilentlyContinue | out-null
        If(-not $?){
            Write-Host "Connecting to MSOL..." -ForegroundColor Green -BackgroundColor Black
            Connect-MsolService
        }
    }
    function Install-ClickOnce {
        [CmdletBinding()] 
        Param(
            $Manifest = "https://cmdletpswmodule.blob.core.windows.net/exopsmodule/Microsoft.Online.CSE.PSModule.Client.application",
            #AssertApplicationRequirements
            $ElevatePermissions = $true
        )
        Try { 
            Add-Type -AssemblyName System.Deployment
            
            Write-Verbose "Start installation of ClickOnce Application $Manifest "
    
            $RemoteURI = [URI]::New( $Manifest , [UriKind]::Absolute)
            if (-not  $Manifest)
            {
                throw "Invalid ConnectionUri parameter '$ConnectionUri'"
            }
    
            $HostingManager = New-Object System.Deployment.Application.InPlaceHostingManager -ArgumentList $RemoteURI , $False
        
            #register an event to trigger custom event 
            Register-ObjectEvent -InputObject $HostingManager -EventName GetManifestCompleted -Action { 
                new-event -SourceIdentifier "ManifestDownloadComplete"
            } | Out-Null
            #register an event to trigger custom event 
            Register-ObjectEvent -InputObject $HostingManager -EventName DownloadApplicationCompleted -Action { 
                new-event -SourceIdentifier "DownloadApplicationCompleted"
            } | Out-Null
    
            #get the Manifest
            $HostingManager.GetManifestAsync()
    
            #Waitfor up to 5s for our custom event
            $event = Wait-Event -SourceIdentifier "ManifestDownloadComplete" -Timeout 5
            if ($event ) {
                $event | Remove-Event
                Write-Verbose "ClickOnce Manifest Download Completed"
    
                $HostingManager.AssertApplicationRequirements($ElevatePermissions)
                #todo :: can this fail ?
                
                #Download Application 
                $HostingManager.DownloadApplicationAsync()
                #register and wait for completion event 
                # $HostingManager.DownloadApplicationCompleted 
                $event = Wait-Event -SourceIdentifier "DownloadApplicationCompleted" -Timeout 15
                if ($event ) {
                    $event | Remove-Event
                    Write-Verbose "ClickOnce Application Download Completed"
                } else {
                    Write-error "ClickOnce Application Download did not complete in time (15s)"
                }
            } else {
                Write-error "ClickOnce Manifest Download did not complete in time (5s)"
            }
    
            #Clean Up 
        } finally {
            #get rid of our eventhandlers
            Get-EventSubscriber|? {$_.SourceObject.ToString() -eq 'System.Deployment.Application.InPlaceHostingManager'} | Unregister-Event
        }
    }
    function Get-ClickOnce {
    [CmdletBinding()]  
    Param(
        $ApplicationName = "Microsoft Exchange Online Powershell Module"
    )
        $InstalledApplicationNotMSI = Get-ChildItem HKCU:\Software\Microsoft\Windows\CurrentVersion\Uninstall | foreach-object {Get-ItemProperty $_.PsPath}
        return $InstalledApplicationNotMSI | ? { $_.displayname -match $ApplicationName } | Select-Object -First 1
    }
    

    Function Test-ClickOnce {
    [CmdletBinding()] 
    Param(
        $ApplicationName = "Microsoft Exchange Online Powershell Module"
    )
        return ( (Get-ClickOnce -ApplicationName $ApplicationName) -ne $null) 
    }
    
    
    <# Simple UnInstall 
    #>
    function Uninstall-ClickOnce {
    [CmdletBinding()] 
    Param(
        $ApplicationName = "Microsoft Exchange Online Powershell Module"
    )
        $app=Get-ClickOnce -ApplicationName $ApplicationName
    
        #Deinstall One to remove all instances 
        if ($App) { 
            $selectedUninstallString = $App.UninstallString 
            #Seperate cmd from parameters (First Space) 
            $parts = $selectedUninstallString.Split(' ', 2)
            Start-Process -FilePath $parts[0] -ArgumentList $parts[1] -Wait 
            #ToDo : Automatic press of OK 
            #Start-Sleep 5
            #$wshell = new-object -com wscript.shell
            #$wshell.sendkeys("`"OK`"~")
    
            $app=Get-ClickOnce -ApplicationName $ApplicationName
            if ($app) {
                Write-verbose 'De-installation aborted'
                #return $false
            } else {
                Write-verbose 'De-installation completed'
                #return $true
            } 
            
        } else {
            #return $null
            wirte-host "oops"
        }
    }
    Function Load-ExchangeMFAModule { 
    [CmdletBinding()] 
    Param ()
        $Modules = @(Get-ChildItem -Path "$($env:LOCALAPPDATA)\Apps\2.0" -Filter "Microsoft.Exchange.Management.ExoPowershellModule.manifest" -Recurse )
        if ($Modules.Count -ne 1 ) {
            throw "No or Multiple Modules found : Count = $($Modules.Count )"  
        }  else {
            $ModuleName =  Join-path $Modules[0].Directory.FullName "Microsoft.Exchange.Management.ExoPowershellModule.dll"
            Write-Verbose "Start Importing MFA Module"
            Import-Module -FullyQualifiedName $ModuleName  -Force 
    
            $ScriptName =  Join-path $Modules[0].Directory.FullName "CreateExoPSSession.ps1"
            if (Test-Path $ScriptName) {
                return $ScriptName
            } else {
                throw "Script not found"
                return $null
            }
        }
    }
}
Process{
    #Setup variables
    $exportData = @()
    $mailboxExportList = @()
    $distributionGroupExportList = @()
    $unifiedGroupExportList = @()
    $mailboxReportInfo = @()
    $distributionGroupReportInfo =@()
    $meuReportInfo =@()
    $unifiedGroupReportInfo =@()
    $licenseReportInfo =@()
    $contactExportList =@()
    $meuExportList =@()

    #Check to see if sessions are open to MSOL and EXO
    Connect-Exchange-Online

    #Collect Tenant Info
    Write-Progress -Activity "Step 1 of 10: Collecting Office365 tenant configuration information" -Status "Collecting: AADC status, PublicFolder config and Accepted Domains"
    $tenantExportInfo = Get-OrganizationConfig -ErrorAction SilentlyContinue | Select-Object DisplayName,Name,HybridConfigurationStatus,PublicFoldersEnabled,PublicFoldersLockedForMigration,OAuth2ClientProfileEnabled                            
    $companyInfo = Get-MsolCompanyInformation -ErrorAction SilentlyContinue | Select-Object DirectorySynchronizationEnabled,PasswordSynchronizationEnabled,DirSyncServiceAccount,LastDirSyncTime,LastPasswordSyncTime
    $dirSyncFeatures = Get-MsolDirSyncFeatures -ErrorAction SilentlyContinue | Select-Object DirSyncFeature,Enabled
    $transportInfo = Get-TransportRule -ErrorAction SilentlyContinue | Select-Object Name,State,Mode,Priority,Comments
    $acceptedDomainInfo = Get-AcceptedDomain -ErrorAction SilentlyContinue | Select-Object Name,DomainName,DomainType,Default
    If($CollectPFInfo){
        $publicFolderInfo = Get-PublicFolder -Recurse -ResultSize Unlimited  -ErrorAction SilentlyContinue | Select-Object Name,Identity,MailEnabled,ContentMailboxName
        $mailPublicFolderInfo = Get-MailPublicFolder -ResultSize Unlimited -ErrorAction SilentlyContinue | Select-Object Name,Identity,PrimarySMTPAddress,EmailAddresses,HiddenFromAddressListsEnabled
    }
    $orgRelatiionhips = Get-OrganizationRelationship -ErrorAction SilentlyContinue | select Identity,FreeBusyAccessEnabled,FreeBusyAccessLevel,Enabled,DomainNames

    #Setup export path
    $tenantName = $tenantExportInfo.Name
    $txtDate = (Get-Date -Format MMddyyyy-HHmm).ToString()
    $txtHTMLPath = "$($home)\Desktop\Office365TenantExportResults($tenantName)-$txtDate.html"

    #Create character list so we can create some visualization on progress
    $filterList = [char[]]([int][char]'a'..[int][char]'z')
    $filterList += [char[]]([int][char]'0'..[int][char]'9')

    #Start timer so we can track performance impact on changes made
    $stopwatch =  [system.diagnostics.stopwatch]::StartNew()

    #Collect Mailbox Info============================
    $i =0
    Foreach ($character in $filterList){
        #Write our progress to screen
        Write-Progress -Activity "Step 2 of 10: Collecting mailbox info from Office365 tenant" -Status "Currently processing UPNs starting with: $character" -percentComplete (($i / $filterList.Count) * 100)
        
        #Collect all mailboxes and their SMTP addresses
        $mailboxExportList += Get-Mailbox -ResultSize Unlimited -Filter "UserPrincipalName -like '$character*'" | Select-Object UserPrincipalName,Alias,PrimarySmtpAddress,EmailAddresses,ExchangeUserAccountControl,RecipientTypeDetails,DisplayName,AccountDisabled,HiddenFromAddressListsEnabled,ForwardingSmtpAddress,IsDirSynced,LitigationHoldEnabled,ArchiveStatus
        $i++
    }
    #Confirm export count to screen before continuing on
    $runtime = Get-ElapsedTime($stopwatch)
    Write-Host "Mailbox export has completed, $($mailboxExportList.Count) mailboxes have been collected.(total runtime: $runtime)" -ForegroundColor Green -BackgroundColor Black

    #Collect MEUs Info============================
    $i =0
    Foreach ($character in $filterList){
        #Write our progress to screen
        Write-Progress -Activity "Step 3 of 10: Collecting Mail-Enabled User info from Office365 tenant" -Status "Currently processing UPNs starting with: $character" -percentComplete (($i / $filterList.Count) * 100)
        
        #Collect all MEUs and their SMTP addresses
        $meuExportList += Get-Mailuser -ResultSize Unlimited -Filter "UserPrincipalName -like '$character*'" | Select-Object Name,PrimarySMTPAddress,EmailAddresses,HiddenFromAddressListsEnabled,IsDirSynced,RecipientTypeDetails,ExternalEmailAddress
        $i++
    }
    #Confirm export count to screen before continuing on
    $runtime = Get-ElapsedTime($stopwatch)
    Write-Host "Mail-Enabled User export has completed, $($meuExportList.Count) mail-enabled users have been collected.(total runtime: $runtime)" -ForegroundColor Green -BackgroundColor Black

    #Collect Contact Info============================
    $i =0
    Foreach ($character in $filterList){
        #Write our progress to screen
        Write-Progress -Activity "Step 4 of 10: Collecting Contact info from Office365 tenant" -Status "Currently processing Contacts starting with: $character" -percentComplete (($i / $filterList.Count) * 100)
        
        #Collect all Contacts and their SMTP address
        $contactExportList += Get-Contact -ResultSize Unlimited -Filter "Name -like '$character*'" | Select-Object Name,WindowsEmailAddress,IsDirSynced,DistinguishedName,RecipientTypeDetails
        $i++
    }
    #Confirm export count to screen before continuing on
    $runtime = Get-ElapsedTime($stopwatch)
    Write-Host "Contact export has completed, $($contactExportList.Count) contacts have been collected.(total runtime: $runtime)" -ForegroundColor Green -BackgroundColor Black

    #Collect DistributionList Info============================
    $i =0
    Foreach ($character in $filterList){
        #Write our progress to screen
        Write-Progress -Activity "Step 5 of 10: Collecting distribution group info from Office365 tenant" -Status "Currently processing distribution groups starting with: $character" -percentComplete (($i / $filterList.Count) * 100)
        
        #Collect all mailboxes and their SMTP addresses
        $distributionGroupExportList += Get-DistributionGroup -ResultSize Unlimited -Filter "Name -like '$character*'" -WarningAction SilentlyContinue | select Name,Alias,PrimarySmtpAddress,EmailAddresses,RecipientTypeDetails,DisplayName,HiddenFromAddressListsEnabled,IsDirSynced
        $i++
    }
    #Confirm export count to screen before continuing on
    $runtime = Get-ElapsedTime($stopwatch)
    Write-Host "Distribution group export has completed, $($distributionGroupExportList.Count) groups have been collected.(total runtime: $runtime)" -ForegroundColor Green -BackgroundColor Black

    #Unified Group Export============================
    $i =0
    Foreach ($character in $filterList){
        #Write our progress to screen
        Write-Progress -Activity "Step 6 of 10: Collecting unified group info from Office365 tenant" -Status "Currently processing unified groups starting with: $character" -percentComplete (($i / $filterList.Count) * 100)
        
        #Collect all mailboxes and their SMTP addresses
        $unifiedGroupExportList += Get-UnifiedGroup -ResultSize Unlimited -Filter "Name -like '$character*'" -WarningAction SilentlyContinue | select Name,Alias,PrimarySmtpAddress,EmailAddresses,RecipientTypeDetails,DisplayName,HiddenFromAddressListsEnabled,IsDirSynced,SharePointSiteUrl,GroupMemberCount
        $i++
    }
    #Confirm export count to screen before continuing on
    $runtime = Get-ElapsedTime($stopwatch)
    Write-Host "Unified group export has completed, $($distributionGroupExportList.Count) groups have been collected.(total runtime: $runtime)" -ForegroundColor Green -BackgroundColor Black

    #Mailbox List Processing============================
    $i=0
    foreach ($mailbox in $mailboxExportList) {
        $exportData += Process-SMTP-Address -object $mailbox -itemType "Mailbox"
        $i++
        Write-Progress -Activity "Step 7 of 10: Counting Mailbox SMTP Addresses" -Status "Processed $i of $($mailboxExportList.Count) mailboxes" -percentComplete (($i / $mailboxExportList.Count)  * 100)
    }
    $mailboxDomain = $exportData | Group-Object SMTPDomain,AddressType | Select Name,Count
    foreach($domain in $mailboxDomain){
        $type = (($domain.Name).split(",")[1]).replace(" ","")
        $domainName = ($domain.Name).split(",")[0]
        $mailboxReportInfo += [PSCustomObject]@{
            ItemType = "MailboxSMTPDomain-$type"
            Value = $domainName
            Count = $domain.Count
        }
    }
    $mailboxType = $mailboxExportList | Group-Object RecipientTypeDetails | Select Name,Count
    foreach($type in $mailboxType){
        $mailboxReportInfo += [PSCustomObject]@{
            ItemType = "MailboxCount"
            Value = $type.Name
            Count = $type.Count
        }
    }
    $mailboxCount = $mailboxExportList.Count
    $mailboxReportInfo += [PSCustomObject]@{
        ItemType = "MailboxCountTotal"
        Value = "All Mailboxes"
        Count = $mailboxCount
    }
    $mailboxForwardCount = (($mailboxExportList | ? {$_.ForwardingSmtpAddress -ne $null}) | Measure-Object).Count
    $mailboxReportInfo += [PSCustomObject]@{
        ItemType = "MailboxesWithExternalForwarders"
        Value = "ForwardingSmtpAddressCount"
        Count = $mailboxForwardCount
    }
    $mailboxDirSync = $mailboxExportList | Group-Object IsDirSynced | Select Name,Count
    foreach($type in $mailboxDirSync){
        If($type.Name -eq "True"){
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "Mailbox-DirSyncStatus"
                Value = "Enabled"
                Count = $type.Count
            }
        }
        Else{
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "Mailbox-DirSyncStatus"
                Value = "Disabled"
                Count = $type.Count
            }
        }

    }
    $mailboxState = $mailboxExportList | Group-Object ExchangeUserAccountControl | Select Name,Count
    foreach($type in $mailboxState){
        If($type.Name -eq "AccountDisabled"){
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "MailboxAccountState"
                Value = "Disabled"
                Count = $type.Count
            }
        }
        Else{
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "MailboxAccountState"
                Value = "Enabled"
                Count = $type.Count
            }
        }

    }
    $mailboxArchive = $mailboxExportList | Group-Object ArchiveStatus | Select Name,Count
    foreach($archive in $mailboxArchive){
        if($archive.Name -eq "None"){
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "ArchiveStatus"
                Value = "NoArchive"
                Count = $archive.Count
            }
        }
        Elseif($archive.Name -eq "Active"){
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "ArchiveStatus"
                Value = "ArchiveEnabled"
                Count = $archive.Count
            }
        }
        Else{
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "ArchiveStatus"
                Value = $archive.Name
                Count = $archive.Count
            }
        }
    }
    $mailboxLithold = $mailboxExportList | Group-Object LitigationHoldEnabled | Select Name,Count
    foreach($lithold in $mailboxLithold){
        If($lithold.Name -eq "True"){
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "LitigationHoldStatus"
                Value = "OnLitHold"
                Count = $lithold.Count
            }
        }
        Elseif($lithold.Name -eq "False"){
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "LitigationHoldStatus"
                Value = "None"
                Count = $lithold.Count
            }
        }
        Else{
            $mailboxReportInfo += [PSCustomObject]@{
                ItemType = "LitigationHoldStatus"
                Value = $lithold.Name
                Count = $lithold.Count
            }
        }

    }

    $runtime = Get-ElapsedTime($stopwatch)
    Write-Host "Mailbox object processing has completed for $($mailboxExportList.Count) mailboxes.(total runtime: $runtime)" -ForegroundColor Green -BackgroundColor Black

    #MEU | Contact Processing============================
    $exportData=@()
    $i = 0
    foreach($meu in $meuExportList){
        $exportData += Process-SMTP-Address -object $meu -itemType "Mail-EnabledUser"
        $i++
        Write-Progress -Activity "Step 8 of 10: Parse Mail-EnabledUser data for export" -Status "Processed $i of $($meuExportList.Count) groups" -percentComplete (($i / $meuExportList.Count)  * 100)
    }
    $meuDomain = $exportData | Group-Object SMTPDomain,AddressType | Select Name,Count
    foreach($domain in $meuDomain){
        $type = (($domain.Name).split(",")[1]).replace(" ","")
        $domainName = ($domain.Name).split(",")[0]
        $meuReportInfo += [PSCustomObject]@{
            ItemType = "Mail-EnabledUserSMTPDomain-$type"
            Value = $domainName
            Count = $domain.Count
        }
    }
    $meuCount = $meuExportList.Count
    $meuReportInfo += [PSCustomObject]@{
        ItemType = "Mail-EnabledUserCountTotal"
        Value = "All MEUs"
        Count = $meuCount
    }
    $contactCount = $contactExportList.Count
    $meuReportInfo += [PSCustomObject]@{
        ItemType = "ContactCountTotal"
        Value = "All Contacts"
        Count = $contactCount
    }
    $runtime = Get-ElapsedTime($stopwatch)
    Write-Host "Contact/Mail-Enabled User object processing has completed for $($mailboxExportList.Count) objects.(total runtime: $runtime)" -ForegroundColor Green -BackgroundColor Black

    #Distribution Group Processing============================
    $exportData=@()
    $i = 0
    foreach($group in $distributionGroupExportList){
        $exportData += Process-SMTP-Address -object $group -itemType "DistributionGroup"
        $i++
        Write-Progress -Activity "Step 9 of 10: Parse distribution group data for export" -Status "Processed $i of $($distributionGroupExportList.Count) groups" -percentComplete (($i / $distributionGroupExportList.Count)  * 100)
    }
    $distributionGorupDomain = $exportData | Group-Object SMTPDomain,AddressType | Select Name,Count
    foreach($domain in $distributionGorupDomain){
        $type = (($domain.Name).split(",")[1]).replace(" ","")
        $domainName = ($domain.Name).split(",")[0]
        $distributionGroupReportInfo += [PSCustomObject]@{
            ItemType = "DistributionGroupSMTPDomain-$type"
            Value = $domainName
            Count = $domain.Count
        }
    }
    $distributionGroupType = $distributionGroupExportList | Group-Object RecipientTypeDetails | Select Name,Count
    foreach($type in $distributionGroupType){
        $distributionGroupReportInfo += [PSCustomObject]@{
            ItemType = "DistributionListType"
            Value = $type.Name
            Count = $type.Count
        }
    }
    $distributionGroupCount = $distributionGroupExportList.Count
    $distributionGroupReportInfo += [PSCustomObject]@{
        ItemType = "DistributionListTotal"
        Value = "All Distribution Lists"
        Count = $distributionGroupCount
    }
    $distributionGroupDirSync = $distributionGroupExportList | Group-Object IsDirSynced | Select Name,Count
    foreach($type in $distributionGroupDirSync){
        If($type.Name -eq "True"){
            $distributionGroupReportInfo += [PSCustomObject]@{
                ItemType = "DistributionList-DirSyncStatus"
                Value = "Enabled"
                Count = $type.Count
            }
        }
        Else{
            $distributionGroupReportInfo += [PSCustomObject]@{
                ItemType = "DistributionList-DirSyncStatus"
                Value = "Disabled"
                Count = $type.Count
            }
        }

    }

    $runtime = Get-ElapsedTime($stopwatch)
    Write-Host "Distribution Group data parsing for $($distributionGroupExportList.Count) groups has completed .(total runtime: $runtime)" -ForegroundColor Green -BackgroundColor Black

    #Unified Group Processing============================
    $exportData=@()
    $i = 0
    foreach($group in $unifiedGroupExportList){
        $exportData += Process-SMTP-Address -object $group -itemType "UnifiedGroup"
        $i++
        Write-Progress -Activity "Step 10 of 10: Parse unified group data for export" -Status "Processed $i of $($unifiedGroupExportList.Count) groups" -percentComplete (($i / $unifiedGroupExportList.Count)  * 100)
    }
    $unifiedGorupDomain = $exportData | Group-Object SMTPDomain,AddressType | Select Name,Count
    foreach($domain in $unifiedGorupDomain){
        $type = (($domain.Name).split(",")[1]).replace(" ","")
        $domainName = ($domain.Name).split(",")[0]
        $unifiedGroupReportInfo += [PSCustomObject]@{
            ItemType = "UnifiedGroupSMTPDomain-$type"
            Value = $domainName
            Count = $domain.Count
        }
    }
    $unifiedGroupType = $unifiedGroupExportList | Group-Object RecipientTypeDetails | Select Name,Count
    foreach($type in $unifiedGroupType){
        $unifiedGroupReportInfo += [PSCustomObject]@{
            ItemType = "UnifiedGroupType"
            Value = $type.Name
            Count = $type.Count
        }
    }
    If($CollectUGInfo){
        foreach($url in $unifiedGroupExportList){
            If($url.SharePointSiteUrl -eq $null){
                $unifiedGroupReportInfo += [PSCustomObject]@{
                    ItemType = "UnifiedGroupSharePointURL:$($url.DisplayName)"
                    Value = "None"
                    Count = "Members:$($url.GroupMemberCount)"
                }
            }
            Else{
                $unifiedGroupReportInfo += [PSCustomObject]@{
                    ItemType = "UnifiedGroupSharePointURL:$($url.DisplayName)"
                    Value = $url.SharePointSiteUrl
                    Count = "Members:$($url.GroupMemberCount)"
                }
            }
        }
    }
    Else{
        foreach($url in $unifiedGroupExportList){
                $unifiedGroupReportInfo += [PSCustomObject]@{
                    ItemType = "UnifiedGroupName"
                    Value = "$($url.DisplayName)"
                    Count = "Members:$($url.GroupMemberCount)"
                }
        }
    }


    $runtime = Get-ElapsedTime($stopwatch)
    Write-Host "Unified Group data parsing for $($unifiedGroupExportList.Count) groups has completed .(total runtime: $runtime)" -ForegroundColor Green -BackgroundColor Black

    #Collect License Info
    $licenseInfo = Get-MsolAccountSku | Select *
    foreach($license in $licenseInfo){
        $licenseName = switch ($license.SkuPartNumber){
            "EXCHANGEESSENTIALS"    {"EXCHANGE ONLINE ESSENTIALS"}
            "EXCHANGE_S_ESSENTIALS"    {"EXCHANGE ONLINE ESSENTIALS"}
            "EXCHANGEDESKLESS"    {"EXCHANGE ONLINE KIOSK"}
            "EXCHANGEARCHIVE"    {"EXCHANGE ONLINE ARCHIVING FOR EXCHANGE SERVER"}
            "EXCHANGEARCHIVE_ADDON"    {"EXCHANGE ONLINE ARCHIVING FOR EXCHANGE ONLINE"}
            "EXCHANGEENTERPRISE"    {"EXCHANGE ONLINE (PLAN 2)"}
            "EXCHANGESTANDARD"    {"EXCHANGE ONLINE (PLAN 1)"}
            "EMSPREMIUM"    {"ENTERPRISE MOBILITY + SECURITY E5"}
            "EMS"    {"ENTERPRISE MOBILITY + SECURITY E3"}
            "AAD_BASIC"    {"AZURE ACTIVE DIRECTORY BASIC"}
            "AAD_PREMIUM"    {"AZURE ACTIVE DIRECTORY PREMIUM P1"}
            "AAD_PREMIUM_P2"    {"AZURE ACTIVE DIRECTORY PREMIUM P2"}
            "SPE_E3"    {"MICROSOFT 365 E3"}
            "O365_BUSINESS"    {"OFFICE 365 BUSINESS"}
            "SMB_BUSINESS"    {"OFFICE 365 BUSINESS"}
            "O365_BUSINESS_ESSENTIALS"    {"OFFICE 365 BUSINESS ESSENTIALS"}
            "SMB_BUSINESS_ESSENTIALS"    {"OFFICE 365 BUSINESS ESSENTIALS"}
            "O365_BUSINESS_PREMIUM"    {"OFFICE 365 BUSINESS PREMIUM"}
            "SMB_BUSINESS_PREMIUM"    {"OFFICE 365 BUSINESS PREMIUM"}
            "STANDARDPACK"    {"OFFICE 365 ENTERPRISE E1"}
            "STANDARDWOFFPACK"    {"OFFICE 365 ENTERPRISE E2"}
            "ENTERPRISEWITHSCAL"    {"OFFICE 365 ENTERPRISE E4"}
            "ENTERPRISEPACK"    {"OFFICE 365 ENTERPRISE E3"}
            "ENTERPRISEPREMIUM"    {"OFFICE 365 ENTERPRISE E5"}
            "ENTERPRISEPREMIUM_NOPSTNCONF"    {"OFFICE 365 ENTERPRISE E5 WITHOUT AUDIO CONFERENCING"}
            "DESKLESSPACK"    {"OFFICE 365 F1"}
            "WIN10_PRO_ENT_SUB"    {"WINDOWS 10 ENTERPRISE E3"}
            "Win10_VDA_E3" {"WINDOWS 10 ENTERPRISE E3"}
            default {"LicenseSKU:$($license.SkuPartNumber)"}
        }

        $remainingLicenses = $license.ActiveUnits - $license.ConsumedUnits
        $licenseReportInfo += [PSCustomObject]@{
            ItemType = "Office365LicenseType"
            Value = $licenseName
            Count = "Total:$($license.ActiveUnits) | Assigned:$($license.ConsumedUnits) | Remaining:$remainingLicenses"
        }
    }
}
End{
    Try{
        #Generate HTML Report
        $header = "
        <title>BinaryTree - Office365 Tenant Discovery Tool</title>
        <style type=`"text/css`">
        h1, h2 {
            font-family: `"Trebuchet MS`", Arial, Helvetica, sans-serif;
            color: `#000000
        }
        table {
            font-family: `"Trebuchet MS`", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          
        table td, table th {
            border: 1px solid `#ddd;
            padding: 8px;
          }
          
        table tr:nth-child(even){background-color: `#f2f2f2;}
          
        table tr:hover {background-color: `#ddd;}
          
        table th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: `#256b93;
            color: white;
          }
        </style>
        "

        #Generate Report Results
        $htmlReport = $null | ConvertTo-Html -Head $header -PreContent "<h1><strong>BinaryTree - Office365 Tenant Discovery Tool: $($tenantName)</strong></h1>"
        $htmlReport += $tenantExportInfo | ConvertTo-Html -PreContent "<h2><strong>Tenant Info:</strong></h2>" -Fragment
        $htmlReport += $orgRelatiionhips | ConvertTo-Html -PreContent "<h2><strong>Tenant Org Relationships Info:</strong></h2>" -Fragment
        $htmlReport += $acceptedDomainInfo | ConvertTo-Html -PreContent "<h2><strong>Accepted Domain Info:</strong></h2>" -Fragment
        $htmlReport += $companyInfo | ConvertTo-Html -PreContent "<h2><strong>AADC Status Info:</strong></h2>" -Fragment
        $htmlReport += $dirSyncFeatures | ConvertTo-Html -PreContent "<h2><strong>AADC Features Settings:</strong></h2>" -Fragment
        $htmlReport += $transportInfo | ConvertTo-Html -PreContent "<h2><strong>Transport Rules Overview:</strong></h2>" -Fragment
        If($CollectPFInfo){
            $htmlReport += $publicFolderInfo | ConvertTo-Html -PreContent "<h2><strong>PublicFolder Tree Overview:</strong></h2>" -Fragment
            $htmlReport += $mailPublicFolderInfo | ConvertTo-Html -PreContent "<h2><strong>Mail-Enabled PublicFolder Overview:</strong></h2>" -Fragment
        }
        $htmlReport += $mailboxReportInfo | ConvertTo-Html -PreContent "<h2><strong>Mailbox Collection Results:</strong></h2>" -Fragment
        $htmlReport += $meuReportInfo | ConvertTo-Html -PreContent "<h2><strong>Contact & Mail-EnabledUser Collection Results:</strong></h2>" -Fragment
        $htmlReport += $distributionGroupReportInfo | ConvertTo-Html -PreContent "<h2><strong>Distribution List Collection Results:</strong></h2>" -Fragment
        $htmlReport += $unifiedGroupReportInfo | ConvertTo-Html -PreContent "<h2><strong>Unified Groups Collection Results:</strong></h2>" -Fragment
        $htmlReport += $licenseReportInfo | ConvertTo-Html -PreContent "<h2><strong>License Consumption Overview:</strong></h2>" -Fragment
        $htmlReport | Out-File $txtHTMLPath

        $stopwatch.Stop()

        Write-Host "Results have been saved locally to: " -ForegroundColor Green -BackgroundColor Black -NoNewline
        Write-host $txtHTMLPath -ForegroundColor white -BackgroundColor Black
    }
    Catch{
        Write-Host "Script has failed to export the results to: $txtHTMLPath, with error:" -ForegroundColor White -BackgroundColor black
        Write-Host "ERROR: $($_.Exception.Message)" -ForegroundColor Red -BackgroundColor black
        $stopwatch.Stop()
    }
}
